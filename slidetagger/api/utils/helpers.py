def alchemy_to_dict(obj):
    columns = [c.name for c in obj.__table__.columns]
    column_items = [(c, getattr(obj, c)) for c in columns]
    return dict(column_items)
