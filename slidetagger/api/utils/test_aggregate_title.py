from unittest import TestCase

from .aggregator import aggregate_title

SAMPLE_OCR_ID = 853


class TestAggregator(TestCase):
    def test_aggregate_title(self):
        title = aggregate_title(SAMPLE_OCR_ID)
        self.assertEqual(title[0], 1)
