import logging
import os
import redis
from pythonjsonlogger import jsonlogger
from flask import Flask
app = Flask(__name__)

r = redis.StrictRedis(host=os.environ.get('REDIS_ADDR'), port=6379, db=0)


class RedisHandler(logging.Handler):
    def __init__(self, redis_conn, key):
        logging.Handler.__init__(self)
        self.redis = redis_conn
        self.redis_key = key

    def emit(self, record):
        self.redis.lpush(self.redis_key, self.format(record))


logger = logging.getLogger('slidetagger')
streamHandler = logging.StreamHandler()
redisHandler = RedisHandler(r, os.environ.get('REDIS_KEY'))

formatter = jsonlogger.JsonFormatter('(asctime) (message)')
redisHandler.setFormatter(formatter)
streamHandler.setFormatter(formatter)

if app.debug:
    logger.addHandler(streamHandler)
logger.addHandler(redisHandler)
logger.setLevel(logging.INFO)
