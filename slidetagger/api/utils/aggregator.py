from collections import Counter

from schemas.database import db_session
from schemas.models import Annotation


def aggregate_title(ocr_id):
    title_annotations = db_session.query(Annotation.body).filter(
        Annotation.ocr_id == ocr_id,
        Annotation.body['stage'].astext == 'title'
    ).all()
    title_ids = [annotation_to_string(annotation.body['payload'])
                 for annotation in title_annotations]
    title_counter = Counter(title_ids)
    title = title_counter.most_common(1)[0][0]
    return string_to_annotation(title)


# Utils
def annotation_to_string(numbers):
    return "_".join(str(x) for x in numbers)


def string_to_annotation(string):
    return [int(x) for x in string.split("_")]
