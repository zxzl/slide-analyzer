from sqlalchemy import func, asc
from schemas.models import OCRResult, Worker


def find_remaining(db_session, job_name_query=None):
    """
    Assigns 1) least finished, 2) least started jobs.
    As a result, it works like round-robin scheduler in ideal condition.
    :param db_session: SQLAlchemy session
    :param job_name_query: string name of job_name query
    :return: an OCRResult object that needs to be tagged.
    """
    unfinished = db_session.query(
        OCRResult,
        func.count(Worker.finished_at).label('finished_count'),
        func.count(Worker.started_at).label('started_count')) \
        .outerjoin(Worker) \
        .group_by(OCRResult.id) \
        .order_by(asc('finished_count')) \
        .order_by(asc('started_count'))
    if job_name_query is not None:
        unfinished = unfinished.filter(OCRResult.job_name == job_name_query)

    row = unfinished.first()
    return row
