from unittest import TestCase
import schemas.database as test_db
import datetime as dt
from schemas.models import OCRResult, Worker
from slidetagger.api.utils import find_remaining, MIN_MARK_ACTIVE, JOB_PER_SLIDE


class TestAssign(TestCase):
    @classmethod
    def setUpClass(cls):
        test_db.init_test_db()
        cls.session = test_db.get_test_session()
        TestAssign.populate_db(cls.session)

    @classmethod
    def tearDownClass(cls):
        cls.session.close()
        print("Dropping table")
        test_db.drop_test_db()
        print("Dropped table")

    def tearDown(self):
        self.session.query(Worker).delete()
        self.session.commit()

    @staticmethod
    def populate_db(session):
        now = dt.datetime.utcnow()
        ocrs = []
        for _ in range(100):
            ocr = OCRResult(job_name='test', last_assigned=now)
            ocrs.append(ocr)
        session.add_all(ocrs)
        session.commit()
        print("Put 100 ocr")

    @staticmethod
    def mark_finish(now, s, worker_id):
        worker = s.query(Worker).get(worker_id)
        worker.finished_at = now
        s.commit()

    @staticmethod
    def find_and_assign(now, s):
        found = find_remaining(now, s)
        found_ocr_id = found.OCRResult.id
        assigned_worker = Worker(ocr_id=found_ocr_id, started_at=now)
        s.add(assigned_worker)
        s.commit()
        return (found_ocr_id, assigned_worker.id)

    def test_jobs_get_active(self):
        now = dt.datetime.utcfromtimestamp(1000)
        ids_1 = [self.find_and_assign(now, self.session)
                 for _ in range(JOB_PER_SLIDE)]

        ids_2 = [self.find_and_assign(now, self.session)
                 for _ in range(JOB_PER_SLIDE)]

        ids_3 = [self.find_and_assign(now, self.session)
                 for _ in range(JOB_PER_SLIDE)]

        for ids in [ids_1, ids_2, ids_3]:
            ocr_ids = list(zip(*ids))[0]
            for i in range(0, len(ocr_ids) - 1):
                self.assertEqual(ocr_ids[i], ocr_ids[i + 1],
                                 "Job already started should assigned first")

        self.assertNotEqual(ids_1[0][0], ids_2[0][0],
                            "Max active job is {}".format(JOB_PER_SLIDE))
        self.assertNotEqual(ids_2[0][0], ids_3[0][0],
                            "Max active job is {}".format(JOB_PER_SLIDE))

    def test_jobs_become_inactive(self):
        dt_0 = dt.datetime.utcfromtimestamp(1000 + MIN_MARK_ACTIVE * 60 + 10)
        ids_past = [self.find_and_assign(dt_0, self.session)
                    for _ in range(JOB_PER_SLIDE - 1)]

        dt_1 = dt_0 + dt.timedelta(minutes=(MIN_MARK_ACTIVE + 1))
        id_now = self.find_and_assign(dt_1, self.session)

        dt_2 = dt_1 + dt.timedelta(minutes=(MIN_MARK_ACTIVE - 1))
        ids_future = [self.find_and_assign(dt_2, self.session)
                      for _ in range(JOB_PER_SLIDE)]

        for id_future in ids_future[:-1]:
            self.assertEqual(id_now[0], id_future[0])
        self.assertNotEqual(id_now[0], ids_future[-1][0])

    def test_ignore_finished_job(self):
        dt_0 = dt.datetime.utcfromtimestamp(1000)
        pairs_0 = [self.find_and_assign(dt_0, self.session)
                   for _ in range(JOB_PER_SLIDE)]

        dt_1 = dt_0 + dt.timedelta(minutes=6)
        for pair in pairs_0:
            self.mark_finish(dt_1, self.session, pair[1])
        pair_1 = self.find_and_assign(dt_1, self.session)
        self.assertNotEqual(pair_1[0], pairs_0[0][0])
