from flask import Flask, request, jsonify, session
from flask_cors import cross_origin
import os
import random
import string
from slidetagger.api.utils import logger
from slidetagger.api.views import admin, slide, vote, annotation
from schemas.database import db_session
from schemas.models import Annotation, Worker, Feedback, Batch

app = Flask(__name__)
app.secret_key = os.environ.get('FLASK_SECRET_KEY')
app.register_blueprint(admin.blueprint, url_prefix="")
app.register_blueprint(slide.blueprint, url_prefix="")
app.register_blueprint(vote.blueprint, url_prefix="")
app.register_blueprint(annotation.blueprint, url_prefix="")


@app.route("/api/session/start/")
@cross_origin(supports_credentials=True)
def start_session():
    # session['token'] = 'test_token'
    new_worker = Worker(alias=''.join(
        random.choices(string.ascii_uppercase + string.digits, k=6)))
    db_session.add(new_worker)
    db_session.flush()
    session['job_id'] = new_worker.id
    db_session.commit()
    logger.info("{} {} issued".format(new_worker.id, new_worker.alias),
                extra={'user': new_worker.id})
    return 'OK'


@app.route("/api/annotation/<string:stage>/<int:slide_id>", methods=['GET'])
@cross_origin(supports_credentials=True)
def get_annotations(stage, slide_id):
    job_id = session.get('job_id', 1853)

    annotation = db_session.query(Annotation.body).filter(
        Annotation.ocr_id == slide_id,
        Annotation.worker_id == job_id,
        Annotation.body['stage'].astext == stage)\
        .first()
    if not annotation:
        return jsonify(annotations=[], stage=stage)
    return jsonify(annotations=annotation[0]['payload'], stage=stage)


@app.route("/api/feedback/<stage>/<int:slide_id>", methods=['POST'])
@cross_origin(supports_credentials=True)
def save_feedback(stage, slide_id):
    worker_id = session.get('job_id')
    body = request.get_data(as_text=True)
    feedback = Feedback(
        created_by=worker_id, slide_id=slide_id, stage=stage, body=body)
    db_session.add(feedback)
    db_session.commit()
    return "OK"


@app.teardown_request
def session_clear(exception=None):
    db_session.remove()
    if exception and db_session.is_active:
        db_session.rollback()


if __name__ == '__main__':
    app.run(threaded=True)
