from flask import Blueprint, session, jsonify, Response, request, abort
from flask_cors import CORS
from sqlalchemy import func, asc, desc, and_
import datetime as dt
import json

from schemas.database import db_session
from schemas.models import Annotation, OCRResult, Batch, Worker

blueprint = Blueprint('annotation', __name__)
CORS(blueprint, supports_credentials=True)


@blueprint.route("/api/annotation/new/<int:ocr_id>", methods=['POST'])
def save_annotation(ocr_id):
    worker_id = session.get('job_id', None)
    batch_id = request.args.get('batch', None)
    result = request.get_json()
    annotation = Annotation(body=result, worker_id=worker_id, ocr_id=ocr_id,
                            batch_from=batch_id)
    db_session.add(annotation)
    db_session.commit()
    return jsonify(result="ok")


@blueprint.route("/api/annotation/next")
def find_next_slide():
    worker_id = session.get('job_id', 3620)
    batch_id = int(request.args.get('batch', -1))
    stage = request.args.get('stage')

    if worker_id is None:
        return Response('Go to the first page', status=400)
    batch = db_session.query(Batch).get(batch_id)
    row = db_session.query(
        OCRResult,
        func.count(Annotation.id).label('annotation_count')
        )\
        .outerjoin(Annotation)\
        .group_by(OCRResult.id)\
        .filter(OCRResult.id >= batch.min_ocrid, OCRResult.id <= batch.max_ocrid)\
        .filter(~OCRResult.annotations.any(and_(
            Annotation.body['stage'].astext == stage,
            Annotation.worker_id == worker_id,
            Annotation.batch_from == batch_id)
        ))\
        .order_by('annotation_count') \
        .order_by(OCRResult.id) \
        .first()

    count = db_session.query(Annotation).filter(Annotation.worker_id == worker_id).count()
    finished = is_work_finished(worker_id, stage, batch) or (row is None)
    code = ""
    if finished:
        worker = db_session.query(Worker).get(worker_id)
        code = worker.alias
    return jsonify(
        slide_id=row[0].id if not finished else -1,
        count=count,
        finished=finished,
        code=code,
    )


def is_work_finished(worker_id, stage, batch):
    rows = db_session.query(
        Annotation.body['stage'].astext,
        func.count())\
        .group_by(Annotation.body['stage'].astext)\
        .filter(Annotation.worker_id == worker_id)\
        .all()
    stage_count = {r[0]: r[1] for r in rows}

    if stage == 'sentence':
        return stage_count.get('sentence', 0) >= batch.annotation_per_worker_sentence
    return stage_count.get(stage, 0) >= batch.annotation_per_worker


@blueprint.route("/api/annotation/<int:annotation_id>")
def get_annotation(annotation_id):
    annotation = db_session.query(Annotation).get(annotation_id)
    return jsonify(
        payload=annotation.body,
        stage=annotation.body['stage'],
        ocr_id=annotation.ocr_id,
        id=annotation.id,
    )
