from collections import defaultdict
from flask import Blueprint, Flask, jsonify, request, session, Response
from flask_cors import CORS

from schemas.database import db_session
from schemas.models import Annotation, Feedback, OCRResult, Worker, Vote
from sqlalchemy import func, desc, distinct, asc
from functools import wraps
import os

from ..utils import alchemy_to_dict

blueprint = Blueprint('admin', __name__)
CORS(blueprint, supports_credentials=True)


@blueprint.route("/api/admin/login", methods=['POST'])
def login_admin():
    body = request.get_json()
    key = body['token']
    if key == os.environ.get('FLASK_ADMIN_KEY'):
        session['admin'] = 'true'
        return jsonify(success=True)
    session['admin'] = 'false'
    return jsonify(success=False)


def admin_only(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('admin', 'false') == 'true':
            return f(*args, **kwargs)
        return Response('fail', status=401)
    return decorated_function


@blueprint.route("/api/admin/result/<int:slide_id>")
@admin_only
def get_result(slide_id):
    batch = int(request.args.get('batch', 0))

    results = db_session.query(Annotation)\
        .filter(Annotation.ocr_id == slide_id)\
        .filter(Annotation.batch_from == batch)
    results_by_stage = defaultdict(list)
    for r in results:
        stage = r.body['stage']
        results_by_stage[stage].append({
            'payload': r.body['payload'],
            'worker': r.worker_id,
        })
    return jsonify(results=results_by_stage)


@blueprint.route("/api/admin/result/feedback/<int:slide_id>")
@admin_only
def get_feedback(slide_id):
    feedbacks = db_session.query(Feedback)\
        .filter(Feedback.slide_id == slide_id).all()
    feedback_dict = [alchemy_to_dict(f) for f in feedbacks]
    return jsonify(feedbacks=feedback_dict)


@blueprint.route("/api/admin/result/vote/<int:annotation_id>")
@admin_only
def get_vote(annotation_id):
    annotation = db_session.query(Annotation).get(annotation_id)
    total, up = 0, 0
    for vote in annotation.votes:
        total += 1
        if vote.up:
            up += 1

    return jsonify(
        finished=annotation.vote_finished,
        total_vote=total,
        upvote=up,
    )


@blueprint.route("/api/admin/result/slides")
@admin_only
def get_annotated_slides():
    batch = int(request.args.get('batch'))
    results = db_session.query(
        Annotation.ocr_id,
        func.count(distinct(Annotation.worker_id)),
        func.count(Annotation.id),
        func.max(Annotation.created_at).label('last_annotated'))\
        .filter(Annotation.batch_from == batch)\
        .group_by(Annotation.ocr_id)\
        .order_by(desc('Annotation.ocr_id'))\
        .all()
    slides = [{
        'slide_id': r[0],
        'worker_count': r[1],
        'annotation_count': r[2],
        'last_annotated': r[3],
    } for r in results]
    return jsonify(slides=slides)


@blueprint.route("/api/admin/result/<int:batch>/annotations")
def get_annotations(batch):
    rows = db_session.query(Annotation)\
        .filter(Annotation.batch_from == batch) \
        .order_by(Annotation.body['stage'].astext) \
        .order_by('Annotation.ocr_id')\
        .all()

    annotations = [{
        'id': annotation.id,
        'ocr_id': annotation.ocr_id,
        'stage': annotation.body['stage'],
        'approved': annotation.is_good,
    } for annotation in rows]
    return jsonify(annotations)


@blueprint.route("/api/admin/status")
@admin_only
def get_worker_status():
    group_name = request.args.get('group', "")
    unfinished = db_session.query(
        OCRResult.id,
        func.count(Worker.finished_at).label('finished_count'),
        func.count(Worker.started_at).label('started_count'),
    ) \
        .outerjoin(Worker) \
        .group_by(OCRResult.id) \
        .order_by(asc('finished_count')) \
        .order_by(asc('started_count')) \

    if group_name != "":
        unfinished = unfinished.filter(OCRResult.job_name == group_name)
    ocrs = [{
        'slide_id': r[0],
        'finished': r[1],
        'started': r[2],
    } for r in unfinished.all()]
    return jsonify(status=ocrs)

