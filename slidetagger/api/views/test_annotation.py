from unittest import TestCase
from .annotation import is_work_finished
from schemas.database import db_session
from schemas.models import Batch


class TestAnnotation(TestCase):
    def test_work_finished(self):
        worker_id = 3292
        task_stage = 'list'
        task_batch = db_session.query(Batch).get(3)
        finished = is_work_finished(worker_id, task_stage, task_batch)
        self.assertTrue(finished)

        worker_id = 3297
        task_stage = 'sentence'
        finished = is_work_finished(worker_id, task_stage, task_batch)
        self.assertTrue(finished)
