from flask import Blueprint, session, jsonify, Response, request, abort
from flask_cors import CORS
from sqlalchemy import func, asc, desc
import datetime as dt

from schemas.database import db_session
from schemas.models import Annotation, Vote, Batch, Worker

blueprint = Blueprint('vote', __name__)
CORS(blueprint, supports_credentials=True)


@blueprint.route("/api/vote/next/<stage>")
def find_annotation_to_vote(stage):
    worker_id = session.get('job_id', 2910)
    batch_id = int(request.args.get('batch', -1))
    batch = db_session.query(Batch).get(batch_id)

    row = db_session.query(
        Annotation,
        func.count(Vote.id).label('vote_count')
    ).outerjoin(Vote).group_by(Annotation.id).filter(
        Annotation.body['stage'].astext == stage,
        Annotation.batch_from == batch_id,
        ~Annotation.votes.any(Vote.worker_id == worker_id)
    ).order_by("vote_count").first()

    count = db_session.query(Vote) \
        .filter(Vote.worker_id == worker_id) \
        .count()
    finished = (count >= batch.vote_per_worker) or (row is None)

    code = ""
    if finished:
        worker = db_session.query(Worker).get(worker_id)
        code = worker.alias
    return jsonify(
        annotation_id=(-1 if finished else row[0].id),
        count=count,
        finished=finished,
        code=code,
        max=batch.vote_per_worker,
    )


@blueprint.route("/api/vote/<int:annotation_id>/<action>")
def save_vote(**kwargs):
    worker_id = session.get('job_id', 1)
    print(worker_id)
    annotation_id = kwargs['annotation_id']
    vote = Vote(worker_id=worker_id, annotation_id=annotation_id)
    if kwargs['action'] == 'up':
        vote.up = True
    elif kwargs['action'] == 'down':
        vote.up = False
    else:
        abort(404)

    db_session.add(vote)
    db_session.commit()

    # Check vote finished
    if db_session.query(Vote).filter(
        annotation_id == annotation_id).count() >= 3:
        annotation = db_session.query(Annotation).get(annotation_id)
        annotation.vote_finished = True
        db_session.add(annotation)
        db_session.commit()

    return jsonify(result='ok')
