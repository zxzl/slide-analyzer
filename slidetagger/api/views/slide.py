from flask import Blueprint, session, jsonify, Response, request
from flask_cors import CORS
import datetime as dt

from slideparser.guess_sentence import guess_one_sentence
from slidetagger.api.utils import logger, find_remaining, helpers
from schemas.database import db_session
from schemas.models import Annotation, Worker, OCRResult

blueprint = Blueprint('slide', __name__)
CORS(blueprint, supports_credentials=True)


@blueprint.route("/api/slide/<int:slide_id>/check/")
def check_session(slide_id):
    job_id = session.get('job_id')
    # Check 4 stage was finished
    records = db_session.query(Annotation).filter(
        Annotation.worker_id == job_id,
        Annotation.ocr_id == slide_id,
    ).all()
    stages = [r.body['stage'] for r in records]
    missing_stages = []
    for s in ['title', 'list', 'sentence', 'figure']:
        if s not in stages:
            missing_stages.append(s)
    if len(missing_stages) > 0:
        logger.info("{} tried to finish {}".format(job_id, missing_stages),
                    extra={'user': job_id, 'slide': slide_id})
        return jsonify(result=False, missing=missing_stages)

    now = dt.datetime.utcnow()
    worker = db_session.query(Worker).get(job_id)
    worker.finished_at = now
    db_session.commit()
    logger.info("{} checked alias".format(job_id),
                extra={'user': job_id, 'slide': slide_id})
    return jsonify(result=True, job_id=worker.alias)


@blueprint.route("/api/slide/new")
def get_slide_unannotated():
    job_id = session.get('job_id')
    row = find_remaining(db_session, 'presentation_sample')
    if row is None:
        return Response('No remaining jobs', status=500)

    # Store assign information
    worker = db_session.query(Worker).get(job_id)
    worker.ocr_id = row.OCRResult.id

    now = dt.datetime.utcnow()
    worker.started_at = now
    db_session.commit()

    return jsonify(slide_id=row.OCRResult.id)


@blueprint.route("/api/slide/<int:slide_id>", methods=['GET', 'POST'])
def handle_slide(slide_id):
    job_id = session.get('job_id')
    if request.method == 'GET':
        ocr = db_session.query(OCRResult).get(slide_id)
        return jsonify(file_path=ocr.file_name,
                       width=ocr.width,
                       height=ocr.height,
                       results=ocr.results,
                       id=ocr.id)
    # request is POST
    worker_id = session.get('job_id', None)
    result = request.get_json()
    annotation = Annotation(body=result, worker_id=worker_id, ocr_id=slide_id)
    db_session.add(annotation)
    db_session.commit()
    logger.info("{} posted slide {}".format(job_id, slide_id),
                extra={'user': job_id, 'slide': slide_id})
    return "OK"


@blueprint.route("/api/slide/sentence/<int:ocr_id>", methods=['GET'])
def guess_sentence(ocr_id):
    ocr = db_session.query(OCRResult).get(ocr_id)
    sentence_guess = db_session.query(Annotation.body).filter(
        Annotation.ocr_id == ocr_id,
        Annotation.body['stage'].astext == 'sentence_draft'
    ).first()
    if not sentence_guess:
        return jsonify(annotations=[])
    return jsonify(annotations=sentence_guess[0]['payload'])


@blueprint.route("/api/slide/<int:ocr_id>/sentence/guess", methods=['GET'])
def guess_sentence_from(ocr_id):
    word_from_id = request.args.get('from')
    first_sentence = guess_one_sentence(ocr_id, word_from_id)
    return jsonify(
        annotations=[
            t.dict
            for t in first_sentence['text_elements']]
    )
