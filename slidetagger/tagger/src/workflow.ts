const stageOrder = [
  'title',
  'list',
  'figure',
  'figureType',
  'sentenceGreedy',
  'sentenceType',
  'bye'
];

const getNextStage = (currentStage: string): string => {
  const currentIndex = stageOrder.indexOf(currentStage);
  // If curretStage is invalid and index is -1, go to first stage
  const nextIndex = Math.min(currentIndex + 1, stageOrder.length - 1);
  return stageOrder[nextIndex];
};

export const getNextURL = (currentURL: string): string => {
  const currentStage = currentURL.split('/').slice(-1).pop() || '';
  const nextStage = getNextStage(currentStage);
  return currentURL.replace(currentStage, nextStage);
};
