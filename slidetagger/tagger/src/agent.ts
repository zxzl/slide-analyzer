import * as urlJoin from 'url-join';
import { OCRResult, BBox } from './models';
import { Sentence, Feedback, AnnotationPayload } from './models';

export const baseURL = process.env.REACT_APP_API;

interface SlideResponse {
  id: number;
  file_path: string;
  width: number;
  height: number;
  results: Array<{
    id: number;
    text: string;
    vertices: {
      top: number;
      left: number;
      bottom: number;
      right: number;
    };
  }>;
}

export interface FigureResponse {
  stage: 'figure';
  annotations: {
    id: number;
    box: BBox;
  }[];
}

export interface SentenceResponse {
  stage: 'sentence';
  annotations: Sentence[];
}

export interface SentencesGuessResponse {
  stage: 'sentence_draft';
  annotations: number[];
}

export interface SentenceGuessResponse {
  annotations: {
    ids: number[];
    bbox: BBox;
    text: string;
  }[];
}

export interface AnnotationResponse {
  id: number;
  ocr_id: number;
  stage: string;
  approved: boolean;
  payload: AnnotationPayload;
}

export interface ProcessedSlide {
  slide_id: number;
  worker_count: number;
  annotation_count: number;
  last_annotated: string;
}

export interface OCRStatus {
  slide_id: number;
  finished: number;
  started: number;
}

export interface VoteResponse {
  annotation_id: number;
  count: number;
  finished: boolean;
  code: string;
}

export interface SlideCount {
  slide_id: number;
  count: number;
  finished: boolean;
  code: string;
}

export interface VoteResult {
  finished: boolean;
  total_vote: number;
  upvote: number;
}
export const fetchSlide = (id: number): Promise<OCRResult> => {
  const url = urlJoin(baseURL, `/api/slide/${id}`);

  return fetchWithCookie(url)
    .then(resp => resp.json())
    .then(mapToResult);
};

export const fetchNewSlideId = (): Promise<string> => {
  const url = urlJoin(baseURL, `/api/slide/new`);
  return fetchWithCookie(url)
    .then(resp => resp.json())
    .then(json => json.slide_id);
};

export const fetchSlideAndCount = (batch: number, stage: string): Promise<SlideCount> => {
  const url = urlJoin(baseURL, `/api/annotation/next?batch=${batch}&stage=${stage}`);
  return fetchWithCookie(url)
    .then(resp => resp.json());
};

export const postAnnotation = (id: number, stage, payload, cb: Function): void => {
  const url = urlJoin(baseURL, `/api/slide/${id}`);
  const headers = new Headers();
  headers.append('Accept', 'application/json, text/plain, */*');
  headers.append('Content-Type', 'application/json');

  const body = JSON.stringify({ stage, payload});
  fetch(url, {
    credentials: 'include',
    method: 'post',
    headers,
    body,
  }).then(resp => cb());
};

export const postAnnotationNoCallback = (ocrId: number, stage, payload, batch: number = 1): Promise<boolean> => {
  const url = urlJoin(baseURL, `/api/annotation/new/${ocrId}?batch=${batch}`);
  const headers = new Headers();
  headers.append('Accept', 'application/json, text/plain, */*');
  headers.append('Content-Type', 'application/json');

  const body = JSON.stringify({ stage, payload});
  return fetch(url, {
    credentials: 'include',
    method: 'post',
    headers,
    body,
  }).then(resp => resp.ok);
};

export const postFeedback = (id: number, stage, payload, cb: Function): void => {
  const url = urlJoin(baseURL, `/api/feedback/${stage}/${id}`);
  fetch(url, {
    credentials: 'include',
    method: 'post',
    body: payload,
  }).then(resp => cb());
};

export const resetSession = (): void => {
  const url = urlJoin(baseURL, `/api/session/start/`);
  fetch(url, {
    credentials: 'include',
  });
};

export const checkSession = (id): Promise<{}> => {
  const url = urlJoin(baseURL, `/api/slide/${id}/check/ `);
  return fetch(url, {
    credentials: 'include'
  })
  .then(resp => resp.json());
};

export const fetchAnnotation = (id: number): Promise<AnnotationResponse> => {
  const url = urlJoin(baseURL, `/api/annotation/${id}`);
  return fetchWithCookie(url).then(resp => resp.json());
} ;

export const fetchAnnotationByStage = (id: number, stage: string):
Promise<FigureResponse | SentenceResponse> => {
  const url = urlJoin(baseURL, `/api/annotation/${stage}/${id}`);
  return fetchWithCookie(url).then(resp => resp.json());
};

export const fetchGuessedSentence = (id: number): Promise<SentencesGuessResponse> => {
  const url = urlJoin(baseURL, `/api/slide/sentence/${id}`);
  return fetchWithCookie(url).then(resp => resp.json());
};

export const fetchGuessedSentenceFrom = (ocrId: number, wordId: number): Promise<SentenceGuessResponse> => {
  const url = urlJoin(baseURL, `/api/slide/${ocrId}/sentence/guess?from=${wordId}`);
  return fetchWithCookie(url).then(resp => resp.json());
};

/**
 * For voting
 */
export const fetchNextVote = (stage: string, batch: number): Promise<VoteResponse> => {
  const url = urlJoin(baseURL, `/api/vote/next/${stage}?batch=${batch}`);
  return fetchWithCookie(url).then(resp => resp.json());
};

export const postVote = (annotationId: number, isUp: boolean): void => {
  const actionParameter = isUp ? 'up' : 'down';
  const url = urlJoin(baseURL, `/api/vote/${annotationId}/${actionParameter}`);
  fetchWithCookie(url).then(resp => resp.json());
};

/**
 * For Admin pages
 */
export const adminLogin = (token: string): Promise<boolean> => {
  const url = urlJoin(baseURL, `/api/admin/login`);
  const headers = new Headers();
  headers.append('Accept', 'application/json, text/plain, */*');
  headers.append('Content-Type', 'application/json');
  return fetch(url, {
    credentials: 'include',
    method: 'post',
    headers,
    body: JSON.stringify({token})
  })
  .then(resp => resp.json())
  .then(json => json.success);
};

export const fetchOCRStatus = (): Promise<OCRStatus[]> => {
  const url = urlJoin(baseURL, '/api/admin/status?group=presentation_sample');
  return fetchWithCookie(url)
  .then(resp => resp.json())
  .then(json => json.status);
};

export const fetchProcessedSlides = (batch: number): Promise<ProcessedSlide[]> => {
  const url = urlJoin(baseURL, `/api/admin/result/slides?batch=${batch}`);
  return fetchWithCookie(url)
  .then(resp => resp.json())
  .then(json => json.slides);
};

export const fetchAnnotations = (batch: number): Promise<AnnotationResponse[]> => {
  const url = urlJoin(baseURL, `/api/admin/result/${batch}/annotations`);
  return fetchWithCookie(url).then(resp => resp.json());
};

export const fetchFeedback = (slideId: number): Promise<Feedback[]> => {
  const url = urlJoin(baseURL, `api/admin/result/feedback/${slideId}`);
  return fetchWithCookie(url)
  .then(resp => resp.json())
  .then(json => json.feedbacks);
};

export const fetchVoteResult = (annotationId: number): Promise<VoteResult> => {
  const url = urlJoin(baseURL, `api/admin/result/vote/${annotationId}`);
  return fetchWithCookie(url).then(resp => resp.json());
};

// Utils
const mapToResult = (resp: SlideResponse): OCRResult => {
  return {
    id: resp.id,
    img_url: resp.file_path,
    width: resp.width,
    height: resp.height,
    boxes: resp.results.map((r) => {
      const { top, left, bottom, right } = r.vertices;
      return {
        id: r.id,
        text: r.text,
        top: top,
        left: left,
        width: right - left,
        height: bottom - top,
      };
    }),
  };
};

const fetchWithCookie = (url: string, options?: RequestInit): Promise<Response> => {
  return fetch(url, {...options, credentials: 'include'});
};
