import * as React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Home, FigureDescriber, Outro, NoMatch, OutroBatch
} from './components';
import { SentenceHome, SentenceVoteHome, SentenceAnnotator, SentenceVoter,
  SentenceTutorial } from './components/sentence';
import { TitleAnnotator, TitleHome, TitleVoteHome, TitleVoter } from './components/title';
import { AdminLogin, SlideList, AnnotationList, AnnotationViewer } from './components/admin';
import { ListAnnotator, ListHome, ListVoteHome, ListVoter } from './components/list';
import { FigureHome, FigureVoteHome, FigureAnnotator, FigureVoter } from './components/figure';

export const AppRouter: React.StatelessComponent<{}> = () => {
  return (
    <BrowserRouter>
      <div>
        <Switch>
          <Route exact={true} path="/" component={Home} />

          <Route path="/:batch/sentence/start" component={SentenceHome} />
          <Route path="/:batch/sentence/startVote" component={SentenceVoteHome} />
          <Route path="/:batch/sentence/annotate/:slideId" component={SentenceAnnotator} />
          <Route path="/:batch/sentence/vote/:slideId" component={SentenceVoter} />
          <Route path="/:batch/sentence/tutorial" component={SentenceTutorial} />

          <Route path="/:batch/title/start" component={TitleHome} />
          <Route path="/:batch/title/startVote" component={TitleVoteHome} />
          <Route path="/:batch/title/annotate/:slideId" component={TitleAnnotator} />
          <Route path="/:batch/title/vote/:slideId" component={TitleVoter} />

          <Route path="/:batch/list/start" component={ListHome} />
          <Route path="/:batch/list/startVote" component={ListVoteHome} />
          <Route path="/:batch/list/annotate/:slideId" component={ListAnnotator} />
          <Route path="/:batch/list/vote/:slideId" component={ListVoter} />

          <Route path="/:batch/figure/start" component={FigureHome} />
          <Route path="/:batch/figure/startVote" component={FigureVoteHome} />
          <Route path="/:batch/figure/annotate/:slideId" component={FigureAnnotator} />
          <Route path="/:batch/figure/vote/:slideId" component={FigureVoter} />

          <Route path="/:batch/bye/:code" component={OutroBatch} />

          <Route path="/annotate/:slideId/bye" component={Outro} />
          <Route path="/annotate/:slideId/figure" component={FigureAnnotator} />
          <Route path="/annotate/:slideId/figureType" component={FigureDescriber} />
          <Route path="/admin/login" component={AdminLogin} />
          <Route exact={true} path="/admin/:batch/slides" component={SlideList} />
          <Route path="/admin/:batch/annotations" exact={true} component={AnnotationList} />
          <Route path="/admin/:batch/annotation/:slideId" component={AnnotationViewer} />
          <Route component={NoMatch} />
        </Switch>
      </div>
    </BrowserRouter>
  );
};
