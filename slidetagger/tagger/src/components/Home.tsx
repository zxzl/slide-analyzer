import * as React from 'react';
import Button from 'react-bootstrap-button-loader';
import { getNextURL } from '../workflow';
import { resetSession, fetchNewSlideId } from '../agent';

interface Props {
  history: {
    push: Function;
  };
}

interface State {
  loading: boolean;
}

export class Home extends React.Component<Props, State> {
  state = { loading: false };

  componentDidMount() {
    resetSession();
  }

  goToNewSlide = (): void => {
    this.setState({loading: true});
    const nextStage = getNextURL('');
    fetchNewSlideId().then(slideId => {
      this.props.history.push(`/annotate/${slideId}/${nextStage}`);
    });
  }

  render() {
    return (
      <div className="container">
        <div className="jumbotron">
          <h1 className="display-3">Semantic labeling of a slide</h1>
          <p className="lead">
          In this task, you will label a slide according to their roles, such as title, list, paragrah, and figures.</p>
          <hr className="my-4" />
          <p>You will follow 4 steps, and each step will be provided with detailed instructions.</p>
          <p>We encourage you to do the task using
            <a href="https://www.google.com/chrome/browser/desktop/index.html"> Google Chrome</a>,
            with the screen resolution larger than 1024 * 700.
          </p>
          <p className="lead">
            <Button
              className="btn-lg"
              bsStyle="primary"
              loading={this.state.loading}
              onClick={this.goToNewSlide}
            >
              Start
            </Button>
            {/* <button
              className="btn btn-primary btn-lg"
              onClick={this.goToNewSlide}
            >
              Start
            </button> */}
          </p>
        </div>
      </div >
    );
  }
}
