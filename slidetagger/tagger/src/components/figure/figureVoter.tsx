import * as React from 'react';
import { Button } from 'react-bootstrap';

import { OCRResult, RouteProps } from '../../models';
import { fetchNextVote, fetchAnnotation, fetchSlide, postVote, AnnotationResponse } from '../../agent';

import { Breadcrumb } from '../breadcrumb';
import { AnnotationRenderer } from '../admin';

interface State {
  slide?: OCRResult;
  annotation?: AnnotationResponse;
  correct: boolean;
}

export class FigureVoter extends React.Component<RouteProps, State> {
  constructor(props: RouteProps) {
    super(props);
    this.submitVote = this.submitVote.bind(this);
    this.state = {
      correct: false,
    };
  }

  componentDidMount() {
    const annotationId = this.props.match.params.slideId || 11;
    this.loadAnnotation(annotationId);
  }

  async loadAnnotation(_id: number) {
    const annotation  = await fetchAnnotation(_id);
    const slide = await fetchSlide(annotation.ocr_id);
    this.setState({
      slide,
      annotation,
    });
  }

  updateVote = (e) => this.setState({correct: e.target.value === 'correct'});

  async submitVote() {
    const { batch } = this.props.match.params;
    const { annotation, correct } = this.state;
    if (!annotation)  { return; }

    await postVote(annotation.id, correct);
    const { annotation_id: nextId, finished, code } = await fetchNextVote('figure', batch);
    if (finished) {
      this.props.history.push(`/${batch}/bye/${code}`);
    } else {
      this.props.history.push(`/${batch}/figure/vote/${nextId}`);
      this.loadAnnotation(nextId);
    }
  }

  renderVote = () => {
    const { correct } = this.state;
    return (
      <div>
        <input type="radio" checked={correct} onChange={this.updateVote} value="correct" /> Correct
        <br />
        <input type="radio" checked={!correct} onChange={this.updateVote} value="incorrect" /> Incorrect
        <br />
        <Button onClick={this.submitVote}>Submit</Button>
      </div>
    );
  }

  render() {
    if (!this.state.slide) {
      return <h1>Loading</h1>;
    }
    const { slide, annotation } = this.state;
    if (slide && annotation) {
      return (
        <div className="container">
          <Breadcrumb currentStage="Check whether figures are grouped correctly" />
          <h1>Please determine whether selected areas are figures in this slide</h1>
          <AnnotationRenderer
            slide={slide}
            annotation={annotation}
          />
          {this.renderVote()}
        </div>
      );
    }
    return 'Loading';
  }
}
