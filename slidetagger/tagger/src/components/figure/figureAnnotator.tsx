import * as React from 'react';
import Button from 'react-bootstrap-button-loader';

import { OCRResult, BBox, RouteProps } from '../../models';
import { fetchSlide, fetchSlideAndCount, postAnnotationNoCallback } from '../../agent';

import { Container, ControlWrapper, ExampleImageWrapper } from '../baseAnnotator';
import { Breadcrumb } from '../breadcrumb';
import { ImagePlotter } from '../imagePlotter';
import { Box } from '../imagePlotter/box';
import { GroupRow } from '../imagePlotter/groupRow';
import { FeedbackButton } from '../feedbackButton';
import { InstructionWrapper } from '../instructionWrapper';

interface State {
  slide?: OCRResult;
  figureAreas: {
    box: BBox;
    id: number;
  }[];
  loading: boolean;
}

export class FigureAnnotator extends React.Component<RouteProps, State> {

  private nextAreaId: number;

  constructor(props: RouteProps) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.nextAreaId = 0;
    this.state = {
      slide: undefined,
      figureAreas: [],
      loading: false,
    };
  }

  public async componentDidMount() {
    const slideId = this.props.match.params.slideId || 11;
    this.loadSlide(slideId);
  }

  loadSlide = (slideId: number): void => {
    fetchSlide(slideId).then(slide => {
      this.setState({
        slide,
        figureAreas: [],
        loading: false,
      });
    });
  }

  handleSelect = (box: BBox): void => {
    const newAreas = this.state.figureAreas.slice();
    newAreas.push({ box, id: this.nextAreaId });
    this.nextAreaId += 1,
    this.setState({
      figureAreas: newAreas,
    });
  }

  deleteArea = (i: number): void => {
    const { figureAreas } = this.state;
    const newAreas = figureAreas.slice();
    const areaIndex = figureAreas.findIndex(area => area.id === i);
    newAreas.splice(areaIndex, 1);
    this.setState({
      figureAreas: newAreas,
    });
  }

  async handleSubmit() {
    const { batch } = this.props.match.params;
    const { figureAreas } = this.state;
    const slideId = this.props.match.params.slideId;
    const numAreas = figureAreas.length;
    if (slideId && confirm(`Do you want to submit ${numAreas} areas?`)) {
      this.setState({loading: true});

      const success = await postAnnotationNoCallback(slideId, 'figure', figureAreas, batch);
      if (success) {
        const { slide_id, finished, code } = await fetchSlideAndCount(batch, 'figure');
        if (finished) {
          this.props.history.push(`/${batch}/bye/${code}`);

        } else {
          this.props.history.push(`/${batch}/figure/annotate/${slide_id}`);
          this.loadSlide(slide_id);
        }
      }
    }
  }

  render() {
    const { slide, figureAreas, loading } = this.state;
    if (!slide) {
      return(<h1>Loading</h1>);
    }
    return (
      <div className="container">
        <Breadcrumb currentStage="Finding other elements in slide" />
        <h1>Find supplementary elements</h1>
        <InstructionWrapper id="figure">
          <div className="alert alert-success" role="alert">
            <strong>Instruction</strong>
            <ul>
              <li>Please drag bounding boxes for <strong>figures</strong>,
                <strong> equations</strong>, <strong>graphs</strong>,
                <strong> charts</strong>, <strong>codes</strong> and <strong>table</strong> outside texts.
              </li>
              <li>Please include <strong>caption</strong> for figures</li>
              <li>
                Please <strong>do not include</strong> icon, logo, graphic from
              <strong> slide template</strong>
              </li>
              <li>
                If there is <strong>no figure</strong>, please <strong>do not select anything</strong>,
                and go to next task
              </li>
            </ul>
            <strong>Examples</strong>
            <ExampleImageWrapper className="row">
              <img src={process.env.PUBLIC_URL + '/img/figure/1.png'} />
              <img src={process.env.PUBLIC_URL + '/img/figure/2.png'} />
              <img src={process.env.PUBLIC_URL + '/img/figure/3.png'} />
              <img src={process.env.PUBLIC_URL + '/img/figure/4.png'} />
              <img src={process.env.PUBLIC_URL + '/img/figure/5.png'} />
              <img src={process.env.PUBLIC_URL + '/img/figure/6.png'} />
            </ExampleImageWrapper>
          </div>
        </InstructionWrapper>
        <Container id="plotter-wrapper">
          <ImagePlotter
            slide={slide}
            handleSelect={this.handleSelect}
          />
          {figureAreas.map((area, index) =>
            <Box key={index} box={area.box} group={area.id} />
          )}
          <ControlWrapper>
            <h5>Selected Areas</h5>
            {figureAreas.map((area, i) => (
              <GroupRow
                key={area.box.text + i}
                groupId={area.id}
                removeGroup={this.deleteArea}
                text={''}
              />
            ))}
            <Button
              bsStyle="success"
              className="btn-lg"
              loading={loading}
              onClick={this.handleSubmit}
            >
              Submit
            </Button>
            <FeedbackButton id={slide.id} stage={'figure'} />
          </ControlWrapper>
        </Container>
      </div>
    );
  }
}
