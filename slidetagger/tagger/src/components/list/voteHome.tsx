import * as React from 'react';
import Button from 'react-bootstrap-button-loader';

import { resetSession, fetchNextVote } from '../../agent';
import { RouteProps } from '../../models';

interface State {
  loading: boolean;
}

export class ListVoteHome extends React.Component<RouteProps, State> {
  constructor(props: RouteProps) {
    super(props);
    this.goToNewVote = this.goToNewVote.bind(this);
    this.state = { loading: false };
  }

  componentDidMount() {
    resetSession();
  }

  async goToNewVote() {
    const { batch } = this.props.match.params;
    this.setState({ loading: true });
    const { annotation_id } = await fetchNextVote('list', batch);
    this.props.history.push(`/${batch}/list/vote/${annotation_id}`);
  }

  render() {
    return (
      <div className="container">
        <div className="jumbotron">
          <h1 className="display-3">Check other's annotation</h1>
          <p className="lead">
            In this task, you will check list in a slide, found by other Turkers</p>
          <hr className="my-4" />
          <p>We encourage you to do the task using
            <a href="https://www.google.com/chrome/browser/desktop/index.html"> Google Chrome</a>,
            with the screen resolution larger than 1024 * 700.
          </p>
          <p>Your task activity will be recorded by <a href="https://www.hotjar.com/">Hotjar </a>
          for analyzing and improving task efficiency</p>
          <p className="lead">
            <Button
              className="btn-lg"
              bsStyle="primary"
              loading={this.state.loading}
              onClick={this.goToNewVote}
            >
              Start
            </Button>
          </p>
        </div>
      </div>
    );
  }
}
