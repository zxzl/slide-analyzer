import * as React from 'react';
import Button from 'react-bootstrap-button-loader';

import { fetchSlide, postAnnotationNoCallback, fetchSlideAndCount } from '../../agent';
import { RouteProps, OCRResult, BBox } from '../../models';

import { Container, ControlWrapper, ExampleImageWrapper } from '../baseAnnotator';
import { Breadcrumb } from '../breadcrumb';
import { ImagePlotter } from '../imagePlotter';
import { Box } from '../imagePlotter/box';
import { GroupRow } from '../imagePlotter/groupRow';
import { FeedbackButton } from '../feedbackButton';
import { InstructionWrapper } from '../instructionWrapper';

interface State {
  slide?: OCRResult;
  listAreas: {
    box: BBox;
    id: number;
  }[];
  loading: boolean;
}

export class ListAnnotator extends React.Component<RouteProps, State> {

  private nextAreaId: number;

  constructor(props: RouteProps) {
    super(props);
    this.nextAreaId = 0;
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      slide: undefined,
      listAreas: [],
      loading: false,
    };
  }

  public componentDidMount() {
    const slideId = this.props.match.params.slideId || 11;
    this.loadSlide(slideId);
  }

  loadSlide = (slideId: number): void => {
    fetchSlide(slideId).then(result => {
      this.setState({
        slide: result,
        loading: false,
        listAreas: [],
      });
    });
  }

  handleSelect = (box: BBox): void => {
    const newAreas = this.state.listAreas.slice();
    newAreas.push({ box, id: this.nextAreaId });
    this.nextAreaId += 1,
    this.setState({
      listAreas: newAreas,
    });
  }

  deleteArea = (i: number): void => {
    const { listAreas } = this.state;
    const newAreas = listAreas.slice();
    const areaIndex = listAreas.findIndex(area => area.id === i);
    newAreas.splice(areaIndex, 1);
    this.setState({
      listAreas: newAreas,
    });
  }

  async handleSubmit() {
    const { batch } = this.props.match.params;
    const slideId = this.props.match.params.slideId || 11;
    const numAreas = this.state.listAreas.length;
    if (confirm(`Do you want to submit ${numAreas} areas as list?`)) {
      this.setState({loading: true});

      const { listAreas } = this.state;
      const success = await postAnnotationNoCallback(slideId, 'list', listAreas, batch);
      if (success) {
        const { slide_id, finished, code } = await fetchSlideAndCount(batch, 'list');
        if (finished) {
          this.props.history.push(`/${batch}/bye/${code}`);

        } else {
          this.props.history.push(`/${batch}/list/annotate/${slide_id}`);
          this.loadSlide(slide_id);
        }
      }
    }
  }

  render() {
    if (!(this.state.slide)) {
      return (<h1>Loading</h1>);
    }
    const { slide, listAreas } = this.state;
    return(
      <div className="container">
        <Breadcrumb currentStage={'2. Find the list'} />
        <h1>Find all the lists in the slide</h1>
        <InstructionWrapper id="list">
          <div className="alert alert-success" role="alert">
            <strong>Instruction</strong>
            <ul>
              <li>Please drag lists with <strong>at least two items</strong>,
              <strong> including numbers or bullet </strong> or similar symbols.</li>
              <li>Even though there are not bullet points or number, please drag as list if you think so.</li>
              <li>For <strong>nested lists</strong>, please drag
                <strong>both outer</strong> lists and <strong>inner</strong> lists.
              </li>
              <li>
                If there is <strong>no list</strong>, press submit and move to next task.
              </li>
            </ul>
            <strong>Examples</strong>
            <ExampleImageWrapper className="row">
              <img src={process.env.PUBLIC_URL + '/img/list/1.png'} />
              <img src={process.env.PUBLIC_URL + '/img/list/2.png'} />
              <img src={process.env.PUBLIC_URL + '/img/list/3.png'} />
              <img src={process.env.PUBLIC_URL + '/img/list/4.png'} />
              <img src={process.env.PUBLIC_URL + '/img/list/6.png'} />
              <img src={process.env.PUBLIC_URL + '/img/list/7.png'} />
            </ExampleImageWrapper>
          </div>
        </InstructionWrapper>
        <Container id="plotter-wrapper">
          <ImagePlotter
            slide={slide}
            handleSelect={this.handleSelect}
            boxes={(listAreas.map((area, index) =>
              <Box
                key={index}
                box={area.box}
                group={area.id}
              />
            ))}
          />
          <ControlWrapper>
            <h5>Selected Areas</h5>
            {listAreas.map((area, i) => (
              <GroupRow
                key={area.box.text + i}
                groupId={area.id}
                removeGroup={this.deleteArea}
                text={''}
              />
            ))}
            <Button
              loading={this.state.loading}
              className="btn-lg"
              bsStyle="success"
              onClick={this.handleSubmit}
            >
              Submit
            </Button>
            <FeedbackButton id={slide.id} stage={'list'} />
          </ControlWrapper>
        </Container>
      </div>
    );
  }
}
