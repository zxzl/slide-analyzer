import * as React from 'react';
import { RouteProps } from '../models';

interface State {
  code: string;
}

export class OutroBatch extends React.Component<RouteProps, State> {
  constructor(props: RouteProps) {
    super(props);
    this.state = { code: ''};
  }

  componentDidMount() {
    const { code } = this.props.match.params;
    if (code) {
      this.setState({ code });
    }
  }

  renderResult = () => {
    const { code } = this.state;
    if (code) {
      return (
        <div>
          <h1 className="display-3">Thanks 🙏</h1>
          <p className="lead">Your token for this task is</p>
          <h3> {code}</h3>
          <hr />
        </div>
      );
    }
    return (
      <div>
        <h1 className="display-3">Error</h1>
        <hr className="lead"/>
        <p className="lead">Some tasks were not finished. Please visit link provided in the HIT again</p>
      </div>
    );
  }

  render() {
    return (
      <div className="container">
        <div className="jumbotron">
          {this.renderResult()}
        </div>
      </div>
    );
  }
}
