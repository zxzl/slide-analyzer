import * as React from 'react';

import { OCRResult, RouteProps } from '../../models';
import { fetchSlide, postAnnotationNoCallback, fetchSlideAndCount } from '../../agent';
import { groups2sentence } from '../../utils';

import { BaseAnnotator, BoxStatus } from '../baseAnnotator';
import { Breadcrumb } from '../breadcrumb';

import { SentenceInstruction } from './instruction';

interface State {
  slide?: OCRResult;
  boxStatus: BoxStatus[];
  groups: number[];
  loading: boolean;
}

export class SentenceAnnotator extends React.Component<RouteProps, State> {

  constructor(props: RouteProps) {
    super(props);
    this.saveGroups = this.saveGroups.bind(this);
    this.loadSlide = this.loadSlide.bind(this);
    this.state = {
      slide: undefined,
      boxStatus: [],
      groups: [],
      loading: false,
    };
  }

  async componentDidMount() {
    const slideId = this.props.match.params.slideId || 11;
    this.loadSlide(slideId);
  }

  async loadSlide(slideId: number) {
    const result = await fetchSlide(slideId);
    const numBox = result.boxes.length;
    this.setState({
      slide: result,
      boxStatus: new Array(numBox).fill(BoxStatus.Normal),
      groups: new Array(numBox).fill(-1),
      loading: false,
    });
  }

  updateBoxStatus = (newStatus: BoxStatus[]) => {
    this.setState({ boxStatus: newStatus });
  }

  updateGroups = (newGroups: number[]) => {
    this.setState({ groups: newGroups });
  }

  async saveGroups(groups: number[]) {
    const slideId = this.props.match.params.slideId || 11;
    const { batch } = this.props.match.params;
    const sentences = groups2sentence(groups);
    if (confirm(`Do you want to submit?`)) {
      this.setState({ loading: true });

      const success = await postAnnotationNoCallback(slideId, 'sentence', sentences, batch);
      if (success) {
        const { slide_id, finished, code } = await fetchSlideAndCount(batch, 'sentence');
        if (finished) {
          this.props.history.push(`/${batch}/bye/${code}`);
        } else {
          this.props.history.push(`/${batch}/sentence/annotate/${slide_id}`);
          this.loadSlide(slide_id);
        }
      }
    }
  }

  render() {
    if (!(this.state.slide)) {
      return (<h1>Loading</h1>);
    }
    const { slide, boxStatus, groups, loading } = this.state;
    return (
      <div className="container">
        <Breadcrumb currentStage={'Finding the text elements'} />
        <h1>Please group words into meaningful text</h1>
        <SentenceInstruction />
        <BaseAnnotator
          slide={slide}
          boxStatus={boxStatus}
          groups={groups}
          updateStatus={this.updateBoxStatus}
          updateGroups={this.updateGroups}
          saveGroups={this.saveGroups}
          stage={'sentence'}
          loading={loading}
        />
      </div>
    );
  }
}
