import * as React from 'react';

import { InstructionWrapper } from '../instructionWrapper';
import { ThumbnailWrapper, ExampleImage } from '../exampleImage';

export const SentenceInstruction: React.StatelessComponent<{}> = () => (
  <InstructionWrapper id={'sentence'}>
  <div className="alert alert-success" role="alert">
    <strong>Instruction</strong>
    <ul>
      <li>Please drag a group of words that make up
        <strong> a paragraph</strong>, or <strong>phrases</strong>
        if they are not in sentences.</li>
      <li>If words are in a <strong>same bullet point</strong>,
      please group them <strong>together </strong>(including bullet point or number).</li>
      <li>Do <strong>not</strong> group words that are <strong>irrelevent to the content</strong> of slide,
        such as words in footer of slide, words in slide template, etc.
      </li>
      <li>Please group words in <strong>title</strong> as well</li>
    </ul>
    <iframe
      src="https://giphy.com/embed/l49JEyMNyD1kjXokM"
      width="480"
      height="266"
      frameBorder="0"
      className="giphy-embed"
      allowFullScreen={true}
    />
    <strong>Examples</strong>
    <ThumbnailWrapper>
      <ExampleImage
        img_url={'img/sentence/1.png'}
        img_url_big={'img/sentence/1.png'}
        id={1}
        desc={'Words in a same bullet point are grouped together. \
          Words in figures are grouped into a paragraph or single phrase.'}
      />
      <ExampleImage
        img_url={'img/sentence/2.png'}
        img_url_big={'img/sentence/2.png'}
        id={2}
        desc={'Equations are not grouped, since they are not words.'}
      />
      <ExampleImage
        img_url={'img/sentence/3.png'}
        img_url_big={'img/sentence/3.png'}
        id={3}
        desc={''}
      />
      <ExampleImage
        img_url={'img/sentence/4.png'}
        img_url_big={'img/sentence/4.png'}
        id={4}
        desc={''}
      />
    </ThumbnailWrapper>
  </div>
  </InstructionWrapper>
);
