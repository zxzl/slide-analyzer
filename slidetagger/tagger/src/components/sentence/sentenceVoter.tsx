import * as React from 'react';
import { Button } from 'react-bootstrap';

import { OCRResult, RouteProps, Sentence } from '../../models';
import { fetchNextVote, fetchAnnotation, fetchSlide, postVote } from '../../agent';
import { sentence2groups } from '../../utils';

import { Breadcrumb } from '../breadcrumb';
import { Container } from '../baseAnnotator';
import { ImagePlotter } from '../imagePlotter/index';
import { BoxPlotter } from '../imagePlotter/boxPlotter';

import { SentenceInstruction } from './instruction';

interface State {
  slide?: OCRResult;
  annotation: Sentence[];
  annotation_id: number;
  correct: boolean;
}

export class SentenceVoter extends React.Component<RouteProps, State> {
  constructor(props: RouteProps) {
    super(props);
    this.submitVote = this.submitVote.bind(this);
    this.state = {
      slide: undefined,
      annotation: [],
      annotation_id: -1,
      correct: false,
    };
  }

  componentDidMount() {
    const annotationId = this.props.match.params.slideId || 11;
    this.loadAnnotation(annotationId);
  }

  async loadAnnotation(_id: number) {
    const { payload, ocr_id, id } = await fetchAnnotation(_id);
    const slide = await fetchSlide(ocr_id);
    if (payload.stage === 'sentence') {
      this.setState({
        slide: slide,
        annotation: payload.payload,
        annotation_id: id,
      });
    }
  }

  updateVote = (e) => this.setState({correct: e.target.value === 'correct'});

  async submitVote() {
    const { annotation_id, correct } = this.state;
    const { batch } = this.props.match.params;

    await postVote(annotation_id, correct);
    const { annotation_id: nextId, finished, code } = await fetchNextVote('sentence', batch);
    if (finished) {
      this.props.history.push(`/${batch}/bye/${code}`);
    } else {
      this.props.history.push(`/${batch}/sentence/vote/${nextId}`);
      this.loadAnnotation(nextId);
    }
  }

  renderVote = () => {
    const { correct } = this.state;
    return (
      <div>
        <input type="radio" checked={correct} onChange={this.updateVote} value="correct" /> Correct
        <br />
        <input type="radio" checked={!correct} onChange={this.updateVote} value="incorrect" /> Incorrect
        <br />
        <Button onClick={this.submitVote}>Submit</Button>
      </div>
    );
  }

  render() {
    if (!this.state.slide) {
      return <h1>Loading</h1>;
    }
    const { slide, annotation } = this.state;
    return (
      <div className="container">
        <Breadcrumb currentStage="Check whether sentences are grouped correctly" />
        <h1>Please determine whether words are grouped into adequate groups</h1>
        <h2>Please make sure that list items are grouped separately</h2>
        <SentenceInstruction />
        <Container id="plotter-wrapper">
          <ImagePlotter slide={slide} />
          <BoxPlotter
            boxes={slide.boxes}
            showBorder={false}
            boxGroups={sentence2groups(annotation, slide.boxes.length)}
          />
        </Container>
        {this.renderVote()}
      </div>
    );
  }

}
