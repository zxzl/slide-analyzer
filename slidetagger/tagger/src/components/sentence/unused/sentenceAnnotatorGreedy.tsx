// import * as React from 'react';
// import { Button, ButtonToolbar } from 'react-bootstrap';

// import { OCRResult, RouteProps, Sentence } from '../models';
// import { fetchSlide, fetchGuessedSentence, postAnnotation } from '../agent';
// import { groups2sentence, mergeSentences } from '../utils';
// import { getNextURL } from '../workflow';

// import { BoxStatus, Container } from './baseAnnotator';
// import { ImagePlotter } from './imagePlotter/index';
// import { BoxPlotter } from './imagePlotter/boxPlotter';
// import { Breadcrumb } from './breadcrumb';
// import { FeedbackButton } from './feedbackButton';
// import { InstructionWrapper } from './instructionWrapper';
// import { ThumbnailWrapper, ExampleImage } from './exampleImage';

// interface State {
//   slide?: OCRResult;
//   lines: Sentence[];
//   currentLines: number[];
//   questionLine: number;
//   savedLines: number[][];
// }

// export class SentenceAnnotatorGreedy extends React.Component<RouteProps, State> {

//   constructor(props: RouteProps) {
//     super(props);
//     this.state = {
//       slide: undefined,
//       lines: [],
//       currentLines: [],
//       savedLines: [],
//       questionLine: 0,
//     };
//   }

//   async componentDidMount() {
//     const slideId = this.props.match.params.slideId;
//     const [ slide, groups ] = await Promise.all([
//       fetchSlide(slideId),
//       fetchGuessedSentence(slideId),
//     ]);
//     const sentences = groups2sentence(groups.annotations);
//     this.setState({
//       slide,
//       lines: sentences,
//       currentLines: [0],
//       questionLine: 1,
//     });
//   }

//   /**
//    * On annotations
//    */
//   resetAnnotation = () => this.setState({currentLines: [0], questionLine: 1});

//   calculateBoxStatus = (slide: OCRResult) => {
//     const emptyStatus = slide.boxes.map(_ => BoxStatus.Normal);
//     const { currentLines, questionLine, lines } = this.state;
//     for (let lineId of currentLines) {
//       for (let wordId of lines[lineId].words) {
//         emptyStatus[wordId] = BoxStatus.Selected;
//       }
//     }
//     if (questionLine !== -1 && questionLine < lines.length) {
//       for (let wordId of lines[questionLine].words) {
//         emptyStatus[wordId] = BoxStatus.Selected;
//       }
//     }
//     return emptyStatus;
//   }

//   calculateGroups = (numWord: number): number[] => {
//     const initGroup = new Array(numWord).fill(-1);
//     const { lines } = this.state;
//     this.state.savedLines.forEach((lineIndices, i) =>
//       lineIndices.forEach(lineIndex => {
//         lines[lineIndex].words.forEach(
//           wordId => initGroup[wordId] = i);
//       })
//     );
//     return initGroup;
//   }

//   /**
//    * Proceeding functions
//    */
//   merge = () => {
//     const { currentLines, questionLine, savedLines, lines} = this.state;
//     if (questionLine + 1 === lines.length) {
//       this.setState({
//         currentLines: [],
//         questionLine: -1,
//         savedLines: [...savedLines, [...currentLines, questionLine]]
//       });
//       return;
//     }
//     this.setState({
//       currentLines: [...currentLines, questionLine],
//       questionLine: questionLine + 1,
//     });
//   }

//   split = () => {
//     const { currentLines, questionLine, savedLines, lines } = this.state;
//     if (questionLine + 1 === lines.length) {
//       this.setState({
//         currentLines: [],
//         questionLine: -1,
//         savedLines: [...savedLines, currentLines, [questionLine]]
//       });
//       return;
//     }
//     this.setState({
//       currentLines: [questionLine],
//       questionLine: questionLine + 1,
//       savedLines: [...savedLines, currentLines],
//     });
//   }

//   skip = () => {
//     const { questionLine, lines } = this.state;
//     if (questionLine + 1 === lines.length) {
//       this.setState({
//         currentLines: [],
//         questionLine: -1,
//       });
//       return;
//     }
//     this.setState({
//       questionLine: questionLine + 1,
//     });
//   }

//   handleSubmit = (): void => {
//     const slideId = this.props.match.params.slideId;
//     const { lines, savedLines } = this.state;
//     const sentences = mergeSentences(
//       savedLines.map(ids => ids.map(id => lines[id])));
//     postAnnotation(slideId, 'sentence', sentences, () => {
//       const nextURL = getNextURL(this.props.match.url);
//       this.props.history.push(nextURL);
//     });
//   }

//   renderSubmit = (): JSX.Element => (
//     <ButtonToolbar>
//       <br /> <br />
//       <Button bsSytle="info" onClick={this.resetAnnotation}>
//         Do again
//       </Button>
//       <Button bsStyle="success" onClick={this.handleSubmit}>
//         Proceed to next step
//       </Button>
//     </ButtonToolbar>
//   )

//   renderYesNo = (): JSX.Element => (
//     <div>
//       <h3>Are selected words belong to a same paragraph, or phrase, or list item ?</h3>
//         <Button bsStyle="success" onClick={this.merge}>Yes</Button>
//         <br /><br />
//         <Button bsStyle="warning" onClick={this.split}>
//           No they should NOT be grouped together
//         </Button>
//         <br /><br />
//         <Button onClick={this.skip}>
//           No, the second one is not valid word
//         </Button>
//         <Button bsSytle="info" onClick={this.resetAnnotation}>
//           Reset current groupings
//       </Button>
//     </div>
//   )

//   render() {
//     if (!(this.state.slide)) {
//       return <h1>Loading</h1>;
//     }
//     const { slide } = this.state;

//     const finished = this.state.questionLine === -1;
//     const boxGroups = finished ? this.calculateGroups(slide.boxes.length) : undefined;
//     const boxStatus = this.calculateBoxStatus(slide);

//     return (
//       <div className="container">
//         <Breadcrumb currentStage={'Finding the ext elements'} />
//         <h1>Please group words into meaningful unit</h1>
//         <InstructionWrapper id="sentence">
//         <strong>Examples</strong>
//           <ThumbnailWrapper>
//             <ExampleImage
//               img_url={'img/sentence/1.png'}
//               img_url_big={'img/sentence/1.png'}
//               id={1}
//               desc={'Words in a same bullet point are grouped together. \
//               Words in figures are grouped into a paragraph or single phrase.'}
//             />
//             <ExampleImage
//               img_url={'img/sentence/2.png'}
//               img_url_big={'img/sentence/2.png'}
//               id={2}
//               desc={'Equations are not grouped, since they are not words.'}
//             />
//             <ExampleImage
//               img_url={'img/sentence/3.png'}
//               img_url_big={'img/sentence/3.png'}
//               id={3}
//               desc={''}
//             />
//             <ExampleImage
//               img_url={'img/sentence/4.png'}
//               img_url_big={'img/sentence/4.png'}
//               id={4}
//               desc={''}
//             />
//           </ThumbnailWrapper>
//         </InstructionWrapper>
//         <Container id="plotter-wrapper">
//           <ImagePlotter slide={slide} />
//           <BoxPlotter
//             boxes={slide.boxes}
//             boxGroups={boxGroups}
//             boxStatus={boxStatus}
//             showBorder={false}
//           />
//         </Container>
//         {
//           finished ?
//           this.renderSubmit() :
//           this.renderYesNo()
//         }
//         <hr />
//         <FeedbackButton id={slide.id} stage={'figure'} />
//       </div>
//     );
//   }

// }
