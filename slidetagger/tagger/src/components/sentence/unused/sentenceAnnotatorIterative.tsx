// import * as React from 'react';
// import { Button, ButtonToolbar } from 'react-bootstrap';

// import { OCRResult, RouteProps } from '../models';
// import { fetchSlide, fetchGuessedSentenceFrom, postAnnotation } from '../agent';
// import { groups2sentence } from '../utils';
// import { getNextURL } from '../workflow';

// import { BoxStatus, Container } from './baseAnnotator';
// import { ImagePlotter } from './imagePlotter/index';
// import { BoxPlotter } from './imagePlotter/boxPlotter';
// import { Breadcrumb } from './breadcrumb';
// import { FeedbackButton } from './feedbackButton';
// import { InstructionWrapper } from './instructionWrapper';
// import { ThumbnailWrapper, ExampleImage } from './exampleImage';

// interface State {
//   slide?: OCRResult;
//   boxStatus: BoxStatus[];
//   groups: number[];
//   starting_word_id: number;
//   canDrag: boolean;
// }

// export class SentenceAnnotatorIterative extends React.Component<RouteProps, State> {
//   private lastProceeded: number = 0;

//   constructor(props: RouteProps) {
//     super(props);
//     this.proceed = this.proceed.bind(this);
//     this.state = {
//       slide: undefined,
//       boxStatus: [],
//       groups: [],
//       starting_word_id: 0,
//       canDrag: false,
//     };
//   }

//   async componentDidMount() {
//     const slideId = this.props.match.params.slideId;
//     const [ slide, firstGuess ] = await Promise.all([
//       fetchSlide(slideId),
//       fetchGuessedSentenceFrom(slideId, 0),
//     ]);
//     const numBox = slide.boxes.length;
//     const sentenceIds = firstGuess.annotations[0].ids;
//     const boxStatus = new Array(numBox).fill(BoxStatus.Normal);
//     for (let id of sentenceIds) {
//       boxStatus[id] = BoxStatus.Selected;
//     }

//     this.setState({
//       slide,
//       boxStatus,
//       groups: new Array(numBox).fill(-1),
//     });
//   }

//   async proceed(save: boolean = true) {
//     if (Date.now() - this.lastProceeded < 1000) {
//       alert('You are working too fast. Keep Calm :D');
//       return;
//     }
//     const slideId = this.props.match.params.slideId;
//     const { boxStatus } = this.state;

//     // Save current selection
//     if (save) {
//       this.setState(prevState => {
//         const {groups} = prevState;
//         const newGroupId = Math.max(...groups) + 1;
//         const newGroups = groups.map((v, i) =>
//           (boxStatus[i] === BoxStatus.Selected) ? newGroupId : v
//         );
//         return { groups: newGroups, canDrag: false };
//       });
//     }

//     // Fetch next word group
//     const lastWordIdSelected = Math.max(
//       ...boxStatus.map((v, i) => v === BoxStatus.Selected ? i : -1)
//     );
//     const nextGuess = await fetchGuessedSentenceFrom(slideId, lastWordIdSelected + 1);
//     const newBoxStatus = boxStatus.map(() => BoxStatus.Normal);

//     if (nextGuess.annotations.length === 0) {
//       this.handleSubmit();
//       return;
//     }

//     for (let id of nextGuess.annotations[0].ids) {
//       newBoxStatus[id] = BoxStatus.Selected;
//     }
//     this.setState({
//       boxStatus: newBoxStatus,
//     });
//     this.lastProceeded = Date.now();
//   }

//   proceedWithoutSave = () => this.proceed(false);

//   getSelectedText = (slide: OCRResult) => {
//     const { boxStatus } = this.state;
//     const selectedTexts = slide.boxes
//     .filter((v, i) => boxStatus[v.id] === BoxStatus.Selected)
//     .map(b => b.text);
//     return selectedTexts.join(' ');
//   }

//   handleSubmit = (): void => {
//     const slideId = this.props.match.params.slideId;
//     const sentences = groups2sentence(this.state.groups);
//     postAnnotation(slideId, 'sentence', sentences, () => {
//       const nextURL = getNextURL(this.props.match.url);
//       this.props.history.push(nextURL);
//     });
//   }

//   startDrag = () => {
//     this.setState({ canDrag: true });
//   }

//   cancelDrag = () => {
//     this.setState({ canDrag: false });
//   }

//   updateIntersection = (ids: number[]): void => {
//     const newStatus = this.state.boxStatus.map((status, i) =>
//       (ids.indexOf(i) === -1) ? BoxStatus.Normal : BoxStatus.Intersected);
//     this.setState({ boxStatus: newStatus });
//   }

//   updateSelection = (ids: number[]): void => {
//     const newStatus = this.state.boxStatus.map((status, i) => (
//       (ids.indexOf(i) === -1) ? BoxStatus.Normal : BoxStatus.Selected
//     ));
//     this.setState({ boxStatus: newStatus });
//   }

//   resetSelection = (): void => this.setState({
//     boxStatus: this.state.boxStatus.map(() => BoxStatus.Normal)
//   })

//   renderYesNo = (slide) => {
//     const selectedTexts = this.getSelectedText(slide);
//     return (
//       <div>
//         <h3>Is selected words <code>{selectedTexts}</code>
//          making meaningful paragraph, or phrase, or list item ?</h3>
//           <Button bsStyle="success" onClick={this.proceed}>Yes</Button>
//           <br /><br />
//           <Button bsStyle="warning" onClick={this.startDrag}>
//             No, grouping should be changed
//           </Button>
//           <br /><br />
//           <Button onClick={this.proceedWithoutSave}>
//             No, they are not words
//           </Button>
//       </div>
//     );
//   }

//   renderDragInstruction = () => (
//     <div>
//       <h3>Drag words including this word, to make complete paragraph, phrase, or list item</h3>
//       <ButtonToolbar>
//         <Button onClick={this.cancelDrag}>Go back</Button>
//         <Button bsStyle="info" onClick={this.resetSelection}>Reset Selection</Button>
//         <Button bsStyle="success" onClick={this.proceed}>Save selection</Button>
//       </ButtonToolbar>
//     </div>
//   )

//   render() {
//     if (!(this.state.slide)) {
//       return <h1>Loading</h1>;
//     }

//     const { slide, boxStatus, canDrag } = this.state;
//     return (
//       <div className="container">
//         <Breadcrumb currentStage={'Finding the text elements'} />
//         <h1>Please group words into meaningful unit</h1>
//         <InstructionWrapper id="sentence">
//         <strong>Examples</strong>
//         <ThumbnailWrapper>
//           <ExampleImage
//             img_url={'img/sentence/1.png'}
//             img_url_big={'img/sentence/1.png'}
//             id={1}
//             desc={'Words in a same bullet point are grouped together. \
//             Words in figures are grouped into a paragraph or single phrase.'}
//           />
//           <ExampleImage
//             img_url={'img/sentence/2.png'}
//             img_url_big={'img/sentence/2.png'}
//             id={2}
//             desc={'Equations are not grouped, since they are not words.'}
//           />
//           <ExampleImage
//             img_url={'img/sentence/3.png'}
//             img_url_big={'img/sentence/3.png'}
//             id={3}
//             desc={''}
//           />
//           <ExampleImage
//             img_url={'img/sentence/4.png'}
//             img_url_big={'img/sentence/4.png'}
//             id={4}
//             desc={''}
//           />
//         </ThumbnailWrapper>
//         </InstructionWrapper>
//         <Container id="plotter-wrapper">
//           <ImagePlotter
//             slide={slide}
//             disableSelection={!canDrag}
//             updateSelection={this.updateSelection}
//             updateIntersection={this.updateIntersection}
//           />
//           <BoxPlotter
//             boxes={slide.boxes}
//             boxStatus={boxStatus}
//             showBorder={false}
//           />
//         </Container>
//         { canDrag ?
//           this.renderDragInstruction() :
//           this.renderYesNo(slide)
//         }
//         <hr/>
//         <FeedbackButton id={slide.id} stage={'figure'} />
//       </div>
//     );
//   }

// }
