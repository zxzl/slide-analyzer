// import * as React from 'react';
// import { Button, FormGroup, Radio } from 'react-bootstrap';

// import { OCRResult, Sentence, RouteProps } from '../models';
// import * as utils from '../utils';
// import { fetchSlide, fetchAnnotationByStage, postAnnotation } from '../agent';

// import { Breadcrumb } from './breadcrumb';
// import { BoxStatus, Container } from './baseAnnotator';
// import { ImagePlotter } from './imagePlotter/index';
// import { BoxPlotter } from './imagePlotter/boxPlotter';
// import { getNextURL } from '../workflow';

// interface State {
//   selected?: SentenceType;
//   slide?: OCRResult;
//   sentences: Sentence[];
//   sentenceIndex: number;
// }

// enum SentenceType {
//   title = 'title',
//   sectionTitle = 'sectionTitle',
//   caption = 'caption',
//   listItem = 'listItem',
//   etc = 'etc',
// }

// const typeExplanation = {
//   'title': 'This text is title of the slide',
//   'sectionTitle': 'This text represents a new section in the slide.',
//   'caption': 'This text describes the content of figure, table, chart, etc.',
//   'listItem': 'This text belongs to a list.',
//   'etc': 'None of above options are suitable for this text.'
// };

// export class SentenceDescriber extends React.Component<RouteProps, State> {
//   private annotations: {[id: number]: SentenceType} = {};

//   constructor(props: RouteProps) {
//     super(props);
//     this.state = {
//       slide: undefined,
//       sentences: [],
//       sentenceIndex: 0,
//     };
//   }

//   async componentDidMount() {
//     const slideId = this.props.match.params.slideId;
//     const [slide, resp] = await Promise.all([
//       fetchSlide(slideId),
//       fetchAnnotationByStage(slideId, 'sentence'),
//     ]);
//     if (resp.stage === 'sentence' && resp.annotations.length > 0) {
//       this.setState({slide, sentences: resp.annotations});
//     } else {
//       this.goToNextPage();
//     }
//   }

//   goToNextPage = (): void => {
//     const nextURL = getNextURL(this.props.match.url);
//     this.props.history.push(nextURL);
//   }

//   handleSelect = (e): void => {
//     this.setState({
//       selected: e.target.value,
//     });
//   }

//   handleSubmit = (e): void => {
//     e.preventDefault();
//     const slideId = this.props.match.params.slideId;
//     const {sentences, sentenceIndex, selected } = this.state;
//     const isLastSentence = sentenceIndex === sentences.length - 1;
//     if (!selected) {
//       alert('If you find trouble choosing correct type, please select etc');
//       return;
//     }
//     this.annotations[sentences[sentenceIndex].id] = selected;
//     if (isLastSentence) {
//         postAnnotation(slideId, 'sentenceType', this.annotations, () => {
//           this.goToNextPage();
//         });
//     } else {
//       this.setState({
//         sentenceIndex: sentenceIndex + 1,
//         selected: undefined,
//       });
//     }
//   }

//   render() {
//     const { slide, sentences, sentenceIndex, selected } = this.state;
//     if (!slide) {
//       return (<div>No Slide</div>);
//     }

//     const progress = `${sentenceIndex + 1} / ${sentences.length}`;
//     let sentenceTypes: string[] = utils.getEnumStrings(SentenceType);
//     const currentSentence = sentences[sentenceIndex];
//     const boxStatus = slide.boxes.map((b, i) =>
//       currentSentence.words.indexOf(i) !== -1 ? BoxStatus.Selected : BoxStatus.Normal);
//     return (
//       <div className="container">
//         <Breadcrumb currentStage="4.1 Tell us the type of figure" />
//         <h1>{`Which word best describes the selected text? (${progress})`}</h1>
//         <Container id="plotter-wrapper">
//           <ImagePlotter
//             slide={slide}
//           />
//           <BoxPlotter
//             boxes={slide.boxes}
//             boxStatus={boxStatus}
//             boxGroups={slide.boxes.map(_ => -1)}
//             showBorder={false}
//           />
//         </Container>
//         <form onSubmit={this.handleSubmit}>
//           <FormGroup>
//             {sentenceTypes.map(v => (
//               <Radio
//                 name="radioGroup"
//                 key={v}
//                 value={v}
//                 checked={v === selected}
//                 onClick={this.handleSelect}
//               >
//                 <strong>{v}</strong>{` (${typeExplanation[v]})`}
//               </Radio>
//             ))}
//           </FormGroup>
//           <Button
//             bsStyle="success"
//             className="btn-lg"
//             type="submit"
//           >
//             Submit
//           </Button>
//         </form>
//       </div>
//     );
//   }
// }
