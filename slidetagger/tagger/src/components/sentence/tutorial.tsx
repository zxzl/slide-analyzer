import * as React from 'react';
import { Button } from 'react-bootstrap';
import * as urlJoin from 'url-join';

import { OCRResult, RouteProps } from '../../models';
import { fetchSlide, fetchSlideAndCount } from '../../agent';
// import { groups2sentence } from '../../utils';

import { BaseAnnotator, BoxStatus } from '../baseAnnotator';
import { Breadcrumb } from '../breadcrumb';

import { SentenceInstruction } from './instruction';

interface State {
  slide?: OCRResult;
  boxStatus: BoxStatus[];
  groups: number[];
  loading: boolean;
  stage: TutorialStage;
}

enum TutorialStage {
  EG1 = 0,
  CHECK1,
  EG2,
  CHECK2,
}

const TutorialStageInfo = {
  [TutorialStage.EG1]: {
    slideId: 777,
    imgPath: '777-sentence-answer.png',
    buttonMessage: 'Check Answer',
  },
  [TutorialStage.CHECK1]: {
    slideId: 777,
    imgPath: '777-sentence-answer.png',
    buttonMessage: 'try another slide',
  },
  [TutorialStage.EG2]: {
    slideId: 778,
    imgPath: '778-sentence-answer.png',
    buttonMessage: 'Check Answer',
  },
  [TutorialStage.CHECK2]: {
    slideId: 778,
    imgPath: '778-sentence-answer.png',
    buttonMessage: 'Move to real task',
  },
};

export class SentenceTutorial extends React.Component<RouteProps, State> {

  constructor(props: RouteProps) {
    super(props);
    this.proceed = this.proceed.bind(this);
    this.loadSlide = this.loadSlide.bind(this);
    this.state = {
      slide: undefined,
      boxStatus: [],
      groups: [],
      loading: false,
      stage: TutorialStage.EG1,
    };
  }

  async componentDidMount() {
    const slideId = TutorialStageInfo[this.state.stage].slideId;
    this.loadSlide(slideId);
  }

  async loadSlide(slideId: number) {
    const result = await fetchSlide(slideId);
    const numBox = result.boxes.length;
    this.setState({
      slide: result,
      boxStatus: new Array(numBox).fill(BoxStatus.Normal),
      groups: new Array(numBox).fill(-1),
      loading: false,
    });
  }

  updateBoxStatus = (newStatus: BoxStatus[]) => {
    this.setState({ boxStatus: newStatus });
  }

  updateGroups = (newGroups: number[]) => {
    this.setState({ groups: newGroups });
  }

  async proceed() {
    const { stage, slide } = this.state;
    // const { batch } = this.props.match.params;
    // const sentences = groups2sentence(groups);
    if (!slide || !confirm(`Do you want to proceed?`)) {
      return;
    }
    if (stage === TutorialStage.CHECK2) {
      this.goToNewSlide();
    } else {
      // await postAnnotationNoCallback(slide.id, 'sentence', sentences, batch);
      const nextStageIndex: number = stage + 1;
      this.setState({
        stage: nextStageIndex,
      });
      if (slide.id !== TutorialStageInfo[nextStageIndex].slideId) {
        this.loadSlide(TutorialStageInfo[nextStageIndex].slideId);
      }
    }
  }

  goToNewSlide = (): void => {
    const { batch } = this.props.match.params;
    fetchSlideAndCount(batch, 'sentence').then(({slide_id, count}) => {
      this.props.history.push(`/${batch}/sentence/annotate/${slide_id}`);
    });
  }

  renderTask(slide: OCRResult, boxStatus: BoxStatus[], groups: number[], stage: TutorialStage) {
    const buttonMessage = TutorialStageInfo[stage].buttonMessage;
    return (
      <BaseAnnotator
        slide={slide}
        boxStatus={boxStatus}
        groups={groups}
        updateStatus={this.updateBoxStatus}
        updateGroups={this.updateGroups}
        saveGroups={this.proceed}
        stage={'sentence'}
        loading={false}
        buttonMessage={buttonMessage}
        buttonHidden={(stage === TutorialStage.CHECK1 || stage === TutorialStage.CHECK2)}
      />
    );
  }

  renderAnswer(stage: TutorialStage) {
    const info = TutorialStageInfo[stage];
    const imgUrl = info.imgPath;
    return (
      <div>
        <h3>Compare your annotation with image below. Item of each list should be grouped separately</h3>
        <div
          className="image-wrapper"
          id="wrapper"
        >
          <img src={urlJoin(process.env.REACT_APP_IMAGE_BUCKET, imgUrl)} draggable={false} />
        </div>
        <Button bsStyle="success" onClick={this.proceed}>
           {info.buttonMessage}
        </Button>
        <hr />
      </div>
    );
  }

  render() {
    if (!(this.state.slide)) {
      return (<h1>Loading</h1>);
    }
    const { slide, boxStatus, groups, stage } = this.state;
    return (
      <div className="container">
        <Breadcrumb currentStage={'Tutorial'} />
        <h1>Please group words into meaningful text</h1>
        <SentenceInstruction />
        { (stage === TutorialStage.CHECK1 || stage === TutorialStage.CHECK2) &&
          this.renderAnswer(stage)
        }
        {this.renderTask(slide, boxStatus, groups, stage)}
      </div>
    );
  }
}
