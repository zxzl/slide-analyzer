import * as React from 'react';
import { Button } from 'react-bootstrap';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  margin-bottom: 10px;
  .alert {
    margin-bottom: 0;
  };
`;

interface Props {
  id: string;
}

interface State {
  hidden: Boolean;
}

export class InstructionWrapper extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      hidden: Boolean(localStorage.getItem('instruction' + props.id))
    };
  }

  toggleState = () => {
    const nextState = !(this.state.hidden);
    localStorage.setItem('instruction' + this.props.id, nextState ? 'true' : '');
    this.setState({
      hidden: nextState,
    });
  }

  render() {
    return (
      this.state.hidden ?
      (
        <StyledWrapper>
          <Button onClick={this.toggleState}>
            Show Instruction
          </Button>
        </StyledWrapper>
       ) :
       (
        <StyledWrapper>
          {this.props.children}
          <Button onClick={this.toggleState}>
            Hide Instruction
          </Button>
        </StyledWrapper>
      )
    );
  }
}
