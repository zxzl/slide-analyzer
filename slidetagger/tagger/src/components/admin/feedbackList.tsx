import * as React from 'react';
import { Feedback } from '../../models';

interface Props {
  feedbacks: Feedback[];
}

export const FeedbackList: React.StatelessComponent<Props> = ({feedbacks}) => {
  return (
    <div>
      <h3>Feedbacks</h3>
      {feedbacks.map((f, i) => (
        <pre key={i} style={{overflow: 'scroll'}}>
        {JSON.stringify(f, null, '\t')}
        </pre>
      ))}
    </div>
  );
};
