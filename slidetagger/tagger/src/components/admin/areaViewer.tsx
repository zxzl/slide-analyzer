import * as React from 'react';
import { Container } from '../baseAnnotator';
import { OCRResult, BBox } from '../../models';
import { ImagePlotter } from '../imagePlotter';
import { Box } from '../imagePlotter/box';

interface Props {
  slide: OCRResult;
  boxes: BBox[];
}

export const AreaViewer: React.StatelessComponent<Props> =  ({slide, boxes}) => {
  return (
    <div className="container">
      <Container id="plotter-wrapper">
        <ImagePlotter
          slide={slide}
          boxes={(boxes.map((box, index) =>
            <Box
              key={index}
              box={box}
              group={index}
            />
          ))}
        />
      </Container>
    </div >
  );
};
