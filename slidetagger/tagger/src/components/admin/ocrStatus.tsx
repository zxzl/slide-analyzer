import * as React from 'react';
import { Link } from 'react-router-dom';
import { Table } from 'react-bootstrap';
import { fetchOCRStatus, OCRStatus } from '../../agent';

interface State {
  rows: OCRStatus[];
}

export class OCRStatusPage extends React.Component<{}, State> {
  constructor(props: {}) {
    super(props);
    this.state = { rows: []};
  }

  async componentDidMount() {
    const status = await fetchOCRStatus();
    this.setState({ rows: status });
  }

  render() {
    const {rows} = this.state;
    return (
      <div className="container">
        <Table striped={true} bordered={true} condensed={true}>
          <thead>
            <tr>
              <th>Slide id</th>
              <th>Finished count</th>
              <th>Started Count</th>
            </tr>
          </thead>
          <tbody>
            {rows.map((s, i) => (
              <tr key={i}>
                <td>
                  <Link key={i} to={`/admin/viewer/${s.slide_id}`}>
                    {s.slide_id}
                  </Link>
                </td>
                <td>{s.finished}</td>
                <td>{s.started}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
     );
  }
}
