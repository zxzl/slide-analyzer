import * as React from 'react';
import { Link } from 'react-router-dom';
import { Table } from 'react-bootstrap';

import { fetchProcessedSlides, ProcessedSlide } from '../../agent';
import { RouteProps } from '../../models';

interface State {
  slides: ProcessedSlide[];
}

export class SlideList extends React.Component<RouteProps, State> {
  constructor(props: RouteProps) {
    super(props);
    this.state = { slides: []};
  }

  async componentDidMount() {
    const { batch } = this.props.match.params;
    const slides = await fetchProcessedSlides(batch);
    this.setState({ slides });
  }

  render() {
    const {slides} = this.state;
    const { batch } = this.props.match.params;
    return (
      <div className="container">
        <Table striped={true} bordered={true} condensed={true}>
          <thead>
            <tr>
              <th>Slide id</th>
              <th>Worker count</th>
              <th>Annotation Count</th>
              <th>Last update</th>
            </tr>
          </thead>
          <tbody>
            {slides.map((s, i) => (
              <tr key={i}>
                <td>
                  <Link key={i} to={`/admin/${batch}/viewer/${s.slide_id}`}>
                    {s.slide_id}
                  </Link>
                </td>
                <td>{s.worker_count}</td>
                <td>{s.annotation_count}</td>
                <td>{s.last_annotated}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
     );
  }
}
