import * as React from 'react';
import { Link } from 'react-router-dom';
import { Table } from 'react-bootstrap';

import { fetchAnnotations, AnnotationResponse } from '../../agent';
import { RouteProps } from '../../models';

interface State {
  annotations: AnnotationResponse[];
}

export class AnnotationList extends React.Component<RouteProps, State> {
  constructor(props: RouteProps) {
    super(props);
    this.state = { annotations: []};
  }

  async componentDidMount() {
    const { batch } = this.props.match.params;
    const annotations = await fetchAnnotations(batch);
    this.setState({ annotations });
  }

  render() {
    const {annotations} = this.state;
    const { batch } = this.props.match.params;
    return (
      <div className="container">
        <Table striped={true} bordered={true} condensed={true}>
          <thead>
            <tr>
              <th>Annotation id</th>
              <th>Stage</th>
              <th>Slide id</th>
              <th>Approved</th>
            </tr>
          </thead>
          <tbody>
            {annotations.map((s, i) => (
              <tr key={i}>
                <td>
                  <Link key={i} to={`/admin/${batch}/annotation/${s.id}`}>
                    {s.id}
                  </Link>
                </td>
                <td>{s.stage}</td>
                <td>{s.ocr_id}</td>
                <td>{String(s.approved)}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
     );
  }
}
