import * as React from 'react';
import { Button , FormControl } from 'react-bootstrap';
import { adminLogin } from '../../agent';

interface Props {
  history: {
    push: Function;
  };
}

interface State {
  userToken: string;
}

export class AdminLogin extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      userToken: ''
    };
  }

  handleChange = e => this.setState({userToken: e.target.value});
  handleSubmit = () => {
    adminLogin(this.state.userToken).then(bool => {
      if (bool) {
        this.props.history.push('/admin/viewer');
        return;
      }
      alert('check token');
    });
  }

  render() {
    return (
      <div>
        <FormControl
          type="text"
          value={this.state.userToken}
          placeholder="Enter token"
          onChange={this.handleChange}
        />
      <Button type="submit" onClick={this.handleSubmit}>
        Submit
      </Button>
      </div>
    );
  }

}
