import * as React from 'react';
import { BoxStatus, Container } from '../baseAnnotator';
import { OCRResult } from '../../models';
import { ImagePlotter } from '../imagePlotter';
import { BoxPlotter } from '../imagePlotter/boxPlotter';

interface Props {
  slide: OCRResult;
  boxStatus: BoxStatus[];
}

export const BoxViewer: React.StatelessComponent<Props> =  ({slide, boxStatus}) => {
  return (
    <div className="container">
      <Container id="plotter-wrapper">
        <ImagePlotter
          slide={slide}
        />
        <BoxPlotter
          boxes={slide.boxes}
          // boxStatus={boxStatus}
          boxStatus={slide.boxes.map(_ => BoxStatus.Normal)}
          boxGroups={boxStatus}
          showBorder={false}
        />
      </Container>
    </div >
  );
};
