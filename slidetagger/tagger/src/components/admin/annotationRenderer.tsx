import * as React from 'react';

import { OCRResult } from '../../models';
import { AnnotationResponse } from '../../agent';

import { BoxViewer } from './boxViewer';
import { AreaViewer } from './areaViewer';
import { sentence2groups } from '../../utils';

interface Props {
  slide: OCRResult;
  annotation: AnnotationResponse;
}

export const AnnotationRenderer: React.StatelessComponent<Props> = ({ slide, annotation }) => {
  return (
    <div>
      {annotation.payload.stage === 'title' &&
        <BoxViewer
          slide={slide}
          boxStatus={annotation.payload.payload}
        />
      }
      {annotation.payload.stage === 'sentence' &&
        <BoxViewer
          slide={slide}
          boxStatus={sentence2groups(annotation.payload.payload, slide.boxes.length)}
        />
      }
      {(annotation.payload.stage === 'list' ||
        annotation.payload.stage === 'figure') &&
        <AreaViewer
          slide={slide}
          boxes={annotation.payload.payload.map(p => p.box)}
        />
      }
    </div>
  );
};
