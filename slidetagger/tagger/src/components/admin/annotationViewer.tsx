import * as React from 'react';
import { RouteProps, OCRResult } from '../../models';
import { fetchSlide, fetchAnnotation, AnnotationResponse, fetchVoteResult, VoteResult } from '../../agent';

import { AnnotationRenderer } from './annotationRenderer';

interface State {
  slide?: OCRResult;
  voteResult?: VoteResult;
  annotation?: AnnotationResponse;
}

export class AnnotationViewer extends React.Component<RouteProps, State> {
  constructor(props: RouteProps) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    const { slideId: annotationId } = this.props.match.params;
    const annotation = await fetchAnnotation(annotationId);
    const [slide, voteResult] = await Promise.all([
      fetchSlide(annotation.ocr_id),
      fetchVoteResult(annotation.id),
    ]);
    this.setState({
      slide,
      voteResult,
      annotation
    });
  }

  render() {
    const { slide, annotation, voteResult} = this.state;
    if (slide && annotation && voteResult) {
      return (
        <div>
          <AnnotationRenderer
            slide={slide}
            annotation={annotation}
          />
          <div>
            {voteResult.upvote} /
            {voteResult.total_vote}
          </div>
        </div>
      );
    }
    return 'Loading';
  }
}
