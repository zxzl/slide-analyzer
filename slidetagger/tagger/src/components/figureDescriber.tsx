import * as React from 'react';

import { fetchSlide, fetchAnnotationByStage, postAnnotation } from '../agent';
import { BBox, OCRResult, RouteProps } from '../models';
import { getNextURL } from '../workflow';
import * as utils from '../utils';

import { Breadcrumb } from './breadcrumb';
import { Button, FormGroup, Radio } from 'react-bootstrap';
import { Container } from './baseAnnotator';
import { Box } from './imagePlotter/box';
import { ImagePlotter } from './imagePlotter/index';

interface State {

  selected?: FigureType;
  slide?: OCRResult;
  figures: {
    box: BBox;
    id: number;
  }[];
  figureIndex: number;
}

enum FigureType {
  picture = 'picture',
  diagram = 'diagram',
  chart = 'chart',
  table = 'table',
  etc = 'etc'
}

export class FigureDescriber extends React.Component<RouteProps, State> {
  private annotations: {[id: number]: FigureType} = {};

  constructor(props: RouteProps) {
    super(props);
    this.state = {
      slide: undefined,
      figures: [],
      figureIndex: 0,
    };
  }

  async componentDidMount() {
    const slideId = this.props.match.params.slideId;
    const [slide, resp] = await Promise.all([
      fetchSlide(slideId),
      fetchAnnotationByStage(slideId, 'figure'),
    ]);
    if (resp.stage === 'figure' && resp.annotations.length > 0) {
      this.setState({slide, figures: resp.annotations});
    } else {
      this.goToNextPage();
    }
  }

  goToNextPage = (): void => {
    const nextURL = getNextURL(this.props.match.url);
    this.props.history.push(nextURL);
  }

  handleSelect = (e): void => {
    this.setState({
      selected: e.target.value,
    });
  }

  handleSubmit = (e): void => {
    e.preventDefault();

    const slideId = this.props.match.params.slideId;
    const {figures, figureIndex, selected } = this.state;
    const isLastFigure = figureIndex === figures.length - 1;
    if (!selected) {
      alert('If you find trouble choosing correct type, please select etc');
      return;
    }
    this.annotations[figureIndex] = selected;
    if (isLastFigure) {
        postAnnotation(slideId, 'figuretype', this.annotations, () => {
          this.goToNextPage();
        });
    } else {
      this.setState({
        figureIndex: figureIndex + 1,
        selected: undefined,
      });
    }
  }

  render() {
    const { slide, figures, figureIndex } = this.state;
    const progress = `${figureIndex + 1} / ${figures.length}`;
    let figureTypes: string[] = utils.getEnumStrings(FigureType);

    return (
      <div className="container">
        <Breadcrumb currentStage="4.1 Tell us the type of figure" />
        <h1>{`Select the type of element below (${progress})`}</h1>
        <Container id="plotter-wrapper">
          { slide &&
            <ImagePlotter
              slide={slide}
            />
          }
          {figures.length > 0 && <Box box={figures[figureIndex].box} group={0} />}
        </Container>
        <form onSubmit={this.handleSubmit}>
          <FormGroup>
            {figureTypes.map(v => (
              <Radio
                name="radioGroup"
                key={v}
                value={v}
                onClick={this.handleSelect}
              >
                {v}
              </Radio>
            ))}
          </FormGroup>
          <Button
            bsStyle="success"
            className="btn-lg"
            type="submit"
          >
            Submit
          </Button>
        </form>
      </div>
    );
  }
}
