import * as React from 'react';
import { Box } from './box';
import { BBox } from '../../models';
import { BoxStatus } from '../baseAnnotator';

interface Props {
  boxes: BBox[];
  boxStatus?: BoxStatus[];
  boxGroups?: number[];
  showBorder: Boolean;
}

export class BoxPlotter extends React.Component<Props, {}> {
  render() {
    let { boxes, boxStatus, boxGroups } = this.props;
    if (!boxGroups) {
      boxGroups = new Array(boxes.length).fill(-1);
    }
    return (
      boxes.map((box, i) => (
        <Box
          key={box.text + i}
          box={box}
          intersected={boxStatus ? boxStatus[i] === BoxStatus.Intersected : false}
          selected={boxStatus ? boxStatus[i] === BoxStatus.Selected : false}
          group={boxGroups ? boxGroups[i] : -1}
        />
      ))
    );
  }
}
