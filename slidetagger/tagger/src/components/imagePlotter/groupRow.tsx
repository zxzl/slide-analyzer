import * as React from 'react';
import * as Color from 'color';
import styled, { StyledFunction } from 'styled-components';
import { getColorFromId } from './box';

interface StyleProps {
  groupId: number;
}

const row: StyledFunction<StyleProps & React.HTMLProps<HTMLDivElement>> = styled.div;

const Row = row`
  width: 200px;
  height: 60px;
  padding: 5px 10px;
  border: 1px solid rgba(0,0,0, 0.08);
  margin-bottom: 5px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  background: ${props => Color(getColorFromId(props.groupId)).alpha(0.4).string()};
`;

const Button = styled.button`
  float: right;
`;

interface Props {
  groupId: number;
  removeGroup: Function;
  text: string;
}

export const GroupRow: React.StatelessComponent<Props> = ({groupId, removeGroup, text}) => (
  <Row groupId={groupId}>
    {text}
    <Button
      className="btn btn-danger btn-sm"
      onClick={() => removeGroup(groupId)}
    >
      Delete
    </Button>
  </Row>
);
