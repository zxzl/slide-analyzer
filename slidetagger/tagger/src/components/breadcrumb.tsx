import * as React from 'react';
import { Link } from 'react-router-dom';

interface Props {
  currentStage: String;
}

export const Breadcrumb: React.StatelessComponent<Props> = ({currentStage}) => {
  return (
    <ol className="breadcrumb">
      <li className="breadcrumb-item"><Link to={'/'}>Home</Link></li>
      <li className="breadcrumb-item active">{currentStage}</li>
    </ol>
  );
};
