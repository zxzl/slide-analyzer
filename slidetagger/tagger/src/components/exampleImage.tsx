import * as React from 'react';
import { Modal, Image } from 'react-bootstrap';
import * as urlJoin from 'url-join';
import styled from 'styled-components';

export const ThumbnailWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
  .thumbnail {
    width: 320px;
    margin: 10px;
    cursor: pointer;
    align-self: flex-start;
  }
  .thumbnail:hover {
    background: #337ab7;
  }
`;

interface Props {
  img_url: string;
  img_url_big: string;
  id: number;
  desc: string;
}

interface State {
  opened: boolean;
}

export class ExampleImage extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { opened: false};
  }

  toggleOpened = () => { this.setState({ opened: !(this.state.opened)}); };

  render() {
    const { opened } = this.state;
    const { img_url, img_url_big, desc } = this.props;
    const baseURL = process.env.PUBLIC_URL;
    return [
      (
        <div
          key="thumb"
          className="thumbnail"
          onClick={this.toggleOpened}
        >
          <img src={urlJoin(baseURL, img_url)} />
        </div>
      ),
      (
        <Modal
          key="modal"
          show={opened}
          onHide={this.toggleOpened}
        >
          <Modal.Header closeButton={true} />
          <Modal.Body>
            <Image src={urlJoin(baseURL, img_url_big)} responsive={true} />
            <p>{desc}</p>
          </Modal.Body>
        </Modal>
      )
    ];
  }

}
