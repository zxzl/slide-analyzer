import * as React from 'react';
import Button from 'react-bootstrap-button-loader';

import { OCRResult, RouteProps } from '../../models';
import { fetchSlide, postAnnotationNoCallback, fetchSlideAndCount } from '../../agent';

import { BoxStatus, Container, ControlWrapper, ButtonWrapper, ExampleImageWrapper } from '../baseAnnotator';
import { Breadcrumb } from '../breadcrumb';
import { ImagePlotter } from '../imagePlotter';
import { BoxPlotter } from '../imagePlotter/boxPlotter';
import { FeedbackButton } from '../feedbackButton';
import { InstructionWrapper } from '../instructionWrapper';

interface State {
  slide?: OCRResult;
  boxStatus: BoxStatus[];
  loading: boolean;
}

export class TitleAnnotator extends React.Component<RouteProps, State> {
  constructor(props: RouteProps) {
    super(props);
    this.saveSelect = this.saveSelect.bind(this);
    this.state = {
      slide: undefined,
      boxStatus: [],
      loading: false,
    };
  }

  async componentDidMount() {
    const slideId = this.props.match.params.slideId || 11;
    this.loadSlide(slideId);
  }

  loadSlide = (slideId: number) => {
    fetchSlide(slideId).then(result => {
      const numBox = result.boxes.length;
      this.setState({
        slide: result,
        boxStatus: new Array(numBox).fill(BoxStatus.Normal),
        loading: false,
      });
    });
  }

  updateIntersection = (ids: number[]): void => {
    const newStatus = this.state.boxStatus.map((status, i) =>
      (ids.indexOf(i) === -1) ? BoxStatus.Normal : BoxStatus.Intersected);
    this.setState({ boxStatus: newStatus });
  }

  updateSelection = (ids: number[]): void => {
    const { boxStatus } = this.state;
    this.setState({
      boxStatus: boxStatus.map((status, i) => (
        (ids.indexOf(i) === -1) ? BoxStatus.Normal : BoxStatus.Selected
      ))
    });
  }

  showSelectedAsText = (): string => {
    const { slide, boxStatus } = this.state;
    if (!slide) {
      return '';
    }
    const text =  slide.boxes
      .filter((b, i) => boxStatus[i] === BoxStatus.Selected)
      .map(box => box.text).join(' ');
    return text;
  }

  resetSelect = (): void => this.setState({
    boxStatus: this.state.boxStatus.fill(BoxStatus.Normal)
  })

  confirmUserTitle = (): boolean => {
    const titleFromUser = this.showSelectedAsText();
    if (titleFromUser === '') {
      return confirm('Does this slide not contain title?');
    }
    return confirm(`Is "${titleFromUser}" the title of slide?`);
  }

  async saveSelect() {
    const slideId = this.props.match.params.slideId || 11;
    const { batch } = this.props.match.params;

    const boxGroups = this.state.boxStatus.map(s =>
      s === BoxStatus.Selected ? 1 : -1);

    if (this.confirmUserTitle()) {
      this.setState({loading: true});
      const success = await postAnnotationNoCallback(slideId, 'title', boxGroups, batch);
      if (success) {
        const { slide_id, finished, code } = await fetchSlideAndCount(batch, 'title');
        if (finished) {
          this.props.history.push(`/${batch}/bye/${code}`);
        } else {
          this.props.history.push(`/${batch}/title/annotate/${slide_id}`);
          this.loadSlide(slide_id);
        }
      }
    }
  }

  render() {
    const { slide, boxStatus,  } = this.state;
    const slideId = this.props.match.params.slideId || 11;
    if (!slide) {
      return(<h1>Loading</h1>);
    }
    return (
      <div className="container">
        <Breadcrumb currentStage={'1. Finding title'} />
        <h1>Drag title of the slide</h1>
        <InstructionWrapper id={'title'}>
          <div className="alert alert-success" role="alert">
            <p><strong>Instruction</strong></p>
            <p>Bounding boxes for words are presented with the green border.
              Please drag words that constitute a title of the slide, then press merge button at the right.</p>
            <p>If there is <strong>no title</strong> in the slide, please <strong>drag nothing</strong>.</p>
            <iframe
              src="https://giphy.com/embed/3oxHQeFjcVEkSz1rG0"
              width="480"
              height="248"
              frameBorder="0"
              className="giphy-embed"
              allowFullScreen={true}
            />
            <p><strong>Example</strong></p>
            <ExampleImageWrapper>
              <img src={process.env.PUBLIC_URL + '/img/title/1.png'} />
              <img src={process.env.PUBLIC_URL + '/img/title/2.png'} />
              <img src={process.env.PUBLIC_URL + '/img/title/3.png'} />
            </ExampleImageWrapper>
          </div>
        </InstructionWrapper>
        <hr/>
        <Container id="plotter-wrapper">
          <ImagePlotter
            slide={slide}
            updateIntersection={this.updateIntersection}
            updateSelection={this.updateSelection}
          />
          <BoxPlotter
            boxes={slide.boxes}
            boxStatus={boxStatus}
            boxGroups={slide.boxes.map(_ => -1)}
            showBorder={false}
          />
          <ControlWrapper>
            <ButtonWrapper>
              <h5>Selected Title</h5>
                <p>
                  <code>
                    {slide && slide.boxes.filter((b, i) => boxStatus[i] === BoxStatus.Selected)
                    .map(box => box.text).join(' ')}
                  </code>
                </p>
                <button type="button" className="btn btn-info" onClick={this.resetSelect}>Reset</button>
                <Button
                  loading={this.state.loading}
                  bsStyle="success"
                  onClick={this.saveSelect}
                >
                  Submit
                </Button>
                <FeedbackButton id={slideId} stage={'title'} />
            </ButtonWrapper>
          </ControlWrapper>
        </Container>
      </div>
    );
  }
}
