import * as React from 'react';
import Button from 'react-bootstrap-button-loader';

import { resetSession, fetchSlideAndCount } from '../../agent';
import { RouteProps } from '../../models';

interface State {
  loading: boolean;
}

export class TitleHome extends React.Component<RouteProps, State> {
  state = { loading: false };

  componentDidMount() {
    resetSession();
  }

  goToNewSlide = (): void => {
    this.setState({ loading: true });
    const { batch } = this.props.match.params;
    fetchSlideAndCount(batch, 'title').then(({slide_id, count}) => {
      this.props.history.push(`/${batch}/title/annotate/${slide_id}`);
    });
  }

  render() {
    return (
      <div className="container">
        <div className="jumbotron">
          <h1 className="display-3">Find the title of slide</h1>
          <p className="lead">
            In this task, you will find title in a slide</p>
          <hr className="my-4" />
          <p>We encourage you to do the task using
            <a href="https://www.google.com/chrome/browser/desktop/index.html"> Google Chrome</a>,
            with the screen resolution larger than 1024 * 700.
          </p>
          <p>Your task activity will be recorded by <a href="https://www.hotjar.com/">Hotjar </a>
          for analyzing and improving task efficiency</p>
          <p className="lead">
            <Button
              className="btn-lg"
              bsStyle="primary"
              loading={this.state.loading}
              onClick={this.goToNewSlide}
            >
              Start
            </Button>
          </p>
        </div>
      </div>
    );
  }
}
