import * as React from 'react';
import { BBox } from '../../models';

export const colors = [
  '#ff7f0e', '#2ca02c', '#ffbb78', '#98df8a', '#d62728', '#ff9896',
  '#9467bd', '#c49c94', '#e377c2', '#f7b6d2', '#17becf', '#9edae5',
  '#7f7f7f', '#c7c7c7',  '#c5b0d5', '#8c564b',  '#bcbd22', '#dbdb8d',
];

export const getColorFromId = (groupId: number) => (
  colors[groupId % colors.length]
);

interface BoxProps {
  box: BBox;
  intersected?: boolean;
  selected?: boolean;
  group: number;
}

export const Box: React.StatelessComponent<BoxProps> = ({box, intersected, selected, group}) => {
  let background = 'none';
  if (selected) {
    background = `rgb(0, 255, 255)`;
  } else if (intersected) {
    background = `rgb(0, 162, 255)`;
  } else if (group !== -1) {
    background = colors[group % colors.length];
  }
  const style = {
    position: 'absolute' as 'absolute',
    top: String(box.top) + 'px',
    left: String(box.left) + 'px',
    width: String(box.width) + 'px',
    height: String(box.height) + 'px',
    border: '1px solid green',
    background,
    opacity: 0.4,
  };

  return (
    <div style={style} />
  );
};
