import * as React from 'react';
import { OCRResult, BBox } from '../../models';
import * as urlJoin from 'url-join';

interface Props {
  slide: OCRResult;
  updateIntersection?: Function;
  updateSelection?: Function;
  handleSelect?: Function;
  boxes?: JSX.Element[];
  disableSelection?: boolean;
}

interface State {
  dragged?: {
    x1: number; // Image's left-top corner is (0, 0)
    y1: number;
    x2: number;
    y2: number;
  };
}

export class ImagePlotter extends React.Component<Props, State> {

  private offsetX: number;
  private offsetY: number;

  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  updateOffset = (): void => {
    const parentNode = document.getElementById('plotter-wrapper');
    if (!parentNode) {
      return;
    }
    const boundingRect = parentNode.getBoundingClientRect();
    this.offsetX = Number(boundingRect.left);
    this.offsetY = Number(boundingRect.top);
  }

  handleSelectStart = (e): void => {
    if (this.props.disableSelection) {
      return;
    }
    this.updateOffset();
    const { clientX, clientY } = e;
    const normalX = clientX - this.offsetX;
    const normalY = clientY - this.offsetY;
    this.setState({
      dragged: {
        x1: normalX,
        y1: normalY,
        x2: normalX,
        y2: normalY,
      }
    });
    window.document.addEventListener('mousemove', this.handleMouseMove);
    window.document.addEventListener('mouseup', this.handleMouseUp);
  }

  handleMouseMove = (e): void => {
    if (!(this.state.dragged)) {
      return;
    }
    const { clientX, clientY } = e;
    const { dragged } = this.state;

    const intersectedIndexes =  this.calculateIntersection(
      dragged.x1, dragged.x2, dragged.y1, dragged.y2);
    if (this.props.updateIntersection) {
      this.props.updateIntersection(intersectedIndexes);
    }
    this.setState({
      dragged: Object.assign({}, this.state.dragged, {
        x2: clientX - this.offsetX,
        y2: clientY - this.offsetY,
      })
    });
  }

  handleMouseUp = (e): void => {
    if (!(this.state.dragged)) { return; }
    window.document.removeEventListener('mousemove', this.handleMouseMove);
    window.document.removeEventListener('mouseup', this.handleMouseUp);

    const { dragged: { x1, x2, y1, y2 } } = this.state;
    const draggedArea = Math.abs(x1 - x2) * Math.abs(y1 - y2);
    if (draggedArea > 200) {
      this.handleSelection(x1, x2, y1, y2);
    }
    this.setState({ dragged: undefined });
  }

  handleSelection = (x1, x2, y1, y2): void => {
    if (this.props.updateIntersection) {
      this.props.updateIntersection([]);
    }
    const intersectedIndexes =  this.calculateIntersection(
      x1, x2, y1, y2);
    if (this.props.updateSelection) {
      this.props.updateSelection(intersectedIndexes);
    }
    if (this.props.handleSelect) {
      const newBox: BBox = {
        'text': 'Box for selected region',
        'id': -1,
        'top': Math.min(y1, y2),
        'left': Math.min(x1, x2),
        'width': Math.abs(x1 - x2),
        'height': Math.abs(y1 - y2)
      };
      this.props.handleSelect(newBox);
    }
  }

  calculateIntersection = (x1, x2, y1, y2): number[] => {
    const { slide: { boxes }} = this.props;
    const DragBox: BBox = {
      'id': -1,
      'text': 'Box for dragged region',
      'top': Math.min(y1, y2),
      'left': Math.min(x1, x2),
      'width': Math.abs(x1 - x2),
      'height': Math.abs(y1 - y2)
    };
    const selectedIndex: number[] = [];
    for (let i = 0; i < boxes.length; i++) {
      if (this.checkIntersect(DragBox, boxes[i])) {
        selectedIndex.push(i);
      }
    }
    return selectedIndex;
  }

  checkIntersect = (a: BBox, b: BBox) => {
    if (a.left + a.width < b.left)  { return false; }
    if (b.left + b.width < a.left)  { return false; }
    if (a.top + a.height < b.top) { return false; }
    if (b.top + b.height < a.top) { return false; }
    return true;
  }

  render() {
    const { img_url } = this.props.slide;
    let style;
    if (this.state.dragged) {
      var { x1, y1, x2, y2 } = this.state.dragged;
      style = {
        width: `${Math.abs(x1 - x2)}px`,
        height: `${Math.abs(y1 - y2)}px`,
        top: `${Math.min(y1, y2)}px`,
        left: `${Math.min(x1, x2)}px`,
        position: 'absolute' as 'absolute',
        background: `rgba(0, 162, 255, 0.4)`,
      };
    }
    return (
      <div
        className="image-wrapper"
        id="wrapper"
        onMouseDown={this.handleSelectStart}
      >
        <img src={urlJoin(process.env.REACT_APP_IMAGE_BUCKET, img_url)} draggable={false} />
        {this.state.dragged && <div style={style} />}
        {this.props.boxes}
      </div>
    );
  }
}
