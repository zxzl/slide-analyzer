import * as React from 'react';
import { ProgressBar } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import { checkSession } from '../agent';
import {} from '../';

interface Props {
  history: {
    push: Function;
  };
  match: {
    params: {
      slideId?: number;
    };
  };
}

interface State {
  fetching: boolean;
  resp?: Result;
}

interface Result {
  result: boolean;
  job_id?: string;
  missing?: string[];
}

export class Outro extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      fetching: true,
    };
  }

  componentDidMount() {
    const slideId = this.props.match.params.slideId;
    if (slideId) {
      checkSession(slideId).then((resp: Result) => {
        this.setState({
          fetching: false,
          resp
        });
      });
    }
  }

  renderResult = (resp) => {
    const slideId = this.props.match.params.slideId;
    if (resp.result) {
      return (
        <div>
          <h1 className="display-3">Thanks 🙏</h1>
          <p className="lead">Your token for this task is</p>
          <h3> {resp.job_id}</h3>
          <hr />
        </div>
      );
    }
    return (
      <div>
        <h1 className="display-3">Error</h1>
        <hr className="lead"/>
        <p className="lead">Some tasks were not finished. Please visit links below to finish tasks.</p>
        {resp.missing.map(s => (
          <Link to={`/annotate/${slideId}/${s}`}>
            Go to finding {s} page <br />
          </Link>
        ))}
      </div>
    );
  }

  render() {
    const {fetching} = this.state;
    return (
      <div className="container">
        <div className="jumbotron">
          {fetching && <ProgressBar active={true} now={45} />}
          {!fetching && this.state.resp &&
            this.renderResult(this.state.resp)}
        </div>
      </div>
    );
  }
}
