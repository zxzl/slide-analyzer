import * as React from 'react';
import Button from 'react-bootstrap-button-loader';
import styled from 'styled-components';
import { OCRResult } from '../models';
import { ImagePlotter } from './imagePlotter';
import { BoxPlotter } from './imagePlotter/boxPlotter';
import { GroupRow } from './imagePlotter/groupRow';
import { FeedbackButton } from './feedbackButton';

export enum BoxStatus {
  Normal = 1,
  Intersected,
  Selected,
}

interface Props {
  slide: OCRResult;
  boxStatus: BoxStatus[];
  groups: number[];
  updateStatus: Function;
  updateGroups: Function;
  saveGroups: Function;
  stage?: string;
  loading?: boolean;
  buttonMessage?: string;
  buttonHidden?: boolean;
}

export const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
`;

export const ControlWrapper = styled.div`
  margin-left: 20px;
  width: 200px;
`;

export const ButtonWrapper =  styled.div`
  button {
    margin-right: 10px;
    margin-top: 10px;
  }
  min-height: 100px;
`;

export const GroupWrapper = styled.div`
  min-height: 300px;
  width: 220px;
  border: 1px solid #eceeef;
  border-radius: 10px;
  margin-bottom: 10px;
  padding: 10px;
`;

export const ExampleImageWrapper = styled.div`
  img {
    margin: 0 10px 10px 0;
  }
  margin: 20px 0 0 10px;
`;

export class BaseAnnotator extends React.Component<Props, {}> {

  private nextGroupId: number;

  constructor(props: Props) {
    super(props);
    this.updateIntersection = this.updateIntersection.bind(this);
    this.updateSelection = this.updateSelection.bind(this);
    this.nextGroupId = Math.max(...props.groups) + 1;
  }

  public updateIntersection(ids: number[]): void {
    const newStatus = this.props.boxStatus.map((status, i) =>
      (ids.indexOf(i) === -1) ? BoxStatus.Normal : BoxStatus.Intersected);
    this.props.updateStatus(newStatus);
  }

  public updateSelection(ids: number[]): void {
    const { boxStatus } = this.props;
    this.props.updateStatus(
      boxStatus.map((status, i) => (
        (ids.indexOf(i) === -1) ? BoxStatus.Normal : BoxStatus.Selected
      ))
    );
  }

  resetSelect = (): void => {
    this.props.updateStatus(this.props.boxStatus.fill(BoxStatus.Normal));
  }

  saveSelect = (e): void => {
    const { groups, boxStatus } = this.props;
    const selectedIndexes: number[] = [];
    boxStatus.forEach((status, i) => {
      if (status === BoxStatus.Selected) {
        selectedIndexes.push(i);
      }
    });
    if (selectedIndexes.length > 0) {
      const newGroups = groups.slice();

      selectedIndexes.forEach(i => {
        newGroups[i] = this.nextGroupId;
      });
      this.nextGroupId += 1;
      this.props.updateGroups(newGroups);
      this.props.updateStatus(boxStatus.slice().fill(BoxStatus.Normal));
    }
  }

  getGroupIds = () => {
    let group: number[] = [];
    for (let i of this.props.groups) {
      if (i !== -1 && group.indexOf(i) === -1) {
        group.push(i);
      }
    }
    return group;
  }

  deleteGroup = (groupId: number) => {
    const newGroup = this.props.groups.map(i => i === groupId ? -1 : i);
    this.props.updateGroups(newGroup);
  }

  saveGroups = () => {
    this.props.saveGroups(this.props.groups);
  }

  public render() {
    const { slide, groups, boxStatus, loading, buttonMessage, buttonHidden } = this.props;

    const groupIds = this.getGroupIds();
    return(
      <div>
        <Container id="plotter-wrapper">
          <ImagePlotter
            slide={slide}
            updateIntersection={this.updateIntersection}
            updateSelection={this.updateSelection}
          />
          <BoxPlotter
            boxes={slide.boxes}
            boxStatus={boxStatus}
            boxGroups={groups}
            showBorder={true}
          />
          <ControlWrapper>
            <ButtonWrapper>
              <h5>Selected Texts</h5>
              <p>
                <code>
                {slide && slide.boxes.filter((b, i) => boxStatus[i] === BoxStatus.Selected)
                      .map(box => box.text).join(' ')}
                </code>
              </p>
              <button
                type="button"
                className="btn btn-info"
                onClick={this.resetSelect}
              >
                Reset
              </button>
              <button
                type="button"
                className="btn btn-primary"
                onClick={this.saveSelect}
                disabled={
                  boxStatus.filter(s => s === BoxStatus.Selected).length === 0}
              >
                Add
              </button>
            </ButtonWrapper>
            <h5>Found Groups</h5>
            <GroupWrapper>
              {slide && groupIds.map(id => (
                <GroupRow
                  key={id}
                  groupId={id}
                  removeGroup={this.deleteGroup}
                  text={slide.boxes
                          .filter((b, i) => groups[i] === id)
                          .map(b => b.text).join(' ')}
                />
              ))}
            </GroupWrapper>
            { !buttonHidden &&
              <Button
                onClick={this.saveGroups}
                bsStyle="success"
                className="btn-lg"
                loading={loading}
              >
                {buttonMessage ? buttonMessage : 'Submit'}
              </Button>
            }
            { !buttonHidden &&
              <FeedbackButton id={slide.id} stage={this.props.stage || ''} />
            }
          </ControlWrapper>
        </Container>;
      </div >
    );
  }
}
