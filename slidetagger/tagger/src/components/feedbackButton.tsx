import * as React from 'react';
import { Modal, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap';
import { postFeedback } from '../agent';

interface Props {
  id: number;
  stage: string;
}

interface State {
  message: string;
  modalOpened: boolean;
}

export class FeedbackButton extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      message: '',
      modalOpened: false,
    };
  }

  handleOpen = () => { this.setState({modalOpened: true}); };
  handleClose = () => { this.setState({modalOpened: false}); };
  handleChange = e => { this.setState({message: e.target.value}); };
  handleSubmit = () => {
    const { id, stage } = this.props;
    const { message } = this.state;
    if (message.length < 5) {
      alert('Please write feedback longer than 5 characters');
      return;
    }
    postFeedback(id, stage, message, () => {
      alert('Thanks for comment');
      this.handleClose();
    });
  }

  render() {
    const { modalOpened } = this.state;
    return [
      (
      <Button key="button" bsStyle="info" onClick={this.handleOpen}>
        I'm not sure😅
      </Button>
      ),
      (
      <Modal
        key="modal"
        show={modalOpened}
        onHide={this.handleClose}
      >
        <Modal.Header closeButton={true}>
          <Modal.Title>Leave Additional Comment</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <FormGroup controlId="formControlsTextarea">
            <ControlLabel>Share your thought</ControlLabel>
            <FormControl componentClass="textarea" onChange={this.handleChange}/>
          </FormGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle="primary" onClick={this.handleSubmit}>Submit</Button>
          <Button onClick={this.handleClose}>Close</Button>
        </Modal.Footer>
      </Modal>
      )
    ];
  }
}
