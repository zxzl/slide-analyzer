import * as React from 'react';

export const NoMatch: React.StatelessComponent<{}> = () => {
  return (
    <h1>
      Invalid URL
    </h1>
  );
};
