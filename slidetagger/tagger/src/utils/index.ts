import { Sentence } from '../models';

// Hard to annotate enum type in argument
// https://stackoverflow.com/questions/30774874/enum-as-parameter-in-typescript
export const getEnumStrings = (e): string[] => {
  let values: string[] = [];
  for (let value in e) {
    if (typeof value === 'string') {
      values.push(value);
    }
  }
  return values;
};

export const groups2sentence = (groups: number[]): Sentence[] => {
  const sentenceMap = new Map();
  for (let i = 0; i < groups.length; i++) {
    const group = groups[i];
    if (sentenceMap.has(group)) {
      sentenceMap.get(group).push(i);
    } else {
      sentenceMap.set(group, [i]);
    }
  }
  const sentenceMapKVs: Array <[number, number[]]> = Array.from(sentenceMap);
  const sentences = sentenceMapKVs.map(([key, value]) => ({id: key, words: value}));
  return sentences;
};

export const sentence2groups = (sentences: Sentence[], numWords: number): number[] => {
  let groupIndex = 0;
  const groups = new Array(numWords).fill(-1);
  for (let sentence of sentences) {
    for (let wordIndex of sentence.words) {
      groups[wordIndex] = groupIndex;
    }
    groupIndex += 1;
  }
  return groups;
};

export const mergeSentences = (sentences2d: Sentence[][]): Sentence[] => {
  let merged: Sentence[] = [];
  let id = 0;
  for (let sentences of sentences2d) {
    let words: number[] = [];
    for (let sentence of sentences) {
      words.push(...sentence.words);
    }
    merged.push({ id: id++, words});
  }
  return merged;
};
