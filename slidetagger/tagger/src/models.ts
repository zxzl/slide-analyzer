export interface OCRResult {
  id: number;
  width: number;
  img_url: string;
  height: number;
  boxes: Array<BBox>;
}

export interface BBox {
  id: number;
  text: string;
  top: number;
  left: number;
  width: number;
  height: number;
}

export interface Sentence {
  id: number;
  words: number[];
}

export interface Feedback {
  id: number;
  slide_id: number;
  created_at: string;
  created_by: number;
  stage: string;
  body: string;
}

export interface RouteProps {
  history: {
    push: Function;
  };
  match: {
    params: {
      slideId: number;
      batch: number;
      code?: string;
    };
    url: string;
  };
  location: {
    search: string;
  };
  children: React.ReactNode;
}

export interface SentencePayload {
  stage: 'sentence';
  payload: Sentence[];
}

export interface TitlePayload {
  stage: 'title';
  payload: number[];
}

export interface ListPayload {
  stage: 'list';
  payload: {
    id: number;
    box: BBox;
  }[];
}

export interface FigurePayload {
  stage: 'figure';
  payload: {
    id: number;
    box: BBox;
  }[];
}

export type AnnotationPayload = SentencePayload | TitlePayload | ListPayload | FigurePayload;
