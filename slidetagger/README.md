Slide Tagger

# Commands Cheat Sheet
For debug..
```commandline
cd (project root folder)
PYTHONPATH=. FLASK_DEBUG=1 flask run
```
For deploy
```commandline
cd (project root folder)
PYTHONPATH=. gunicorn --reload slidetagger.api.wsgi:app
```

For migration
```commandline
cd (migration folder)
PYTHONPATH=../ alembic revision --autogenerate -m (YOUR MESSAGE)
(check migration file)
PYTHONPATH=../ alembic upgrade head
```
