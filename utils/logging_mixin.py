import sys
import logging

IN_DEBUG_MODE = True
gettrace = getattr(sys, 'gettrace', None)
if gettrace is None:
    IN_DEBUG_MODE = False

handler = logging.StreamHandler()
formatter = logging.Formatter("[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s")
handler.setFormatter(formatter)

logger = logging.getLogger()
logger.addHandler(handler)
if IN_DEBUG_MODE:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)


class LoggingMixin(object):
    """
    For self.log method
    """
    @property
    def log(self):
        return logger
