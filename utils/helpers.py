import os
import glob
import requests
import shutil
from PIL import Image


def load_images_from_folder(input_path):
    """
    :param input_path: string
    :return: string
    """
    abs_path = os.path.abspath(input_path)
    img_types = ('*.jpg', '*.png', '*.jpeg', '*.gif')
    results = [glob.glob(os.path.join(abs_path, t)) for t in img_types]
    files = sum(results, [])
    files.sort()
    return files


def measure_image_width_height(img_path):
    image = Image.open(img_path)
    size = image.size
    return size


IMAGE_TEMP_DIR = '/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/temp_images'
S3_BUCKET_URL = 'http://slidetagger-images.s3-website.ap-northeast-2.amazonaws.com'


def get_slide_image_from_s3(filename):
    file_path = os.path.join(IMAGE_TEMP_DIR, filename)
    if os.path.isfile(file_path):
        return file_path

    s3_object_url = os.path.join(S3_BUCKET_URL, filename)
    response = requests.get(s3_object_url, stream=True)
    with open(file_path, 'wb') as f:
        shutil.copyfileobj(response.raw, f)
    return file_path


