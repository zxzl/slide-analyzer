"""Base revision

Revision ID: 7b1fbf01f2f7
Revises:
Create Date: 2017-12-04 15:32:38.714111

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7b1fbf01f2f7'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('ocr_results',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('job_name', sa.String(), nullable=True),
    sa.Column('file_name', sa.String(), nullable=True),
    sa.Column('width', sa.Integer(), nullable=True),
    sa.Column('height', sa.Integer(), nullable=True),
    sa.Column('results', sa.JSON(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('annotation',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('body', sa.JSON(), nullable=True),
    sa.Column('worker_id', sa.Integer(), nullable=True),
    sa.Column('ocr_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['ocr_id'], ['ocr_results.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('annotation')
    op.drop_table('ocr_results')
    # ### end Alembic commands ###
