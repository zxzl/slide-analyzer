"""Add last_assigned

Revision ID: cf57fea5a4e8
Revises: 7b1fbf01f2f7
Create Date: 2017-12-04 15:39:19.946264

"""
from alembic import op
import sqlalchemy as sa
import datetime


# revision identifiers, used by Alembic.
revision = 'cf57fea5a4e8'
down_revision = '7b1fbf01f2f7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('ocr_results',
                  sa.Column('last_assigned', sa.DateTime(timezone=True), nullable=False, server_default=sa.func.current_timestamp()))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('ocr_results', 'last_assigned')
    # ### end Alembic commands ###
