import argparse
import json
import os

from parser.item import OCRItem
from parser.position import Position


class WordMerger:
    def __init__(self, input_path, output_path, indices):
        self.target_frames = self.load_ocr(input_path, indices)
        self.output_path = output_path
        self.id_counter = 0

    def merge_frames(self):
        for f in self.target_frames:
            f['words'] = self.merge(f['words'])
        if bool(self.output_path):
            for f in self.target_frames:
                f['words'] = [f.make_dict() for f in f['words']]
            with open(self.output_path, 'w') as f:
                json.dump(self.target_frames, f, indent=4)
            return f"Saved to {self.output_path}"
        return self.target_frames

    def merge(self, frame):
        """
        Regard two words who share horizontal edges, and close (10px) to be one word
        """
        # 0-th item is concatenation of every word
        # Filter words with only one character
        ocr_items = [self.make_object(item) for item in frame[1:] if len(item['description']) > 1]
        merged = []
        prev_item = ocr_items[0]
        index = 1
        while index < len(ocr_items):
            curr_item = ocr_items[index]
            if self.is_near_position(prev_item.position, curr_item.position):   # merge
                prev_item = prev_item.merge(curr_item)
            else:
                merged.append(prev_item)
                prev_item = curr_item
            index = index + 1
        merged.append(prev_item)
        return merged


    @staticmethod
    def is_near_position(pos_a, pos_b, max_distance = 20):
        max_distance = pos_a.height
        aligned = pos_a.is_horizontally_aligned(pos_b, pos_a.height)
        near = pos_a.get_distance(pos_b) < max_distance
        # if pos_b.top == 60 and pos_b.left == 700:
        #    print(near)
        return aligned and near

    @staticmethod
    def load_ocr(input_path, slide_indices):
        ocr_results = []
        input_path = os.path.abspath(input_path)
        files = [f for f in os.listdir(input_path) if os.path.splitext(f)[1] == '.json']
        for i, start_end in enumerate(slide_indices):
            file = [f for f in files if ("%04d" % start_end[0]) in f]
            if len(file) is not 1:
                raise Exception("Frame not found")
            file = file[0]
            with open(os.path.join(input_path, file), 'r') as f:
                print(file)
                words = json.load(f, encoding='UTF-8', strict=False)
                ocr_results.append({
                    'index': i,
                    'start': start_end[0],
                    'end': start_end[1],
                    'words': words,
                    'pairs': []
                })
        return ocr_results


def input_reader(input_file):
    with open(input_file, 'r') as f:
        ocr_path = f.readline().strip()
        output_path = f.readline().strip()
        slides = []
        while True:
            line = f.readline()
            if not line:
                break
            start, end = list(map(int, line.split()))
            slides.append((start, end))
        return ocr_path, output_path, slides


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Give input file')
    parser.add_argument('input_file', help='the text file including required information')
    args = parser.parse_args()

    ocr_path, output_path, slides = input_reader(args.input_file)

    merger = WordMerger(ocr_path, output_path, slides)
    merger.merge_frames()
