import math
from unittest import TestCase

from parser.position import Position


class TestPosition(TestCase):
    def test_get_area(self):
        p1 = Position(10, 10, 10, 10)
        self.assertAlmostEqual(p1.get_area(), 100)

        p2 = Position(10, 10, 20, 15)
        self.assertAlmostEqual(p2.get_area(), 300)

    def test_get_distance(self):
        p1 = Position(10, 10, 10, 10)
        p2 = Position(30, 20, 10, 10)
        self.assertAlmostEqual(p1.get_distance(p2), 10)
        self.assertAlmostEqual(p2.get_distance(p1), 10)

        p3 = Position(10, 10, 10, 10)
        p4 = Position(20, 30, 10, 10)
        self.assertAlmostEqual(p3.get_distance(p4), 10)
        self.assertAlmostEqual(p4.get_distance(p3), 10)

        p5 = Position(10, 10, 10, 10)
        p6 = Position(30, 30, 10, 10)
        self.assertAlmostEqual(p5.get_distance(p6), math.sqrt(200))
        self.assertAlmostEqual(p6.get_distance(p5), math.sqrt(200))

        p7 = Position(10, 10, 10, 10)
        p8 = Position(15, 15, 10, 10)
        self.assertAlmostEqual(p7.get_distance(p8), 0)

    def test_merge(self):
        p1 = Position(10, 10, 10, 10)
        p2 = Position(10, 20, 10, 10)
        p3 = p1.merge(p2)
        self.assertEqual([p3.top, p3.left, p3.width, p3.left], [10, 10, 20, 10])

    def test_horizontal_align(self):
        p1 = Position(10, 10, 10, 10)
        p2 = Position(9, 10, 10, 12)
        self.assertEqual(p1.is_horizontally_aligned(p2), True)

    def test_vertical_align(self):
        p1 = Position(10, 10, 10, 10)
        p2 = Position(10, 9, 12, 10)
        self.assertEqual(p1.is_aligned_with_position_below(p2), True)
