from unittest import TestCase
from .subtitle import Subtitle
import os

TEST_SUBTITLE_PATH = '/Users/shik/Dropbox/classes/17.s/slide-analyzer/sample_video/video2.srt'
TEST_ALIGNMENT_PATH = '/Users/shik/Dropbox/classes/17.s/slide-analyzer/sample_video/video2_aligned.json'


class TestSubtitle(TestCase):
    @classmethod
    def setUp(cls):
        cls.subtitle = Subtitle('test')

    def test_load(self):
        pass

    def test_load_lines_srt(self):
        self.subtitle.load_lines_srt(TEST_SUBTITLE_PATH)
        self.assertEqual(len(self.subtitle.words), 0)
        self.assertEqual(len(self.subtitle.sentences), 0)
        self.assertEqual(self.subtitle.lines[0].text,
                         'DANIEL HEIMPEL: Hello, and welcome to Journalism')
        print(self.subtitle.lines[0])

    def test_load_words(self):
        self.subtitle.load_words(TEST_ALIGNMENT_PATH)
        print(self.subtitle.words)

    def test_load_sentences(self):
        self.subtitle.load_lines_srt(TEST_SUBTITLE_PATH)
        self.subtitle.load_words(TEST_ALIGNMENT_PATH)
        self.subtitle.load_sentences()
        pass

    def test_save_and_load(self):
        self.subtitle.load_lines_srt(TEST_SUBTITLE_PATH)
        self.subtitle.load_words(TEST_ALIGNMENT_PATH)
        self.subtitle.load_sentences()
        self.subtitle.save_as_json("./test_video2.json")
        new_sub = Subtitle('test')
        new_sub.load_from_json("./test_video2.json")
        self.assertSequenceEqual(self.subtitle.words, new_sub.words)
        self.assertSequenceEqual(self.subtitle.lines, new_sub.lines)
        self.assertSequenceEqual(self.subtitle.sentences, new_sub.sentences)
        os.remove('./test_video2.json')
