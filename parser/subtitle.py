import json
import os
from typing import NamedTuple, List
from webvtt import WebVTT
from nltk.tokenize import sent_tokenize
import argparse
from tempfile import TemporaryFile
import srt
import requests

GENTLE_ENDPOINT = "http://localhost:8765/transcriptions?async=false"


class SubtitleUnit(NamedTuple):
    id: int
    start: float
    end: float
    text: str


class Subtitle:
    def __init__(self, name):
        self.name: str = name
        self.words: List[SubtitleUnit] = []
        self.lines: List[SubtitleUnit] = []
        self.sentences: List[SubtitleUnit] = []
        self.transcript: str = ""
        self.alignments = []

    def save_as_json(self, output_path):
        output_dict = {
            "name": self.name,
            "words": [w._asdict() for w in self.words],
            "lines": [l._asdict() for l in self.lines],
            "sentences": [s._asdict() for s in self.sentences],
        }
        with open(output_path, 'w') as f:
            json.dump(output_dict, f, indent=2)

    def load_from_json(self, input_path):
        with open(input_path) as f:
            d = json.load(f)
            self.name = d['name']
            self.words = [SubtitleUnit(**w) for w in d['words']]
            self.lines = [SubtitleUnit(**l) for l in d['lines']]
            self.sentences = [SubtitleUnit(**s) for s in d['sentences']]
        return self

    def load(self, subtitle_path, gentle_json_path):
        self.load_lines(subtitle_path)
        self.load_words(gentle_json_path)
        self.load_sentences_from_gentle()

    def load_lines(self, subtitle_path):
        subtitle_path = os.path.abspath(subtitle_path)
        ext = os.path.splitext(subtitle_path)[1]
        if ext not in ['.srt', '.vtt']:
            raise Exception("Wrong subtitle format")
        if ext == '.srt':
            self.load_lines_srt(subtitle_path)
        else:
            self.load_lines_vtt(subtitle_path)

    def load_lines_srt(self, subtitle_path):
        with open(subtitle_path, 'r', encoding='utf-8') as f:
            txt = f.read()
        lines = srt.parse(txt)
        for i, l in enumerate(lines):
            if bool(l.content):
                self.lines.append(SubtitleUnit(
                    i,
                    l.start.total_seconds(),
                    l.end.total_seconds(),
                    l.content.replace("\n", " ")
                ))
        return self.lines

    def load_lines_vtt(self, subtitle_path):
        captions = WebVTT().read(self.input_path)
        for i, caption in enumerate(captions):
            self.words.append(SubtitleUnit(
                i,
                caption.start_in_seconds,
                caption.end_in_secons,
                " ".join(caption.lines)
            ))

    def load_words(self, gentle_json_path):
        with open(gentle_json_path, 'r', encoding='utf-8') as f:
            rj = json.load(f)

        self.transcript = rj['transcript']
        self.alignments = rj['words']

        for i, w in enumerate(rj['words']):
            if ('start' not in w) or ('end' not in w):
                continue
            self.words.append(SubtitleUnit(
                i,
                w['start'],
                w['end'],
                w['word']
            ))

    def load_sentences(self, text_file=''):
        if text_file:
            with open(text_file, 'r') as f:
                lines = f.readlines()
            txt = " ".join([line.strip() for line in lines])
        else:
            lines_text_only = [l.text for l in self.lines]
            txt = " ".join(lines_text_only)
        sentences = sent_tokenize(txt)
        sentences_stripped = [s.replace("\\", "") for s in sentences]

        starting_line = 0

    def print_lines(self):
        concatentated = " ".join([s.text for s in self.lines])
        print(concatentated)

    def load_sentences_from_gentle(self):
        """
        Given transcript and words from Gentle, generate sentence level subtitle
        """
        sentences = sent_tokenize(self.transcript)
        # self.sentences = [SubtitleUnit(i, -1, -1, sentence)
        #                   for i, sentence in enumerate(self.transcript)]
        # Offset ranges of each sentences
        last_end_offset = -1
        offset_ranges = []
        for s in sentences:
            start_offset = last_end_offset + 1
            last_end_offset = start_offset + len(s)
            offset_ranges.append((start_offset, last_end_offset))

        # Get start, end timing from aligned words using char offset
        align_index = 0
        for i, s in enumerate(sentences):
            offset_range = offset_ranges[i]
            start_sec = 99999999
            end_sec = -1
            while align_index < len(self.alignments) and \
                self.alignments[align_index]['startOffset'] >= offset_range[0] and \
                    self.alignments[align_index]['endOffset'] <= offset_range[1]:
                alignment = self.alignments[align_index]
                align_index = align_index + 1
                if 'start' in alignment and 'end' in alignment:
                    start_sec = min(alignment['start'], start_sec)
                    end_sec = max(alignment['end'], end_sec)
            self.sentences.append(SubtitleUnit(i, start_sec, end_sec, s))


if __name__ == '__main__':
    # parser = argparse.ArgumentParser(description='Give input file')
    # parser.add_argument('input_file', help='subtitle_path')
    # parser.add_argument('alignment_result', help='subtitle_path')
    # args = parser.parse_args()

    # name = "video16"
    name = "video20"
    video_dir = f"/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/{name}"
    input_file = os.path.join(video_dir, f"{name}.srt")
    gentle_file = os.path.join(video_dir, f"{name}_gentle.json")
    output_file = os.path.join(video_dir, f"{name}_sub.json")

    ##
    # When we don't have Gentle file
    #
    # subtitle = Subtitle("commandline")
    # subtitle.load_lines(input_file)
    # subtitle.print_lines()


    ##
    # When we have Gentle file
    #
    subtitle = Subtitle(name)
    subtitle.load(input_file, gentle_file)
    subtitle.save_as_json(output_file)
