import cv2
import numpy as np

template_path = '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/bullet/circle.jpg'


def locate_circle(img_src):
    img = cv2.imread(img_src, 0)
    template = cv2.imread(template_path, 0)
    w, h = template.shape[::-1]

    img2 = cv2.imread(img_src)
    method = cv2.TM_CCOEFF_NORMED

    # Apply template Matching
    res = cv2.matchTemplate(img, template, method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    threshold = 0.9
    loc = np.where(res >= threshold)

    locs = []
    for pt in zip(*loc[::-1]):
        template_center = (pt[0] + w/2, pt[1] + h/2)
        locs.append(template_center)

    for pt in zip(*loc[::-1]):
        cv2.rectangle(img2, pt, (pt[0] + w, pt[1] + h), (0, 0, 255), 2)
    cv2.imwrite('res.png', img2)
    return locs
