from sqlalchemy import asc

from schemas.models import Slide, OCRResult
from schemas.database import init_db, db_session

def load_title(ocr_id):
    pass


def load_group(ocr_id):
    pass


def load_ocr_by_jobname(job_query):
    ocr_results = db_session.query(OCRResult).\
        filter(OCRResult.job_name == job_query).\
        order_by(asc(OCRResult.id)).\
        all()
    return ocr_results
