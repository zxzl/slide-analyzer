import json
from typing import List

from schemas.database import db_session
from schemas.models import OCRResult
from parser.layout.loaders import load_ocr_by_jobname
from parser.item import OCRItem
from parser.position import Position
from parser.layout.group_words import GroupWords
from parser.layout.plotter import Plotter

case_json_path = "/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/cases.json"
case_root = "/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2"


def eval_group(guessed_groups: List[List[int]], ocr: OCRItem, verbose=False):
    answer = ocr.sentences
    answer_groups = [w['words'] for w in answer.body['payload']
                     if w['id'] != -1]

    if verbose:
        words = [w['text'] for w in ocr.results]
        guessed_words = []
        correct_words = []
        for group in guessed_groups:
            word = " ".join(words[i] for i in group)
            guessed_words.append(word)
        for group in answer_groups:
            word = " ".join(words[i] for i in group)
            correct_words.append(word)
        print("guess")
        print(guessed_words)
        print("answer")
        print(correct_words)

    # Finding dot-only words
    dot_ids = [w['id'] for w in ocr.results if w['text'] == '.']

    num_total = len(answer_groups)
    num_correct = 0
    guessed_groups_sets = [set([id for id in g if id in ocr.valid_word_ids])
                           for g in guessed_groups]
    # guessed_groups_sets_without_dots = [(g - set(dot_ids)) for g in
    #                                     guessed_groups_sets]
    for answer in answer_groups:
        answer_set = set(answer)
        if answer_set in guessed_groups_sets:
            num_correct += 1
    return num_correct, num_total


def run_by_id(id):
    ocr = db_session.query(OCRResult).get(id)
    words = ocr.results_flat
    word_objects = [OCRItem(word['id'],
                            word['text'],
                            Position(word['top'], word['left'],
                                     word['width'], word['height']))
                    for word in words]
    grouper = GroupWords(word_objects, ocr.width, ocr.height,
                         ocr.file_name, max_distance=30, debug=True)
    paragraphs = grouper.merge_both_direction(word_objects, verbose=True)
    plotter = Plotter('result', ocr.file_name)
    plotter.record_items(paragraphs)
    plotter.save()


if __name__ == '__main__':
    run_by_id(10399)

    # Load names
    with open(case_json_path, encoding='utf-8') as f:
        cases = json.load(f)
        names = [case['name'] for case in cases]

    # names_to_run = names[:1]
    # verbosity = True

    names_to_run = names
    verbosity = True

    max_distance = 30

    cases_correct, cases_total = 0, 0
    for name in names_to_run:
        job_name = 'tenvideo_' + name
        ocrs = load_ocr_by_jobname(job_name)
        ocr_correct, ocr_total = 0, 0
        for i, ocr in enumerate(ocrs):
            words = ocr.results_flat
            word_objects = [OCRItem(word['id'],
                                    word['text'],
                                    Position(word['top'], word['left'],
                                             word['width'], word['height']))
                            for word in words]

            grouper = GroupWords(word_objects, ocr.width, ocr.height,
                                 ocr.file_name, max_distance=max_distance,
                                 debug=True)
            paragraphs = grouper.merge_both_direction(word_objects)
            paragraphs_ids = [list(p.ids) for p in paragraphs]

            _correct, _total = eval_group(paragraphs_ids, ocr,
                                          verbose=verbosity)
            print(i, ocr.file_name, _correct, _total,
                  (_correct * 100 / max(_total, 1)))

            if _correct != _total:
                plotter = Plotter('result', ocr.file_name)
                plotter.record_items(paragraphs)
                plotter.save()

            ocr_correct += _correct
            ocr_total += _total
        print(job_name, ocr_correct, ocr_total, (ocr_correct * 100 / ocr_total))
        cases_correct += ocr_correct
        cases_total += ocr_total
    print(job_name, cases_correct, cases_total,
          (cases_correct * 100 / cases_total))
