import numpy as np
from typing import List, Tuple, Set

from ..item import OCRItem
from ..position import Position
from ..bullet.locator import locate_circle
from .plotter import Plotter
from utils.helpers import get_slide_image_from_s3


class GroupWords():
    def __init__(self, ocr_items, width, height, file_name, row_distance=10, max_distance=20, horizontal_alignment_threshold=10, enclose_margin=5, island_dist=50, debug=False):
        self.ocr_items = ocr_items
        self.dists = self.calc_distance_matrix(ocr_items)
        self.width = width
        self.height = height
        self.file_name = file_name
        self.row_distance = row_distance
        self.max_distance = max_distance
        self.horizontal_alignment_threshold = horizontal_alignment_threshold
        self.enclose_margin = enclose_margin
        self.island_dist = island_dist
        self.plotter = Plotter("steps", self.file_name)
        self.debug = debug

    def merge_words_horizontally(self, ocr_items):
        if len(ocr_items) == 0:
            return []

        merged = []
        prev_item = ocr_items[0]

        for index in range(1, len(ocr_items)):
            curr_item = ocr_items[index]
            is_near = self.is_near_position(prev_item.position, curr_item.position)
            if is_near:
                prev_item = prev_item.merge(curr_item)
            else:
                merged.append(prev_item)
                prev_item = curr_item
        merged.append(prev_item) # Append last group remaining
        return merged

    def merge_words_vertically(self, ocr_items: List[OCRItem]):
        if len(ocr_items) == 0:
            return []

        ocr_items = self.sort_rows_vertically(ocr_items)

        merged = []
        prev_item = ocr_items[0]

        circles = self.find_circle()
        circle_index = 0

        for index in range(1, len(ocr_items)):
            curr_item = ocr_items[index]

            distance = curr_item.position.get_distance(prev_item.position)
            is_near_row = distance < self.row_distance
            # is_encompassing = prev_item.position.enclose_vertically(curr_item.position, self.enclose_margin) or \
            #                   curr_item.position.enclose_vertically(prev_item.position, self.enclose_margin)

            is_aligned = prev_item.position.is_aligned_with_position_below(curr_item.position)

            other_dists = self.get_other_closest_distances(list(prev_item.ids.union(curr_item.ids)))
            is_island = (other_dists > self.island_dist) and (distance < self.row_distance + 20)

            is_with_circle = circles[circle_index].at_the_left_of(curr_item.position) \
                if circle_index < len(circles) else False
            if is_with_circle:
                circle_index += 1

            if self.file_name == '10398.jpg' and index == 11:
                print('debug')
            if (is_island or (is_near_row and is_aligned)) and (not is_with_circle):
                prev_item = prev_item.merge(curr_item)
            else:
                merged.append(prev_item)
                prev_item = curr_item

            if self.debug:
                memo = f"near_{is_near_row} island_{is_island} align_{is_aligned} bullet_{is_with_circle}"
                print(index, curr_item.text, memo)
                self.plotter.record_item(curr_item, memo)
                self.plotter.save()

        merged.append(prev_item)  # Append last group remaining
        return merged

    def find_circle(self):
        """
        Find bullet points in slide image and return positions
        :return:
        """
        image_path = get_slide_image_from_s3(self.file_name)
        xys = locate_circle(image_path)
        radius = 5
        circles_positions = [Position(xy[1] - radius, xy[0] - radius, radius*2, radius*2)
                             for xy in xys]

        # filter too close circles
        bullets = []
        if len(circles_positions) == 0:
            return bullets
        prev = circles_positions[0]
        for i in range(1, len(circles_positions)):
            curr = circles_positions[i]
            if abs(prev.center_x - curr.center_x) + abs(prev.center_y - curr.center_y) < 10:
                continue
            else:
                bullets.append(prev)
                prev = curr
        bullets.append(curr)

        return bullets

    def merge_both_direction(self, ocr_items, verbose=False):
        merged_horizontally = self.merge_words_horizontally(ocr_items)
        merged_vertically = self.merge_words_vertically(merged_horizontally)
        return merged_vertically

    def is_near_position(self, pos_a, pos_b):
        max_distance = pos_a.height
        aligned = pos_a.is_horizontally_aligned(pos_b, self.horizontal_alignment_threshold)
        near = pos_a.get_distance(pos_b) < max_distance
        # if pos_b.top == 60 and pos_b.left == 700:
        #    print(near)
        return aligned and near

    def get_other_closest_distances(self, group_word_index: List[int]) -> float:
        """
        Return distances between other items and a+b
        """
        dists_from_group = self.dists[group_word_index]
        # Invalidate distance to arguments
        dists_from_group[:, group_word_index] = float('Inf')
        return np.min(dists_from_group)

    @staticmethod
    def sort_rows_vertically(ocr_items: List[OCRItem]):
        ocr_items.sort(key=lambda item: item.position.top)
        return ocr_items

    @staticmethod
    def calc_distance_matrix(ocr_items):
        N = len(ocr_items)
        dists = np.zeros((N, N))
        for i in range(N):
            for j in range(N):
                if i == j:
                    continue
                dist = ocr_items[i].position.get_distance(ocr_items[j].position)
                dists[i][j] = dist
                dists[j][i] = dist
        return dists
