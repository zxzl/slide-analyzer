import cv2
import os
from utils.helpers import get_slide_image_from_s3

debug_images_root = "/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/layout/debug_image"


class Plotter():
    def __init__(self, job_name, file_name):
        self.job_name = job_name
        self.file_name = file_name
        image_path = get_slide_image_from_s3(self.file_name)
        self.img = cv2.imread(image_path)
        self.step = 0

    def record_item(self, ocr_item, memo=""):
        position = ocr_item.position
        topleft = (position.left, position.top)
        bottomright = (position.right, position.bottom)
        cv2.rectangle(self.img,
                      topleft,
                      bottomright,
                      (0, 0, 255),
                      2)
        cv2.putText(self.img, str(ocr_item.position) + memo, topleft, cv2.FONT_HERSHEY_SIMPLEX,
                    0.4, (0, 255, 0), 1, cv2.LINE_AA)

    def record_items(self, ocr_items):
        for ocr_item in ocr_items:
            self.record_item(ocr_item)

    def save(self):
        output_path = os.path.join(debug_images_root,
                                   f"{self.file_name} {self.job_name} {self.step}.jpg")
        cv2.imwrite(output_path, self.img)
        self.step += 1

    def clear(self):
        self.step = 0
        image_path = get_slide_image_from_s3(self.file_name)
        self.img = cv2.imread(image_path)
