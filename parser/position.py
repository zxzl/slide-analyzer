import math


class Position:
    def __init__(self, top, left, width, height):
        self.top = top
        self.left = left
        self.width = width
        self.height = height

    @classmethod
    def init_with_xys(cls, xys):
        min_y = min([int(v[1]) for v in xys])
        max_y = max([int(v[1]) for v in xys])
        min_x = min([int(v[0]) for v in xys])
        max_x = max([int(v[0]) for v in xys])
        return cls(min_y, min_x, max_x - min_x, max_y - min_y)

    @property
    def bottom(self):
        return self.top + self.height

    @property
    def right(self):
        return self.left + self.width

    @property
    def center_x(self):
        return self.left + (self.width / 2)

    @property
    def center_y(self):
        return self.top + (self.height / 2)

    def __repr__(self):
        return f"({self.left}, {self.top}, {self.right}, {self.bottom})"

    def get_area(self):
        return self.width * self.height

    def enclose(self, other):
        if not isinstance(other, Position):
            raise Exception("Position can be closed by other positions, only")
        return self.enclose_vertically(other) and self.enclose_horizontally(other)

    def enclose_horizontally(self, other, margin=0):
        return (self.top - margin <= other.top) and (self.bottom >= other.bottom)

    def enclose_vertically(self, other, margin=0):
        return (self.left - margin <= other.left) and (self.right >= other.right)

    def get_intersection_area(self, other):
        x_left = max(self.left, other.left)
        x_right = min(self.left + self.width, other.left + other.width)
        y_top = max(self.top, other.top)
        y_bottom = min(self.top + self.height, other.top + other.height)

        x_overlap = max(0, x_right - x_left)
        y_overlap = max(0, y_bottom - y_top)

        return x_overlap * y_overlap

    def get_intersection_score(self, other_position):
        area_a = self.get_area()
        area_b = other_position.get_area()
        area_ab = self.get_intersection_area(other_position)

        score = area_ab / (area_a + area_b - area_ab)
        return score

    def get_distance(self, other_position):
        w = self.get_horizontal_distance(other_position)
        h = self.get_vertical_distance(other_position)
        return math.sqrt(w ** 2 + h ** 2)

    def get_horizontal_distance(self, other_position):
        if self.left < other_position.left:  # this object ~ other object
            return max(0, other_position.left - (self.left + self.width))
        else:  # other object ~ this object
            return max(0, self.left - (other_position.left + other_position.width))

    def get_vertical_distance(self, other_position):
        if self.top < other_position.top:
            return max(0, other_position.top - (self.top + self.height))
        else:
            return max(0, self.top - (other_position.top + other_position.height))

    def is_horizontally_aligned(self, other_position, threshold=10):
        enclosed = self.enclose_horizontally(other_position) or \
                    other_position.enclose_horizontally(self)
        aligned = abs(self.top - other_position.top) < threshold and \
               abs(self.height - other_position.height) < threshold
        return enclosed or aligned

    def is_aligned_with_position_below(self, other_position):
        left = self.is_left_aligned(other_position)
        center = self.is_center_aligned(other_position)
        right = self.is_right_aligned(other_position)
        return left or center or right

    def is_left_aligned(self, other_position, threshold_close=15):
        left_distance, right_distance = self._left_right_distance(other_position)
        close_left = left_distance < threshold_close
        close_right = right_distance < threshold_close
        return close_left and not close_right

    def is_right_aligned(self, other_position, threshold_close=15):
        left_distance, right_distance = self._left_right_distance(other_position)
        close_left = left_distance < threshold_close
        close_right = right_distance < threshold_close
        return not close_left and close_right

    def is_center_aligned(self, other_position, threshold_close=40):
        left_distance, right_distance = self._left_right_distance(other_position)
        return abs(left_distance - right_distance) < threshold_close

    def _left_right_distance(self, other_position):
        left_distance = abs(self.left - other_position.left)
        right_distance = abs(self.right - other_position.right)
        return left_distance, right_distance

    def at_the_left_of(self, other_position, y_threshold=10, max_distance=80):
        in_a_row = abs(self.center_y - other_position.center_y) < y_threshold
        left = (other_position.left - self.right) < max_distance
        return in_a_row and left

    def merge(self, other_position):
        top = min(self.top, other_position.top)
        left = min(self.left, other_position.left)
        bottom = max(self.top + self.height, other_position.top + other_position.height)
        right = max(self.left + self.width, other_position.left + other_position.width)
        return Position(top, left, right - left, bottom - top)

    @staticmethod
    def merge_multiple(positions):
        if len(positions) == 0:
            return Position(0, 0, 0, 0)
        merged = positions[0]
        for p in positions[1:]:
            merged = merged.merge(p)
        return merged

    def make_dict(self):
        return {
            'top': self.top,
            'left': self.left,
            'width': self.width,
            'height': self.height
        }
