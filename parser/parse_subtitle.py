from typing import NamedTuple, List
from webvtt import WebVTT
import srt
import json
import argparse
import os


class SubtitleLine(NamedTuple):
    id: int
    start: float
    end: float
    lines: List[str]


class SubtitleParser:
    def __init__(self, input_path, output_path):
        self.input_path = input_path
        self.output_path = output_path
        self.ext = os.path.splitext(input_path)[1]
        if self.ext not in ['.srt', '.vtt']:
            raise Exception("Wrong subtitle format")

    def parse(self):
        if self.ext == '.srt':
            captions_json = self.parse_srt()
        elif self.ext == '.vtt':
            captions_json = self.parse_vtt()
        with open(self.output_path, 'w') as f:
            json.dump(captions_json, f, indent=4)
        return captions_json

    def parse_srt(self):
        with open(self.input_path, 'r', encoding='utf-8') as f:
            txt = f.read()
        lines = srt.parse(txt)
        captions_json = []
        for i, l in enumerate(lines):
            if bool(l.content):
                captions_json.append({
                    'id': i,
                    'start': l.start.total_seconds(),
                    'end': l.end.total_seconds(),
                    'lines': [l.content],
                })
        return captions_json

    def parse_vtt(self):
        captions = WebVTT().read(self.input_path)
        captions_json = []
        caption_id = 0
        for caption in captions:
            caption_id = caption_id + 1
            captions_json.append({
                'id': caption_id,
                'start': caption.start_in_seconds,
                'end': caption.end_in_seconds,
                'lines': caption.lines,
            })
        return captions_json


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Given srt / vtt subtitle file, return processed json file')
    parser.add_argument('subtitle_path')
    parser.add_argument('result_path')
    args = parser.parse_args()

    subtitle_path = args.subtitle_path
    result_path = args.result_path

    # Mock commandline arguments
    subtitle_path = "/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/video20/video20.srt"
    result_path = "/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/video20/video20_sub.json"
    subtitle_parser = SubtitleParser(subtitle_path, result_path)
    subtitle_parser.parse()
