import json


class Presentation():
    def __init__(self, slide_path):
        with open(slide_path) as f:
            slide_json = json.load(f)
        self.slides = slide_json

    @property
    def docs(self):
        sentences = []
        for slide in self.slides:
            for sentence in slide.sentences:
                sentences.append(sentence.text)
        return sentences

        # with open(slide_path) as f:
        #     slide_json = json.load(f)
        #     for slide in slide_json:
        #         slide['text'] = " ".join([w['text'] for w in slide['words']])
        # self.slides = slide_json
        # self.docs = [slide['text'] for slide in slides]

