from data.v2 import case_list
from parser.keyword.lecture_tfidf import LectureTfIdf
import json
from pprint import pprint
import os


def find_keyphrase(name, presentation, subtitle_docs):
    """
    :param slides: list of slide
    :return: Keyphrase in each slide
    """

    for slide in presentation.slides:
        slide['candidates'] = []
        for sentence in slide['sentences']:
            slide['candidates'].append(extract_candidate_words(sentence['text']))

    # Check each word in the slide are in the candidate list
    print(presentation)
    for slide in presentation.slides:
        candidate_wordIds = []
        for sentence, candidate in zip(slide['sentences'], slide['candidates']):
            for wordId in sentence['words']:
                word = slide['words'][wordId]
                text = word['text'].lower().strip()
                if text in candidate:
                    candidate_wordIds.append(wordId)
        slide['candidate_wordIds'] = candidate_wordIds

    print(presentation.slides)

    return presentation

def save_candidateWords(presentation, output_path):
    slides = []
    for slide in presentation.slides:
        slides.append({
            'id': slide['id'],
            'candidate_words': slide['candidate_wordIds']
        })
    with open(output_path, 'w') as f:
        json.dump(slides, f)

    return


def lambda_unpack(f):
    return lambda args: f(*args)


def extract_candidate_chunks(text,
                             grammar=r'KT: {(<JJ>* <NN.*>+ <IN>)? <JJ>* <NN.*>+}'):
    import itertools, nltk, string

    # exclude candidates that are stop words or entirely punctuation
    punct = set(string.punctuation)
    stop_words = set(nltk.corpus.stopwords.words('english'))
    # tokenize, POS-tag, and chunk using regular expressions
    chunker = nltk.chunk.regexp.RegexpParser(grammar)
    tagged_sents = nltk.pos_tag_sents(
        nltk.word_tokenize(sent) for sent in nltk.sent_tokenize(text))
    all_chunks = list(itertools.chain.from_iterable(
        nltk.chunk.tree2conlltags(chunker.parse(tagged_sent))
        for tagged_sent in tagged_sents))
    # join constituent chunk words into a single chunked phrase
    candidates = [' '.join(word for word, pos, chunk in group).lower()
                  for key, group in itertools.groupby(all_chunks, lambda_unpack(
            lambda word, pos, chunk: chunk != 'O')) if key]

    return [cand for cand in candidates
            if
            cand not in stop_words and not all(char in punct for char in cand)]


def extract_candidate_words(text, good_tags=set(
    ['JJ', 'JJR', 'JJS', 'NN', 'NNP', 'NNS', 'NNPS'])):
    import itertools, nltk, string

    # exclude candidates that are stop words or entirely punctuation
    punct = set(string.punctuation)
    stop_words = set(nltk.corpus.stopwords.words('english'))
    # tokenize and POS-tag words
    tagged_words = itertools.chain.from_iterable(
        nltk.pos_tag_sents(nltk.word_tokenize(sent)
                           for sent in nltk.sent_tokenize(text)))
    # filter on certain POS tags and lowercase all words
    candidates = [word.lower() for word, tag in tagged_words
                  if tag in good_tags and word.lower() not in stop_words
                  and not all(char in punct for char in word)]

    return candidates


def score_keyphrases_by_tfidf(texts, boc_texts=[], candidates='chunks'):
    import gensim, nltk

    # extract candidates from each text in texts, either chunks or words
    if boc_texts == []:
        if candidates == 'chunks':
            boc_texts = [extract_candidate_chunks(text) for text in texts]
        elif candidates == 'words':
            boc_texts = [extract_candidate_words(text) for text in texts]
    # make gensim dictionary and corpus
    dictionary = gensim.corpora.Dictionary(boc_texts)
    corpus = [dictionary.doc2bow(boc_text) for boc_text in boc_texts]
    # transform corpus with tf*idf model
    tfidf = gensim.models.TfidfModel(corpus)
    corpus_tfidf = tfidf[corpus]

    return corpus_tfidf, dictionary, tfidf


if __name__ == '__main__':
    names = case_list.names
    base_path = case_list.case_base_path
    slide_paths = case_list.slide_paths
    subtitle_paths = case_list.subtitle_paths

    cases = list(zip(names, slide_paths, subtitle_paths))

    for name, slide_path, subtitle_path in cases[-2:]:
        slide_docs, subtitle_docs = LectureTfIdf.build_corpus(slide_path,
                                                              subtitle_path)

        presentation = find_keyphrase(name, slide_docs, subtitle_docs)
        save_candidateWords(presentation, os.path.join(base_path, name, f"{name}_phrases.json") )
