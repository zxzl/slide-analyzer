import json
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from parser.keyword.presentation import Presentation

import nltk

nltk.download("punkt")
from nltk.corpus import stopwords
from nltk import download

download('stopwords')
stop_words = stopwords.words('english')


class LectureTfIdf():
    def __init__(self, sub_path, slide_path):
        slide_docs, sub_docs = self.build_corpus(slide_path, sub_path)
        corpus = slide_docs + sub_docs
        self.vectorizer = TfidfVectorizer(stop_words=stop_words,
                                          tokenizer=nltk.word_tokenize)
        self.vectorizer.fit(corpus)
        self.vocabulary = self.vectorizer.vocabulary_

    def get_document_weight(self, words):
        doc = " ".join(word for word in words)
        vectors = self.vectorizer.transform([doc])
        vectors = vectors.toarray()
        word2weight = {}
        for word in words:
            word_index = self.vocabulary.get(word.lower())
            if (word_index is not None):
                weight = vectors[0, word_index]
                word2weight[word] = weight

        # Count rank
        kv_sorted = sorted(word2weight.items(), key=lambda kv: kv[1], reverse=True)
        for rank, kv in enumerate(kv_sorted):
            weight = word2weight[kv[0]]
            word2weight[kv[0]] = (weight, rank)

        return word2weight

    @staticmethod
    def build_corpus(slide_path, subtitle_path):
        with open(subtitle_path, 'r', encoding="utf-8") as f:
            sub = json.load(f)
            sub_docs = []
            for sentence in sub['sentences']:
                sub_docs.append(sentence['text'])

        slides = Presentation(slide_path)
        return slides, sub_docs


if __name__ == '__main__':
    sub_path = "/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/video12/video12_sub.json"
    slide_path = "/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/video12/video12_slides.json"
    slideweight_path = "/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/video12/video12_slides_weight.json"

    slides = Presentation(slide_path).slides
    vectorizer = LectureTfIdf(sub_path, slide_path)
    weights = []
    for slide in slides:
        words = [word['text'] for word in slide['words']]
        weight = vectorizer.get_document_weight(words)
        weights.append(weight)

    with open(slideweight_path, 'w') as f:
        json.dump(weights, f)

