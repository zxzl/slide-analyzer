import json
import os
from typing import List

from parser.layout.loaders import load_ocr_by_jobname


def format_ocr(ocr):
    try:
        sentence_payload = [a.body['payload'] for a in ocr.annotations if a.body['stage'] == 'sentence'][0]
        sentence_payload = [s for s in sentence_payload if s['id'] != -1]
    except:
        sentence_payload = []
    try:
        title_payload = [a.body['payload'] for a in ocr.annotations if a.body['stage'] == 'title'][0]
    except Exception:
        title_payload = []

    title_ids = [index for (index, group) in enumerate(title_payload) if group != -1]
    for d in sentence_payload:
        texts = [ocr.results_flat[word_index]['text'] for word_index in d['words']]
        d['text'] = " ".join(texts)

    return {
        'id': ocr.id,
        'width': ocr.width,
        'height': ocr.height,
        'start': 0,
        'end': 0,
        'words': ocr.results_flat,
        'sentences': sentence_payload,
        'title': title_ids,
    }


def get_slides(jobname, boundaries: List[int]):
    ocrs = load_ocr_by_jobname(jobname)
    assert len(ocrs) == len(boundaries)
    slides = []
    for i, ocr in enumerate(ocrs):
        slide = format_ocr(ocr)
        slide['start'] = boundaries[i]
        slide['end'] = (boundaries[i+1] - 1) if i+1 < len(boundaries) else -1
        slides.append(slide)
    return slides


def get_slides_by_boundaries(jobname, boundaries: List[int]):
    ocrs = load_ocr_by_jobname(jobname)
    slides = []
    for i, boundary in enumerate(boundaries):
        ocr = ocrs[boundary]
        print(ocr.id)
        slide = format_ocr(ocr)
        slide['start'] = boundaries[i]
        slide['end'] = (boundaries[i+1] - 1) if i+1 < len(boundaries) else -1
        slides.append(slide)
    return slides


def get_boundaries(boundary_path):
    with open(boundary_path, 'r') as f:
        lines = f.readlines()
        boundaries = [int(l) for l in lines]
    return boundaries

if __name__ == '__main__':
    """
    Given a video, import slide and grouped words
    """
    cases_dir = '/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2'

    # numbers = range(11, 20)
    # for n in numbers:
    #     jobname = f'tenvideo_video{n}'
    #     boundary_path = os.path.join(cases_dir, f'video{n}', 'shot_boundaries.txt')
    #     boundaries = get_boundaries(boundary_path)
    #
    #     slides = get_slides(jobname, boundaries)
    #     slide_path = os.path.join(cases_dir, f'video{n}', f'video{n}_slides.json')
    #     with open(slide_path, 'w') as f:
    #         json.dump(slides, f)

    numbers = [20, 21]
    for n in numbers:
        print(n)
        boundary_path = os.path.join(cases_dir, f'video{n}', 'shot_boundaries.txt')
        boundaries = get_boundaries(boundary_path)

        jobname = f"video{n}"
        slides = get_slides_by_boundaries(jobname, boundaries)
        slide_path = os.path.join(cases_dir, f'video{n}', f'video{n}_slides.json')
        with open(slide_path, 'w') as f:
            json.dump(slides, f)
