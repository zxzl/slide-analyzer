import argparse
import os
import json
import numpy as np
from scipy import misc
from tqdm import tqdm
import jellyfish

"""
Method from '3.4 Unique Frame Extraction' in
Biswas, A. (2015). MMToC : A Multimodal Method for Table of Content Creation in Educational Videos, 621–630. 
https://doi.org/10.1145/2733373.2806253
"""

"""
GT 
9 -> 10
16 -> 17
# 47 -> 48
# 58 -> 59 
66 -> 67
71 -> 72 
112 -> 113
116 -> 117
140 -> 141
173 -> 174
174 -> 175
181 -> 182
- > 217
- > 218
-> 257
-> 269 
-> 270
-> 284
-> 285
-> 320
-> 368
-> 369
-> 408, 409
-> 425
-> 431
-> 435, 436
-> 458, 459
-> 478, 480
-> 508
-> 567
"""

def load_text(file_path):
    with open(file_path, 'r') as f:
        d = json.load(f)
        if len(d) == 0:
            return ''
        return d[0]['description']

def compare(prev_text, prev_image, curr_text, curr_image):
    image_px = prev_image.shape[0] * prev_image.shape[1]
    px_diff = curr_image - prev_image
    px_diff_sum = np.sum(px_diff)

    text_distance = jellyfish.levenshtein_distance(prev_text, curr_text)

    return (px_diff_sum / image_px, text_distance)

def suggest_cut(diffs):
    image_diffs, text_diffs = zip(*diffs)
    image_diffs = np.array(image_diffs)
    text_diffs = np.array(text_diffs)

    norm_image_diffs = image_diffs / np.max(image_diffs)
    norm_text_diffs = text_diffs / np.max(text_diffs)

    cuts = []
    for i in range(len(diffs)):
        img = norm_image_diffs[i]
        txt = norm_text_diffs[i]
        if img + txt > 0.8 or txt > 0.4:
            print("XXXX ", i+1, norm_image_diffs[i], norm_text_diffs[i])
            cuts.append(i)
        else:
            print(i+1, norm_image_diffs[i], norm_text_diffs[i])
    return cuts

def main(image_directory, json_directory):
    # Create output directory if not exists
    image_path = os.path.abspath(image_directory)
    text_path = os.path.abspath(json_directory)
    parent_path = os.path.dirname(image_path)
    output_dir = os.path.join(parent_path, 'slides')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    image_files = [os.path.join(image_path, file) for file in os.listdir(image_path)]
    text_files = [os.path.join(text_path, file) for file in os.listdir(text_path)]

    prev_index = 1
    prev_image = misc.imread(image_files[0])
    prev_text = load_text(text_files[0])

    diffs = []

    for index, image_file in tqdm(enumerate(image_files)):
        index = index + 1 # Images are using one-index
        curr_image = misc.imread(image_file)
        curr_text = load_text(text_files[index-1])

        (px_diff, txt_dist) = compare(prev_text, prev_image, curr_text, curr_image)
        diffs.append((px_diff, txt_dist))

        prev_image = curr_image
        prev_text = curr_text

    cuts = suggest_cut(diffs)
    print(cuts)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Detect image and get starting frame of each slides')
    parser.add_argument('image_directory', help='the image directory you\'d like to detect slide.')
    parser.add_argument('json_directory', help='the json directory you\'d like to detect slide.')
    args = parser.parse_args()

    main(args.image_directory, args.json_directory)
