from utils.helpers import load_images_from_folder
import cv2
from pyemd import emd_samples
import numpy as np
from tqdm import tqdm

def load_imagearray(image_paths):
    imgs = []
    print("converting images into array")
    for image_path in tqdm(image_paths):
        image_gray = cv2.imread(image_path, 0)
        imgs.append(image_gray)
    return imgs

def calc_pixel_difference(images):
    print("calculating pixel diffs")
    row, col = images[0].shape
    diffs = []
    for i in tqdm(range(0, len(images) - 1)):
        j = i + 1
        diff_image = np.abs(images[i] - images[j])
        diff = np.sum(diff_image)
        diff_norm = diff / (row * col)
        diffs.append(diff_norm)
    return diffs


def calc_histogram(image_paths):
    image_hists = []
    print("Loading histogram")
    for image_path in tqdm(image_paths):
        image_gray = cv2.imread(image_path, 0)
        hist = cv2.calcHist([image_gray], [0], None, [15], [0, 256])
        image_hists.append(hist)
    return image_hists


def calc_distances_btw_histograms(histograms):
    dists = []
    print("Loading histogram distances")
    for i in tqdm(range(0, len(histograms)-1)):
        hist_curr = histograms[i]
        hist_next = histograms[i+1]
        dist = calc_emd(hist_curr, hist_next)
        dists.append(dist)
    return dists


def calc_emd(hist1, hist2):
    bin_size = len(hist1)
    arr1 = np.array(hist1)
    arr2 = np.array(hist2)
#     distance_matrix = np.ones((bin_size, bin_size))
#     np.fill_diagonal(distance_matrix, 0)
#     print(arr1.shape, arr2.shape, distance_matrix.shape)
    dist = emd_samples(arr1, arr2)
    return dist


def find_boundary_by_hist(hists, threshold=5000):
    dists = calc_distances_btw_histograms(hists)
    boundaries = []
    for i, diff in enumerate(dists):
        if diff > threshold:
            #             print(i+1, diff)
            boundaries.append(i + 1)
    return boundaries


def run_histogram(frames_folder):
    img_files = load_images_from_folder(frames_folder)
    histograms = calc_histogram(img_files)
    boundaries = find_boundary_by_hist(histograms)
    return boundaries
