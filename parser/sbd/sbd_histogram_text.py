from parser.sbd.sbd_histogram import *
from parser.sbd.sbd_edge import calc_edgeimage, calc_edge_diffs
from slideparser.cloud_vision_wrapper import CloudVisionWrapper
from tqdm import tqdm
from pathlib import Path
from joblib import Parallel, delayed
from PIL import Image
import cv2
import json
import jellyfish


def read_ocr(ocr_path):
    with open(ocr_path) as f:
        ocr_info = json.load(f)
    words = [box['text'] for box in ocr_info['results']]
    texts = " ".join(words)
    return texts


def load_text(img_files):
    texts = []
    gcv_wrapper = CloudVisionWrapper()
    print("Loading texts")
    for file in tqdm(img_files):
        ocr_path = file.replace("jpg", "ocr.json")
        ocr_file = Path(ocr_path)
        if not ocr_file.is_file():
            print("Finding characters in {}".format(file))
            ocr_result = gcv_wrapper.run(file)
            with open(ocr_path, 'w') as f:
                json.dump(ocr_result, f, indent=4)
        text = read_ocr(ocr_path)
        texts.append(text)
    return texts


def calc_distances_btw_texts(texts, verbose):
    dists = []
    for i in range(0, len(texts)-1):
        text_curr = texts[i]
        text_next = texts[i+1]
        dist = jellyfish.levenshtein_distance(text_curr, text_next)
        dists.append(dist)
        # if verbose:
        #     print(i, text_curr)
    return dists


def find_boundary(hists, texts, threshold_histogram=1000, threshold_text=0.6, **kwargs):
    verbose = kwargs.get('verbose', False)
    hist_dists = calc_distances_btw_histograms(hists)
    text_dists = calc_distances_btw_texts(texts, verbose=verbose)
    assert len(hist_dists) == len(text_dists)

    boundaries = []
    for i in range(0, len(hist_dists)):
        histogram_diff = hist_dists[i]
        text_diff = text_dists[i]
        changed_histogram = histogram_diff > threshold_histogram

        text_len = max((len(texts[i]) + len(texts[i+1])) / 2, 1) # Not to divide by zero..
        text_score = (text_diff / text_len)
        changed_text = text_score > threshold_text
        if verbose:
            print(i + 1, histogram_diff, text_score, )
        # if changed_histogram or changed_text:
        if changed_histogram and changed_text:
            print(i + 1, changed_histogram, histogram_diff, changed_text, text_diff)
            boundaries.append(i + 1)
    return boundaries


def find_boundary_edge_text(edges, texts, threshold_edge=0.1, threshold_text=0.6, **kwargs):
    verbose = kwargs.get('verbose', False)
    edge_scores = calc_edge_diffs(edges)
    text_dists = calc_distances_btw_texts(texts, verbose)
    assert len(edge_scores) == len(text_dists)

    boundaries = []
    for i in range(0, len(edge_scores)):
        edge_diff = edge_scores[i]
        text_diff = text_dists[i]
        changed_histogram = edge_diff > threshold_edge

        text_len = max((len(texts[i]) + len(texts[i+1])) / 2, 1) # Not to divide by zero..
        text_score = (text_diff / text_len)
        changed_text = text_score > threshold_text
        if verbose:
            print(i + 1, edge_diff, text_score, )
        # if changed_histogram or changed_text:
        if changed_histogram and changed_text:
            print(i + 1, changed_histogram, edge_diff, changed_text, text_diff)
            boundaries.append(i + 1)
    return boundaries


def filter_boundaries(boundaries, min_gap=2):
    filtered = []
    for i in range(len(boundaries) - 1):
        current = boundaries[i]
        next = boundaries[i + 1]

        if next - current >= min_gap:
            filtered.append(current)
    return filtered


def run(frames_folder, verbose=False):
    img_files = load_images_from_folder(frames_folder)
    slide_texts = load_text(img_files)

    # histograms = calc_histogram(img_files)
    edges = calc_edgeimage(img_files)

    boundaries = find_boundary_edge_text(edges, slide_texts, verbose=verbose)
    boundaries_filtered = filter_boundaries(boundaries)

    return boundaries_filtered
