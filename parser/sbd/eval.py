import json

from parser.sbd.sbd_edge import run_edge
from parser.sbd.sbd_histogram import run_histogram
from parser.sbd.sbd_histogram_text import run as run_hybrid
from parser.sbd.cases import cases


def load_answers(answer_path):
    with open(answer_path, 'r') as f:
        lines = f.readlines()
    new_frames = [int(line.split()[0]) for line in lines]
    return new_frames


def measure_performance(answers, guesses):
    trueset = set(answers)
    positiveset = set(guesses)

    if len(trueset) == 0:
        print("No answers")
        return
    if len(positiveset) == 0:
        print("No boundaries found")
        return

    tp = trueset & positiveset
    fp = list(positiveset - trueset)
    tn = list(trueset - positiveset)
    fp.sort()
    tn.sort()

    recall = len(tp) / len(trueset)
    precision = len(tp) / len(positiveset)
    #     print(recall, len(tp), len(trueset))
    print("Recall : {0:.2f}%".format(recall * 100))
    print("Precision: {0:.2f}%".format(precision * 100))
    print("FP", fp)
    print("TN", tn)
    return recall, precision


if __name__ == '__main__':
    # Edge-based
    # runner = run_edge
    # runner = run_histogram
    runner = run_hybrid

    cases_to_run = cases
    # cases_to_run = [cases[6]]
    results = []

    for case in cases_to_run:
        case_id = case['id']
        frames = case['frames']
        answer_path = case['answer_path']

        print("---{}---".format(case_id))
        answers = load_answers(answer_path)
        boundaries = runner(frames, True)
        result = measure_performance(answers, boundaries)
        results.append((case_id, result))
    print(json.dumps(results, indent=4))
    print()
