import os
import glob
from PIL import Image
import numpy as np
import sys
import cv2
from tqdm import tqdm
from pyemd import emd_samples
from utils.helpers import load_images_from_folder


def compare_edge(edge1, edge2, ratio_blocks_changed=0.3,
                 ratio_single_block_changed=0.1, num_split_row=20,
                 num_split_col=20):
    # TODO: Calculate edge difference image first !!

    edge1 = np.array(edge1)
    edge2 = np.array(edge2)

    # Split into parts
    edge1_rows = np.array_split(edge1, num_split_row)
    edge2_rows = np.array_split(edge2, num_split_row)
    edge1_blocks = [np.array_split(row, num_split_col, axis=1) for row in
                    edge1_rows]
    edge2_blocks = [np.array_split(row, num_split_col, axis=1) for row in
                    edge2_rows]

    num_changed_block = 0

    rows, cols = len(edge1_blocks), len(edge1_blocks[0])
    for i in range(rows):
        for j in range(cols):
            block_1 = np.where(edge1_blocks[i][j] > 0, 1, 0)
            block_2 = np.where(edge2_blocks[i][j] > 0, 1, 0)

            block_width, block_height = block_1.shape
            block_diff = np.sum(np.abs(block_1 - block_2)) / (
                    block_width * block_height)
            if block_diff > ratio_single_block_changed:
                num_changed_block += 1

    ratio = num_changed_block / (rows * cols)
    changed = ratio > ratio_blocks_changed
    return changed, ratio


def find_boundary_by_edge(edges):
    boundaries = []
    for i in range(1, len(edges)):
        prev_edge = edges[i - 1]
        current_edge = edges[i]

        changed, ratio = compare_edge(prev_edge, current_edge)
        if changed:
            # print('TRUE', i, ratio)
            boundaries.append(i)
    return boundaries


def calc_edge_diffs(edges):
    print("Calculating edge diffs")
    ratios = []
    for i in tqdm(range(len(edges) - 1)):
        prev_edge = edges[i]
        current_edge = edges[i+1]

        _, ratio = compare_edge(prev_edge, current_edge)
        ratios.append(ratio)

    return ratios


def calc_edgeimage(image_paths, canny_threshold1=100, canny_threshold2=200):
    print("Calculating edges")
    image_edges = []
    for image_path in tqdm(image_paths):
        img_gray = cv2.imread(image_path, 0)
        edge = cv2.Canny(img_gray, canny_threshold1, canny_threshold2)
        image_edges.append(edge)
    return image_edges


def run_edge(frames_folder):
    img_files = load_images_from_folder(frames_folder)
    edges = calc_edgeimage(img_files)
    boundaries = find_boundary_by_edge(edges)
    return boundaries
