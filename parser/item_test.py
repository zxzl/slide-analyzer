from item import TextItem
from position import Position

p1 = Position(31, 35, 473, 245)
t1 = TextItem(0, 0, p1, "Hash property 1: Collision-free Nobody can find x and y such that x1= y and H(x)=H(y) H(x) = H(y)")

p2 = Position(31, 35, 473, 247)
t2 = TextItem(0, 0, p2, "Hash property 1: Collision-free Nobody can find x and y such that x1= y and H(x)=H(y) H(x) = H(y)")

print(t1.check_similar(t2))