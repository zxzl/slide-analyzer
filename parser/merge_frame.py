import argparse
import os
import json

from item import TextItem
from position import Position


def merge_items(items):
    # Check if larger item can merge smaller one
    index_larger = 0
    while index_larger < len(items):
        index_smaller = 0
        while index_smaller < len(items):
            if index_smaller == index_larger:
                index_smaller += 1
                continue
            big_item = items[index_larger]
            small_item = items[index_smaller]
            if (big_item.check_contain(small_item)):
                items.remove(small_item)
                if index_smaller < index_larger:
                    index_larger -= 1
            else:
                index_smaller += 1
        index_larger += 1
    return items

def update_items(prev_items, frame, time_index):
    print(time_index)
    if time_index == 436:
        print("Here :D")
    new_live_items = []
    new_items = [TextItem(time_index, time_index, Position.init_with_xys(item['vertices']), item['description']) for item in frame]

    # Merge new items to existing items
    for old_item in prev_items['live']:
        new_index = 0
        while new_index < len(new_items):
            new_item = new_items[new_index]
            if old_item.check_similar(new_item) or old_item.check_contain(new_item):
                del new_items[new_index]
                old_item.end = time_index
            else:
                new_index += 1

    # Move dead items
    for old_item in prev_items['live']:
        if old_item.end != time_index:
            prev_items['dead'].append(old_item)
        else:
            new_live_items.append(old_item)

    # Create new items that are not merged
    for item in new_items:
        new_live_items.append(item)

    # See if merge can happen within live items
    new_live_items = merge_items(new_live_items)

    return {
        "dead": prev_items['dead'],
        "live": new_live_items
    }


def main(json_folder):
    items = {
        "dead": [],
        "live": []
    }

    json_folder = os.path.abspath(json_folder)
    for filename in os.listdir(json_folder):
        file_path = os.path.join(json_folder, filename)
        with open(file_path, 'r') as f:
            frame = json.load(f)
        timestamp = int(os.path.splitext(filename)[0])
        items = update_items(items, frame, timestamp)

    parent_path = os.path.dirname(json_folder)
    with open(os.path.join(parent_path, 'items.json'), 'w') as f:
        json.dump({
            "dead": [item.make_dict() for item in items["dead"]],
            "live": [item.make_dict() for item in items["live"]]
        }, f)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Merge jsons into items with start and end')
    parser.add_argument('input_directory', help='the directory you\'d like to detect text in.')
    args = parser.parse_args()

    main(args.input_directory)
