from unittest import TestCase
from pptx import Presentation
import os

from schemas.database import init_db, db_session
from schemas.models import Presentation
from .load_pptx import SlideImporter

LIBRE_OFFICE_PATH = '/Applications/LibreOffice.app/Contents/MacOS/soffice'
PDF_REPO_PATH = '/Users/shik/Downloads/pdf_repo'
SAMPLE_SLIDE_PATH = '/Users/shik/Downloads/pptx_scraping/slides/engineering/!5361756469204172616d636f.pptx'
SAMPLE_SLIDE_PATH_2 = '/Users/shik/Downloads/pptx_scraping/slides/history/02a-history-of-psych.pptx'
PRESENTATION_ID = 19


class TestSlideImporter(TestCase):
    def setUp(self):
        self.pptx_loader = SlideImporter(LIBRE_OFFICE_PATH, PDF_REPO_PATH)

    def tearDown(self):
        self.pptx_loader.cleanup()

        num_deleted = db_session.query(Presentation)\
            .filter(Presentation.topic == 'test')\
            .delete()
        db_session.commit()
        print("{} rows deleted".format(num_deleted))

    def test_load_elements(self):
        presentation = Presentation(SAMPLE_SLIDE_PATH_2)
        elements = self.pptx_loader.load_elements(SAMPLE_SLIDE_PATH_2)
        self.assertEqual(len(elements), len(presentation.slides))

    def test_convert_pptx_to_pdf(self):
        pdf_path = self.pptx_loader.convert_pptx_to_pdf(SAMPLE_SLIDE_PATH)
        self.assertTrue(os.path.isfile(pdf_path))

    def test_convert_pdf_to_pngs(self):
        pdf_path = self.pptx_loader.convert_pptx_to_pdf(SAMPLE_SLIDE_PATH)
        png_dir = self.pptx_loader.convert_pdf_to_pngs(pdf_path)

        presentation = Presentation(SAMPLE_SLIDE_PATH)
        len_pptx = len(presentation.slides)
        self.assertTrue(len(png_dir), len_pptx)

        os.remove(pdf_path)

    def test_process_presentation(self):
        self.pptx_loader.process_presentation(SAMPLE_SLIDE_PATH, 'sample')
        pass

    def test_get_initial_annotation(self):
        annotations = self.pptx_loader.get_initial_annotation(PRESENTATION_ID)
        presentation = db_session.query(Presentation).get(PRESENTATION_ID)
        self.assertEqual(len(annotations), len(presentation.slides))
