import os
from unittest import TestCase
from slideparser.save_ocr import import_image, OCRResult
from schemas.database import db_session as session
import boto3

s3 = boto3.client('s3')


class TestSaveOCR(TestCase):
    SAMPLE_IMAGE_FOLDER = '/Users/shik/Dropbox/classes/17.s/slide-analyzer/slideparser/sample'
    SAMPLE_IMAGE = os.path.join(SAMPLE_IMAGE_FOLDER, '1.jpg')
    SAMPLE_OUTPUT_PATH = os.path.join(SAMPLE_IMAGE_FOLDER, '1.resized.jpg')

    def test_upload_proper_image(self):
        import_image(self.SAMPLE_IMAGE_FOLDER, 'test_insert')

    def tearDown(self):
        query = session.query(OCRResult).filter_by(job_name='test_insert')
        for row in query:
            s3.delete_object(Bucket=os.environ.get('AWS_IMAGE_BUCKET'),
                             Key=row.file_name)
            session.delete(row)
        session.commit()

