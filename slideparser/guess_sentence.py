from schemas.models import Slide, OCRResult
from schemas.database import init_db, db_session
from .text_merger import TextMerger
from .bbox import BBox


def guess_sentence(slide):
    ocr_results = slide.ocr_results

    merger = TextMerger()
    slides_merged = []
    # Horizontally merge words..
    merged_items = merger.merge(ocr_results.id)
    return {
        'text_elements': merged_items,
        'group_ids': get_group_ids(merged_items)
    }


def guess_one_sentence(ocr_id, starting_word_id):
    merger = TextMerger()
    merged_elements = merger.merge_once(ocr_id, int(starting_word_id))
    return {
        'text_elements': merged_elements,
        'group_ids': get_group_ids(merged_elements)
    }


def get_group_ids(text_elements):
    word_group = []
    new_group_index = 0
    for text in text_elements:
        word_group += [new_group_index for _ in text.ids]
        new_group_index += 1
    return word_group
