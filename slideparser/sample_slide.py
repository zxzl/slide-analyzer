from collections import namedtuple
import bisect
import glob
import os
from tqdm import tqdm
from pptx import Presentation
import random

from utils.logging_mixin import LoggingMixin
from slideparser.load_pptx import SlideImporter


class SlideSampler(LoggingMixin):
    def __init__(self, topics_dir):
        self.topics_dir = os.path.abspath(topics_dir)
        self.topics = self._load_topics()

    def sample_slide(self, topic, num_to_sample):
        if topic not in self.topics:
            self.log.error("Wrong topic")

        topic_pptx_files = glob.glob(
            os.path.join(self.topics_dir, topic, 'pptx', '*.pptx'))
        self.log.info(
            "{} pptx files were found for {}".format(len(topic_pptx_files),
                                                     topic))

        num_slides, starting_indices, valid_pptx_files = self._count_index(
            topic_pptx_files)
        sampled_indices = random.sample(range(num_slides), num_to_sample)
        sampled_indices.sort()

        slides_to_save = self._find_slides_in_pptx(starting_indices,
                                                   sampled_indices)
        return slides_to_save, valid_pptx_files

    def _load_topics(self):
        topics = next(os.walk(self.topics_dir))[1]
        self.log.info("Topics are {}".format(topics))
        return topics

    def _count_index(self, pptx_files):
        valid_pptx_files = []
        starting_indices = []
        last_index = -1
        for file in tqdm(pptx_files):
            try:
                presentation = Presentation(file)
                starting_index = last_index + 1
                starting_indices.append(starting_index)
                last_index += len(presentation.slides)
                valid_pptx_files.append(file)
            except Exception as e:
                pass
        return last_index + 1, starting_indices, valid_pptx_files

    def _find_slides_in_pptx(self, starting_indices, sampled_indices):
        results = []
        SlidesToSave = namedtuple('SlidesToSave',
                                  ['file_index', 'slide_indices'])

        index_to_save = []
        prev_file_index = bisect.bisect_right(starting_indices,
                                              sampled_indices[0]) - 1
        for index in sampled_indices:
            file_index = bisect.bisect_right(starting_indices, index) - 1
            slide_index = index - starting_indices[file_index]
            if file_index != prev_file_index:
                results.append(SlidesToSave(prev_file_index, index_to_save))
                index_to_save = [slide_index]
                prev_file_index = file_index
            else:
                index_to_save.append(slide_index)
        results.append(SlidesToSave(prev_file_index, index_to_save))

        return results


if __name__ == '__main__':
    topics_dir = "/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/slides-v2"
    LIBRE_OFFICE_PATH = '/Applications/LibreOffice.app/Contents/MacOS/soffice'
    PDF_REPO_PATH = '/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/pdfs'

    importer = SlideImporter(LIBRE_OFFICE_PATH, PDF_REPO_PATH)
    sampler = SlideSampler(topics_dir)
    print(sampler.topics)
    topic = sampler.topics[-1]
    # # for topic in sampler.topics[5:]:
    presentations, files = sampler.sample_slide(topic, 900)
    for ppt in tqdm(presentations):
        try:
            importer.process_presentation(files[ppt.file_index],
                                          topic,
                                          ppt.slide_indices)
        except Exception as e:
            print(e)
