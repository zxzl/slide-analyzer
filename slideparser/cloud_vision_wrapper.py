import os
import io
from google.cloud import vision
from google.cloud.vision import types
from PIL import Image


class CloudVisionWrapper:
    def __init__(self):
        self.client = vision.ImageAnnotatorClient()

    def run(self, file_path):
        label = self.process_image(file_path)
        return label

    def process_image(self, image_path):
        file_name = os.path.basename(image_path)
        im = Image.open(image_path)
        width, height = im.size
        with io.open(image_path, 'rb') as image_file:
            content = image_file.read()
            image = types.Image(content=content)
            response = self.client.text_detection(image=image)
            texts = response.text_annotations
        return {
            'file_name': file_name,
            'results': [{
                'id': i,
                'text': text.description,
                'vertices': self.format_vertices(list(text.bounding_poly.vertices))
            } for i, text in enumerate(texts[1:])],
            'width': width,
            'height': height,
        }

    @staticmethod
    def format_vertices(vertices):
        xs = sorted([v.x for v in vertices])
        ys = sorted([v.y for v in vertices])
        left, right = xs[0], xs[-1]
        top, bottom = ys[0], ys[-1]
        return {
            'left': left,
            'top': top,
            'right': right,
            'bottom': bottom
        }
