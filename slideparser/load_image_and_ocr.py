import argparse
import os
from PIL import Image, ImageOps
import boto3
import glob
from tqdm import tqdm
import tempfile
import json
from parser.sbd.eval import load_answers

from utils.helpers import load_images_from_folder
from schemas.models import OCRResult
from slideparser.cloud_vision_wrapper import CloudVisionWrapper
from schemas.database import init_db, db_session

s3 = boto3.client('s3')


def import_image_and_ocr(image_path, ocr_path, job_name):
    _, ext = os.path.splitext(image_path)

    with open(ocr_path) as f:
        label = json.load(f)
    label_db = OCRResult(
        job_name=job_name,
        results=label['results'],
        width=label['width'],
        height=label['height'],
    )
    db_session.add(label_db)
    db_session.flush()
    id_from_db = label_db.id
    s3_file_name = str(id_from_db) + ext
    label_db.file_name = s3_file_name

    db_session.commit()
    s3.upload_file(image_path,
                   os.environ.get('AWS_IMAGE_BUCKET'),
                   s3_file_name)


VIDEO_ROOT = "/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2"
CASES = ["video10", "video11", "video12", "video13", "video14", "video15", "video16", "video17", "video18", "video19"]

if __name__ == '__main__':
    for case in CASES:
        video_path = os.path.join(VIDEO_ROOT, case)
        frames_path = os.path.join(video_path, 'frames')
        boundaries_path = os.path.join(video_path, 'shot_boundaries.txt')
        boundaries = load_answers(boundaries_path)
        for boundary in boundaries:
            index = '{:05d}'.format(boundary)
            frame_path = os.path.join(frames_path, index + '.jpg')
            ocr_path = os.path.join(frames_path, index +'.ocr.json')
            import_image_and_ocr(frame_path, ocr_path, 'tenvideo_' + case)


