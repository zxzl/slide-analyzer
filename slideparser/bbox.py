import math


class BBox:
    def __init__(self, top, left, width, height):
        self.top = top
        self.left = left
        self.width = width
        self.height = height

    def __str__(self):
        return "{} {} {} {}".format(self.top, self.left, self.width, self.height)

    @classmethod
    def init_with_xys(cls, xys):
        min_y = min([v[1] for v in xys])
        max_y = max([v[1] for v in xys])
        min_x = min([v[0] for v in xys])
        max_x = max([v[0] for v in xys])
        return cls(min_y, min_x, max_x - min_x, max_y - min_y)

    @classmethod
    def init_with_dict(cls, coorddict):
        return cls(
            coorddict['top'],
            coorddict['left'],
            coorddict.get('width', coorddict['right'] - coorddict['left']),
            coorddict.get('height', coorddict['bottom'] - coorddict['top'])
        )

    def get_area(self):
        return self.width * self.height

    def enclose(self, other) -> bool:
        if not isinstance(other, BBox):
            raise Exception("Position can be closed by other positions, only")
        return (self.top <= other.top) and (self.left <= other.left) and \
               (self.top + self.height) >= (other.top + other.height) and \
               (self.left + self.width) >= (other.left + other.width)

    def get_intersection_area(self, other):
        x_left = max(self.left, other.left)
        x_right = min(self.left + self.width, other.left + other.width)
        y_top = max(self.top, other.top)
        y_bottom = min(self.top + self.height, other.top + other.height)

        x_overlap = max(0, x_right - x_left)
        y_overlap = max(0, y_bottom - y_top)

        return x_overlap * y_overlap;

    def get_intersection_score(self, other_position):
        area_a = self.get_area()
        area_b = other_position.get_area()
        area_ab = self.get_intersection_area(other_position)

        score = area_ab / (area_a + area_b - area_ab)
        return score

    def get_distance(self, other_position):
        w = self.get_horizontal_distance(other_position)
        h = self.get_vertical_distance(other_position)
        return math.sqrt(w ** 2 + h ** 2)

    def get_horizontal_distance(self, other_position):
        if self.left < other_position.left:  # this object ~ other object
            return max(0, other_position.left - (self.left + self.width))
        else:  # other object ~ this object
            return max(0, self.left - (other_position.left + other_position.width))

    def get_vertical_distance(self, other_position):
        if self.top < other_position.top:
            return max(0, other_position.top - (self.top + self.height))
        else:
            return max(0, self.top - (other_position.top + other_position.height))

    def is_horizontally_aligned(self, other_position, threshold=10):
        return abs(self.top - other_position.top) < threshold and \
               abs(self.height - other_position.height) < threshold

    def is_vertically_aligned(self, other_position, threshold=10):
        return abs(self.left - other_position.left) < threshold and \
               abs(self.left + self.width - other_position.left - other_position.height) < threshold

    def merge(self, other_position):
        top = min(self.top, other_position.top)
        left = min(self.left, other_position.left)
        bottom = max(self.top + self.height, other_position.top + other_position.height)
        right = max(self.left + self.width, other_position.left + other_position.width)
        return BBox(top, left, right - left, bottom - top)

    def make_dict(self):
        return {
            'top': self.top,
            'left': self.left,
            'width': self.width,
            'height': self.height
        }
