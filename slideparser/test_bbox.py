import math
from unittest import TestCase

from slideparser.bbox import BBox


class TestBBox(TestCase):
    def test_init_with_dict(self):
        d = { 'top': 10, 'left': 10, 'right': 30, 'bottom': 25}
        box = BBox.init_with_dict(d)
        self.assertEqual(box.top, 10)
        self.assertEqual(box.left, 10)
        self.assertEqual(box.width, 20)
        self.assertEqual(box.height, 15)

    def test_get_area(self):
        p1 = BBox(10, 10, 10, 10)
        self.assertAlmostEqual(p1.get_area(), 100)

        p2 = BBox(10, 10, 20, 15)
        self.assertAlmostEqual(p2.get_area(), 300)

    def test_get_distance(self):
        p1 = BBox(10, 10, 10, 10)
        p2 = BBox(30, 20, 10, 10)
        self.assertAlmostEqual(p1.get_distance(p2), 10)
        self.assertAlmostEqual(p2.get_distance(p1), 10)

        p3 = BBox(10, 10, 10, 10)
        p4 = BBox(20, 30, 10, 10)
        self.assertAlmostEqual(p3.get_distance(p4), 10)
        self.assertAlmostEqual(p4.get_distance(p3), 10)

        p5 = BBox(10, 10, 10, 10)
        p6 = BBox(30, 30, 10, 10)
        self.assertAlmostEqual(p5.get_distance(p6), math.sqrt(200))
        self.assertAlmostEqual(p6.get_distance(p5), math.sqrt(200))

        p7 = BBox(10, 10, 10, 10)
        p8 = BBox(15, 15, 10, 10)
        self.assertAlmostEqual(p7.get_distance(p8), 0)

    def test_merge(self):
        p1 = BBox(10, 10, 10, 10)
        p2 = BBox(10, 20, 10, 10)
        p3 = p1.merge(p2)
        self.assertEqual([p3.top, p3.left, p3.width, p3.left], [10, 10, 20, 10])

    def test_horizontal_align(self):
        p1 = BBox(10, 10, 10, 10)
        p2 = BBox(9, 10, 10, 12)
        self.assertEqual(p1.is_horizontally_aligned(p2), True)

    def test_vertical_align(self):
        p1 = BBox(10, 10, 10, 10)
        p2 = BBox(10, 9, 12, 10)
        self.assertEqual(p1.is_vertically_aligned(p2), True)
