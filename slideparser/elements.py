from slideparser.bbox import BBox


class Element():
    def __init__(self, pos):
        self.bbox: BBox = pos


class TextElement(Element):
    def __init__(self, pos, txt, ids=None):
        super(TextElement, self).__init__(pos)
        self.text: str = txt
        self.ids = ids

    def __str__(self):
        return self.text

    @property
    def dict(self):
        return {
            'ids': list(self.ids),
            'text': self.text,
            'bbox': self.bbox.make_dict(),
        }

    def merge(self, b: 'TextElement'):
        merged_text = self.text + " " + b.text
        merged_bbox = self.bbox.merge(b.bbox)
        merged_ids = self.ids.union(b.ids)
        return TextElement(merged_bbox, merged_text, merged_ids)
