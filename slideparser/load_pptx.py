from pathlib import Path
import os
import subprocess
import shutil
import glob

from pptx import Presentation
import boto3

from schemas.database import init_db, db_session
from schemas.models import Presentation as PresentationORM, Slide, OCRResult, Annotation
from slideparser.cloud_vision_wrapper import CloudVisionWrapper
from utils.logging_mixin import LoggingMixin
import utils.helpers as helpers

from .bbox import BBox
from .guess_sentence import guess_sentence

s3 = boto3.client('s3')


class SlideImporter(LoggingMixin):
    def __init__(self, libre_office_path, pdf_repo_path):
        self.libre_office_path = libre_office_path
        self.pdf_path = pdf_repo_path
        self._temp_path = os.path.abspath('./temp')
        self.cv = CloudVisionWrapper()
        Path(self._temp_path).mkdir(parents=True, exist_ok=True)
        Path(self.pdf_path).mkdir(parents=True, exist_ok=True)

    def process_presentation(self, pptx_path, topic="", index=[]):
        if len(index) is 0:
            index = [a for a in range(self._get_slide_length(pptx_path))]

        elements = self.load_elements(pptx_path)
        slide_images = self.load_slide_images(pptx_path)
        filename = os.path.basename(pptx_path)

        elements_chosen, slide_images_chosen = [elements[i] for i in index], \
                                               [slide_images[i] for i in index]
        self.save_presentation(filename, elements_chosen, slide_images_chosen, topic)
        return

    def load_elements(self, pptx_path):
        # Analyze its' layout
        presentation = Presentation(pptx_path)
        elements = self.extract_elements(presentation)
        self.log.info("{} elements found in {}".format(len(elements), pptx_path))

        return elements

    def load_slide_images(self, pptx_path):
        # # Get Images of each slides
        self.log.debug("Converting pptx to pdf...")
        pdf_path = self.convert_pptx_to_pdf(pptx_path)
        self.log.debug("Converting pdf to pptx...")
        png_paths = self.convert_pdf_to_pngs(pdf_path)
        return png_paths

    def save_presentation(self, filename, elements, slide_images, topic):
        if len(elements) != len(slide_images):
            self.log.critical("Number of image and slide is different")

        presentation = PresentationORM(filename=filename, topic=topic)
        db_session.add(presentation)
        db_session.commit()
        self.log.info("Presentation {} uploaded".format(filename))

        img_width, img_height = helpers.measure_image_width_height(slide_images[0])
        for i in range(len(slide_images)):
            slide_element, slide_image = elements[i], slide_images[i]

            s3_filename = "{}_{}.png".format(presentation.id, i)
            s3.upload_file(slide_image,
                           os.environ.get('AWS_IMAGE_BUCKET'),
                           s3_filename)
            self.log.info("{} img was uploaded to s3".format(s3_filename))

            # Upload slide
            slide = Slide(
                presentation_id=presentation.id,
                text_objects=slide_element['texts'],
                figure_objects=slide_element['figures'],
                width=img_width,
                height=img_height,
                image_filename=s3_filename,
            )
            db_session.add(slide)
            db_session.commit()
            self.log.info("{} slide was saved in db".format(s3_filename))

            # Run OCR
            label = self.cv.run(slide_image)
            label_alchemy = OCRResult(
                job_name="presentation_{}".format(topic),
                results=label['results'],
                width=label['width'],
                height=label['height'],
                file_name=s3_filename,
                slide_id=slide.id,
            )
            db_session.add(label_alchemy)
            db_session.commit()
            self.log.info("{} was OCRed and saved".format(s3_filename))

            # Save initial annotation
            annotation = self.get_initial_annotation(slide.id)
            db_session.bulk_save_objects([annotation])
            self.log.info("{}'s initial annotation was uploaded".format(s3_filename))

        self.log.info("{} Slide for {} save".format(len(elements), filename))

    def get_initial_annotation(self, slide_id):
        slide = db_session.query(Slide).get(slide_id)
        ocr_id = slide.ocr_results.id
        texts_merged = guess_sentence(slide)
        sentence_annotation = Annotation(
            ocr_id=ocr_id,
            body={
                'stage': 'sentence_draft',
                'payload': texts_merged['group_ids']
            })
        return sentence_annotation

    def convert_pptx_to_pdf(self, pptx_path):
        name = self.extract_filename(pptx_path)
        pdf_path_copied = os.path.join(self.pdf_path, name + '.pdf')
        self.log.info("Converting {} into pdf".format(pptx_path))
        args = [self.libre_office_path, '--headless', '--convert-to', 'pdf',
                '--outdir', self.pdf_path, pptx_path]
        try:
            result = subprocess.check_output(args)
            self.log.info(result)
        except Exception:
            self.log.critical("Error while opening {}".format(pptx_path))
            return

        return pdf_path_copied

    def convert_pdf_to_pngs(self, pdf_path):
        name = self.extract_filename(pdf_path)
        output_path = os.path.join(self._temp_path, name)
        Path(output_path).mkdir(parents=True, exist_ok=True)
        os.chdir(output_path)

        #do convert
        self.log.info("Converting {} into images under {}".format(pdf_path, output_path))
        args = ['pdftoppm', '-scale-to', '700', pdf_path, 'slide', '-png']
        result = subprocess.check_output(args)
        self.log.info(result)
        pngs = glob.glob(os.path.join(output_path, '*.png'))
        pngs.sort()
        return [os.path.join(output_path, png) for png in pngs]

    def extract_elements(self, prs):
        slides = []
        for i, slide in enumerate(prs.slides):
            texts = self.extract_texts(prs, slide)
            figures = self.extract_figures(prs, slide)
            slides.append({
                'index': i,
                'texts': texts,
                'figures': figures
            })
        return slides

    def extract_texts(self, presentation, slide):
        texts = []
        for shape in slide.shapes:
            if not shape.has_text_frame:
                continue
            text = {'paragraphs': []}
            for paragraph in shape.text_frame.paragraphs:
                text['paragraphs'].append({
                    'text': paragraph.text,
                    'level': paragraph.level,
                })
            text['bbox'] = vars(self.calculate_size(presentation, shape))
            texts.append(text)
        return texts

    def extract_figures(self, presentation, slide):
        figures = []
        for shape in slide.shapes:
            if shape.has_text_frame:
                continue
            figures.append({
                'xml_type_enum': shape.shape_type,
                'xml_type': shape.shape_type.__str__(),
                'bbox': vars(self.calculate_size(presentation, shape))
            })
        return figures

    def cleanup(self):
        shutil.rmtree(self._temp_path)

    @staticmethod
    def _get_slide_length(pptx_path):
        presentation = Presentation(pptx_path)
        slide_count = len(presentation.slides)
        return slide_count

    @staticmethod
    def calculate_size(presentation, shape) -> BBox:
        """
        Return relative size of shape based on slide
        :param presentation: Presentation object from python-pptx
        :param shape: Shape object from python-pptx
        :return: BBox with relative coordinate
        """
        slide_width = presentation.slide_width
        slide_height = presentation.slide_height
        return BBox(shape.top / slide_height,
                    shape.left / slide_width,
                    shape.width / slide_width,
                    shape.height / slide_height)

    @staticmethod
    def extract_filename(p):
        base = os.path.basename(p)
        name, _ = os.path.splitext(base)
        return name
