import argparse
import os
from PIL import Image, ImageOps
import boto3
import glob
from tqdm import tqdm
import tempfile
import json

from utils.helpers import load_images_from_folder
from schemas.models import OCRResult
from slideparser.cloud_vision_wrapper import CloudVisionWrapper
from schemas.database import init_db, db_session

s3 = boto3.client('s3')


def save_result_to_json(image_path, ocr_result, id_from_db):
    json_path = image_path + '.ocr.json'
    ocr_result['image_id'] = id_from_db
    with open(json_path, 'w') as f:
        json.dump(ocr_result, f, indent=4)


def import_image(img_dir, job_name):
    image_paths = load_images_from_folder(img_dir)
    cv = CloudVisionWrapper()
    for image_path in tqdm(image_paths):
        base_path, ext = os.path.splitext(image_path)
        temp_fp = tempfile.NamedTemporaryFile()
        temp_path = temp_fp.name
        processed_image = check_image(image_path)
        if processed_image is None:
            continue
        processed_image.save(temp_fp, format='png')

        label = cv.run(temp_path)

        label_db = OCRResult(
            job_name=job_name,
            results=label['results'],
            width=label['width'],
            height=label['height'],
        )
        db_session.add(label_db)
        db_session.flush()
        id_from_db = label_db.id
        s3_file_name = str(id_from_db) + ext
        label_db.file_name = s3_file_name

        db_session.commit()
        s3.upload_file(temp_path,
                       os.environ.get('AWS_IMAGE_BUCKET'),
                       s3_file_name)
        save_result_to_json(base_path, label, id_from_db)
        temp_fp.close()


# Pipelines..
def check_image(img_path):
    """
    Delete small images + Resize images within 700x700 frame
    :param img_path: string
    :return: PIL Image object or none (when image is too small
    """
    try:
        image = Image.open(img_path)
        size = image.size;
        if min(size) > 400:
            image.thumbnail((700, 700))
            return image
    except Exception as e:
        print(e)
    return None


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dir')
    parser.add_argument('--name', default='test')
    args = parser.parse_args()
    image_folder = args.dir
    job_name = args.name

    image_folder = "/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/video21/frames"
    job_name = "video21"

    import_image(image_folder, job_name)
