from unittest import TestCase
from slideparser.text_merger import TextMerger
from slideparser.bbox import BBox


class TestTextMerger(TestCase):
    test_item_id = 11

    def test_merge_works(self):
        merger = TextMerger()
        result = merger.merge(self.test_item_id)
        print(result)

    def test_is_near_position(self):
        p1 = BBox(20, 20, 10, 10)
        p2 = BBox(20, 35, 10, 10)
        self.assertEqual(TextMerger.is_near_horizontally(p1, p2), True)

        p3 = BBox(20, 20, 10, 10)
        p4 = BBox(15, 20, 10, 15)
        self.assertEqual(TextMerger.is_near_horizontally(p3, p4), True)

    def test_load_ocr(self):
        elements = TextMerger.load_ocr(self.test_item_id)
        self.assertEqual(elements[0].text, "Backup")
