from typing import List

from schemas.database import init_db, db_session
from schemas.models import OCRResult
from slideparser.bbox import BBox
from slideparser.elements import TextElement


class TextMerger:
    MAX_DISTANCE = 20

    def merge(self, ocr_id):
        """
        Regard two words who share horizontal edges, and close (10px) to be one word
        """
        text_elements = self.load_ocr(ocr_id)
        merged_horizontally = self.merge_horizontally(text_elements)
        return merged_horizontally

    def merge_once(self, ocr_id, id_from) -> List[TextElement]:
        text_elements = self.load_ocr(ocr_id)
        if id_from >= len(text_elements):
            return []
        text_elements_sliced = text_elements[id_from:]
        merged = self.merge_horizontally_once(text_elements_sliced)
        return merged

    def merge_horizontally(self, elements: List[TextElement]):
        merged = []
        if len(elements) == 0:
            return merged
        prev_item = elements[0]
        index = 1
        while index < len(elements):
            curr_item = elements[index]
            if self.is_near_horizontally(prev_item.bbox, curr_item.bbox):   # merge
                prev_item = prev_item.merge(curr_item)
            else:
                merged.append(prev_item)
                prev_item = curr_item
            index = index + 1
        merged.append(prev_item)
        return merged

    def merge_horizontally_once(self, elements: List[TextElement]) -> List[TextElement]:
        if len(elements) <= 1:
            return elements
        prev_item = elements[0]
        index = 1
        while self.is_near_horizontally(prev_item.bbox, elements[index].bbox):
            prev_item = prev_item.merge(elements[index])
            if index + 1 < len(elements):
                index = index + 1
            else:
                break
        return [prev_item]

    def merge_vertically(self, elements: List[TextElement]):
        raise NotImplementedError

    @staticmethod
    def is_near_horizontally(pos_a, pos_b, max_distance=None):
        if max_distance is None:
            max_distance = TextMerger.MAX_DISTANCE
        max_distance = pos_a.height
        aligned = pos_a.is_horizontally_aligned(pos_b, pos_a.height)
        near = pos_a.get_distance(pos_b) < max_distance
        # if pos_b.top == 60 and pos_b.left == 700:
        #    print(near)
        return aligned and near

    @staticmethod
    def load_ocr(input_id):
        row = db_session.query(OCRResult).get(input_id)
        elements = []
        for index, word in enumerate(row.results):
            vertices = word['vertices']
            elem = TextElement(
                BBox(vertices['top'],
                     vertices['left'],
                     vertices['right'] - vertices['left'],
                     vertices['bottom'] - vertices['top']),
                word['text'],
                {index}
            )
            elements.append(elem)
        return elements

