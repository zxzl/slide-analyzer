from unittest import TestCase
from slideparser.cloud_vision_wrapper import CloudVisionWrapper


class TestCloudVisionWrapper(TestCase):
    SAMPLE_IMAGE_PATH = '/Users/shik/Dropbox/classes/17.s/slide-analyzer/slideparser/sample/1.jpg'
    SAMPLE_WIDTH = 450
    SAMPLE_HEIGHT = 338

    def setUp(self):
        self.cv = CloudVisionWrapper()

    def test_process_image(self):
        label = self.cv.process_image(self.SAMPLE_IMAGE_PATH)
        print(label)
        self.assertEqual(label['width'], self.SAMPLE_WIDTH)
        self.assertEqual(label['height'], self.SAMPLE_HEIGHT)
        for result in label['results']:
            vertices = result['vertices']
            self.assertTrue(vertices['left'] < vertices['right'])
            self.assertTrue(vertices['top'] < vertices['bottom'])
