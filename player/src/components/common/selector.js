import React from 'react'

const Selector = ({options, currentIndex, changeIndex}) => {
  return (
    <div className="buttons has-addons is-centered">
      {options.map((o, i) => {
        const activeClass = (i === currentIndex) ? 'is-active' : 'is-outlined'
        return (
          <span
            key={o + i}
            className={`button is-small is-primary ${activeClass}`}
            onClick={() => changeIndex(i)}
          >
            {o}
          </span>
        )
      })}
    </div>
  )
}

export default Selector
