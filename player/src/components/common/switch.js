import React, { PureComponent } from 'react'

class Switch extends PureComponent {
  render() {
    const { on, label, onClick } = this.props
    return (
      <div className="field" onClick={onClick}>
        <input
          type="checkbox"
          className="switch"
          checked={ on ? "checked" : ""}
          onChange={onClick} /**To supress warning.. */
        />
        <label>{label}</label>
      </div>
    )
  }
}

export default Switch

