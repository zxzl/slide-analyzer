import loglevel from 'loglevel'
import { db } from './firebase'
import _ from 'lodash/object'

const logRef = db.ref('log')
const startDate = new Date().toString()

const originalFactory = loglevel.methodFactory
loglevel.methodFactory = (methodName, logLevel, loggerName) => {
  const rawMethod = originalFactory(methodName, logLevel, loggerName)
  return message => {
    const url = new URL(window.location.href)
    const uid = url.searchParams.get("uid") || 'anonymous'
    const condition = url.searchParams.get("condition") || 0
    const videoTime = _.get(window.reactPlayer.getState(), "player.currentTime")
    const structuredLog = {
      time: Date.now(),
      uid,
      condition,
      videoTime,
      video: window.location.pathname,
      message: (typeof message === 'string') ? { 'message': message } : message,
    }
    logRef.child(uid).child(startDate).push(structuredLog)
    rawMethod(structuredLog)
  }
}

const developmentLogger = loglevel.getLogger("development")
developmentLogger.setLevel("trace")

export { developmentLogger as logger }
