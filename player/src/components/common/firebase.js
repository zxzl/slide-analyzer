import firebase from 'firebase'

const firebaseConfig = {
  'databaseURL': process.env.REACT_APP_FIREBASE_DB,
  'apiKey': process.env.REACT_APP_FIREBASE_KEY,
  'authDomain': process.env.REACT_APP_FIREBASE_DOMAIN,
}

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const db = firebase.database()
const auth = firebase.auth()

export default firebase
export { db, auth }
