import React, { Component } from 'react';
import { db } from './firebase';

const withFirebase = (WrappedComponent) => (
  class extends Component {
    constructor(props) {
      super(props)
      this.app = db.ref(props.dbPath)
      this.state = {
        items: {}
      }
    }

    componentDidMount() {
      this.app.on('value', snapshot => {
        this.getData(snapshot.val())
      })
    }

    componentWillUnmount() {
      this.app.off()
    }

    getData = (values) => {
      console.log('notes layer received data', values)
      this.setState({
        items: values
      })
    }

    addItem = (item) => {
      const newId = this.props.dbRef.push().key;
      this.updateItem(newId, item)
    }

    updateItem = (path, item) => {
      this.app.update({
        [path]: item
      })
    }

    removeItem = (path) => {
      this.app.update({ [path]: null })
    }

    objectToArray = (keyValue) => {
      return (
        keyValue ?
          Object.keys(keyValue).map(k => Object.assign(keyValue[k], {id: k})) :
          []
      )
    }

    render() {
      const { items } = this.state
      const { dbPath, ...componentProps } = this.props
      return (
        <WrappedComponent
          items={this.objectToArray(items)}
          addItem={this.addItem}
          updateItem={this.updateItem}
          removeItem={this.removeItem}
          {...componentProps}
        />
      )
    }
  }
)



export default withFirebase
