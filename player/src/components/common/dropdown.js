import React from 'react'

const Dropdown = ({ active, items, toggle, currentIndex, updateItem }) => {
  const activeclassName = active ? 'is-active' : ''
  return (
    <div className={`dropdown ${activeclassName}`}>
      <div className="dropdown-trigger">
        <button
          className="button"
          aria-haspopup="true"
          aria-controls="dropdown-menu"
          onClick={toggle}
        >
          <span>{items[currentIndex]}</span>
          <span className="icon is-small">
            {active ?
              <i className="fas fa-angle-up" aria-hidden="true"></i> :
              <i className="fas fa-angle-down" aria-hidden="true"></i>
            }
          </span>
        </button>
      </div>
      <div className="dropdown-menu" id="dropdown-menu" role="menu">
        <div className="dropdown-content">
          {items.map((name, i) => (
            <a
              key={name}
              onClick={() => updateItem(i)}
              className={`dropdown-item ${currentIndex === i ? 'is-active' : ''}`}
            >
              {name}
            </a>
          ))}
        </div>
      </div>
    </div>
  )
}

export default Dropdown
