import leftPad from 'left-pad'

const formatSeconds = (float) => {
  var sec = Math.floor(float) % 60
  var minutes = Math.floor(float / 60)
  return `${minutes}:${leftPad(sec, 2, 0)}`
}

export { formatSeconds }
