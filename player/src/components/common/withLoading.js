import React, { PureComponent } from 'react'


class WithLoading extends PureComponent {
  state = {
    data: null,
  }

  retrieve = payload => this.props.source(payload).then(data => this.setState({ data }))

  componentWillMount() {
    this.retrieve(this.props.payload)
  }

  componentWillReceiveProps(nextProps) {
    const payloadChanged = JSON.stringify(this.props.payload) !== JSON.stringify(nextProps.payload)
    if (payloadChanged) {
      this.setState({
        data: null
      })
      this.retrieve(nextProps.payload);
    }
  }

  render() {
    const { data } = this.state
    return (
      data ?
        this.props.render(data) :
        <span>Loading</span>
    )
  }
}

export default WithLoading
