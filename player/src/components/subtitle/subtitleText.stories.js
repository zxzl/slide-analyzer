import React from 'react'
import { storiesOf } from '@storybook/react'
import SubtitleText from './subtitleText'

const sampleSelection = { startOffset: 1, endOffset: 5}
const text = 'Customisable renderRange function that allow you to add tooltip on the top of user selection for exemple'
const onTextHighlighted = e => console.log(e)

storiesOf('Subtitle', module)
  .add('range', () => (
    <SubtitleText
      start={45}
      text={text}
      selection={sampleSelection}
      onTextHighlighted={onTextHighlighted}
    />
  ))
