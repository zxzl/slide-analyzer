import React, { PureComponent, Fragment } from 'react'
import Tooltip from './tooltip'
import SubtitleText from './subtitleText'

// https://stackoverflow.com/questions/47079171/react-handling-click-outside-for-multiple-elements
class SubtitleRow extends PureComponent {
  state = {
    hasSelection: false,
  }

  openTooltip = () => {
    this.setState({ hasSelection: true })
    document.addEventListener('mousedown', this.handleClickOutside)
  }

  closeTooltip = () => {
    this.setState({ hasSelection: false })
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  handleClickOutside = (e) => {
    if (this.tooltip && !this.tooltip.contains(e.target)) {
      window.getSelection().removeAllRanges()
      this.closeTooltip()
    }
  }

  handleStar = () => {
    const { start: startOffset, end: endOffset, text: selectionText } = this.selection
    const { updateItem, id, text, start } = this.props
    if (selectionText) {
      updateItem(
        id,
        {
          id,
          text: selectionText,
          type: 'SUB',
          start,
          startOffset,
          endOffset,
        }
      )
    }
    this.closeTooltip()
  }

  handleHighlight = e => {
    this.openTooltip()
    this.selection = e
  }

  render() {
    const { hasSelection } = this.state
    return (
      <div style={{position: 'relative'}}>
        <SubtitleText
          {...this.props}
          onTextHighlighted={this.handleHighlight}
        />
        {hasSelection &&
          <div ref={tooltip => this.tooltip = tooltip}>
            <Tooltip
              handleStar={this.handleStar}
            />
          </div>
        }
      </div>
    )
  }
}

export default SubtitleRow
