import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import styled from 'styled-components'

import SubtitleRow from './subtitleRow'
import { logger } from '../common/logger'

const Wrapper = styled.nav`
  max-height: 450px;
  overflow-y: auto;
  .column {
    padding-bottom: 0
  }
`

class SubtitlePanel extends Component {

  findCurrentIndex(currentTime, subtitles) {
    const index = subtitles.findIndex(s => s.end >= currentTime)
    return index
  }

  scrollToCurrent = () => {
    const currentSubtitleNode = ReactDOM.findDOMNode(this.currentRow)
    const panel = ReactDOM.findDOMNode(this.subtitlePanel)
    if (panel && currentSubtitleNode) {
      panel.scrollTop = currentSubtitleNode.offsetTop - panel.offsetTop
    }
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const { currentTime, sub } = this.props
    const sentences = (sub && sub.sentences) || []
    const currentIndex = this.findCurrentIndex(currentTime, sentences)
    if (this.currentIndex !== currentIndex) {
      // logger.debug({message: "Subtitle changed"})
      this.scrollToCurrent()
    }
    this.currentIndex = currentIndex
  }

  render() {
    const { currentTime, sub, items, ...rest } = this.props
    const sentences = (sub && sub.sentences) || []
    return (
      <Wrapper className="panel" ref={panel => this.subtitlePanel = panel}>
        {sentences.map((s, i) => {
          const { start, text } = s
          const isCurrent = i === this.currentIndex
          const subtitleId = `sub_${i}`
          const selection = items.find(s => s.id === subtitleId)
          return (
            isCurrent ?
              <SubtitleRow
                key={i}
                id={subtitleId}
                selection={selection}
                start={start}
                text={text}
                isCurrent={isCurrent}
                ref={(row) => this.currentRow = row}
                {...rest}
              /> :
              <SubtitleRow
                key={i}
                id={subtitleId}
                selection={selection}
                start={start}
                text={text}
                isCurrent={false}
                {...rest}
              />
          )
        })}
      </Wrapper>
    )
  }
}

export default SubtitlePanel
