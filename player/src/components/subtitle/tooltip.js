import React, { PureComponent } from 'react'
import styled from 'styled-components'

const TooltipDiv = styled.div`
  position: absolute;
  top: 40px;
  left: 0;
`

class Tooltip extends PureComponent {
  render() {
    // const { x, y } = this.props
    const { handleStar } = this.props
    return (
      <TooltipDiv className="buttons has-addons">
        <a
          className="button is-warning is-small"
          onClick={handleStar}
        >
          <span className="icon is-small">
            <i className="fa fa-star" style={{color: 'white'}}></i>
          </span>
        </a>
      </TooltipDiv>
    )
  }
}

export default Tooltip
