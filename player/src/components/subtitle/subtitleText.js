import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import Highlightable from 'highlightable'
import { formatSeconds } from '../common/helpers'

class SubtitleText extends PureComponent {
  render() {
    const { selection, start, text, isCurrent, onMouseUp, onTextHighlighted } = this.props;
    const startOffset = selection && selection.startOffset
    const endOffset= selection && selection.endOffset
    let ranges = []
    if (startOffset && endOffset) {
      ranges.push({start: startOffset, end: endOffset})
    }
    return (
      <div className={`columns subtitle-row ${isCurrent ? 'has-text-info current-subtitle' : ''}`}>
        <div className="column is-2 has-text-right">
          <span>{formatSeconds(start)}</span>
        </div>
        <div
          className="column is-10"
          onMouseUp={onMouseUp}
        >
          <Highlightable
            ranges={ranges}
            text={text || ''}
            onTextHighlighted={onTextHighlighted}
            enabled={!this.context.baseline}
            highlightStyle={{
              backgroundColor: '#ffdb4a'
            }}
          />
        </div>
      </div>
    )
  }
}

SubtitleText.contextTypes = {
  baseline: PropTypes.bool,
}

export default SubtitleText
