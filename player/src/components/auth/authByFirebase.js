import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import firebase from '../common/firebase'

const provider = new firebase.auth.GoogleAuthProvider();
firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)


class AuthPage extends Component {
  constructor() {
    super()
    this.state = {
      user: undefined,
    }
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({
          user: {
            uid: user.uid,
            displayName: user.displayName,
          }
        })
      }
    });
  }

  handeLogin = () => {
    firebase.auth().signInWithPopup(provider)
      .then(result => {
        console.log(result)
        this.setState({
          user: {
            uid: result.user.uid,
            displayName: result.user.displayName,
          }
        })
      })
      .catch((e) => alert(e.message))
  }

  handleLogout = () => {
    firebase.auth().signOut()
      .then(() => this.setState({ user: undefined }))
      .catch(() => alert("Something went wrong"))
  }

  renderAuthButton = user => (
    <div className="container">
      {user ?
        <a
          className="button is-warning"
          onClick={this.handleLogout}
        >
          Logout
        </a> :
        <a
          className="button is-danger"
          onClick={this.handeLogin}
        >
          Login with Google
        </a>
      }
    </div>
  )

  render() {
    const { user } = this.state
    return (
      <section className="hero">
        <div className="hero-body">
          {this.renderAuthButton(user)}
          {user &&
            <div className="container">
              <h3>{`Hi ${user.displayName}`}</h3>
              <ul>
                <Link to={`watch/0?uid=${user.uid}`}>
                  <li>Watch example</li>
                </Link>
                <Link to={`watch/1?uid=${user.uid}`}>
                  <li>Watch videoA</li>
                </Link>
                <Link to={`watch/2?uid=${user.uid}`}>
                  <li>Watch videoB</li>
                </Link>
              </ul>
            </div>
          }
        </div>
      </section>
    )
  }
}

export default AuthPage
