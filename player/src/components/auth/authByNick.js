import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class AuthPage extends Component {
  constructor() {
    super()
    this.state = {
      user: undefined,
      input: '',
    }
  }

  handeLogin = () => {
    const { user, input } = this.state
    if (!input) {
      return
    }

    this.setState({
      user: input
    })
  }

  handleLogout = () => {
    this.setState({
      user: undefined,
      input: '',
    })
  }

  handleInput = (e) => this.setState({ input: e.target.value })

  renderAuthButton = user => (
    <div className="container">
      {user ?
        <a
          className="button is-warning"
          onClick={this.handleLogout}
        >
          Logout
        </a> :
        <div>
          <div className="field">
            <label className="label">Username</label>
            <div className="control">
              <input
                className="input"
                type="text"
                placeholder="Username"
                value={this.state.input}
                onChange={this.handleInput}
              />
            </div>
          </div>
          <a
            className="button is-danger"
            onClick={this.handeLogin}
          >
            Start
          </a>
        </div>
      }
    </div>
  )

  render() {
    const { user } = this.state
    return (
      <section className="hero">
        <div className="hero-body">
          {this.renderAuthButton(user)}
          {user &&
            <div className="container">
              <h2 className="title">{`Hi ${user}`}</h2>
              <ul>
                <Link to={`watch/0?uid=${user}`}>
                  <li>Watch example</li>
                </Link>
                <Link to={`watch/1?uid=${user}&condition=0`}>
                  <li>Watch videoA (Condition 0)</li>
                </Link>
                <Link to={`watch/2?uid=${user}&condition=0`}>
                  <li>Watch videoB (Condition 0)</li>
                </Link>
                <Link to={`watch/1?uid=${user}&condition=1`}>
                  <li>Watch videoA (Condition 1)</li>
                </Link>
                <Link to={`watch/2?uid=${user}&condition=1`}>
                  <li>Watch videoB (Condition 1)</li>
                </Link>
              </ul>
            </div>
          }
        </div>
      </section>
    )
  }
}

export default AuthPage
