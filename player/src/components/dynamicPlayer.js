import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components'
import { Player, ControlBar } from 'video-react';
import firebase from './common/firebase'
import withFirebase from './common/withFirebase'
import { logger } from './common/logger'

import { SubtitlePanel } from './subtitle'
import { SlideNavigation } from './navigation'
import Header from './header'
import { AnimationWrapper } from './animation'
import { VIDEO_WIDTH } from './constants'
import { GooglePage } from './materials'
import { ConceptBoard } from './concepts'
import { Notes } from './notes'
import { Selector } from './common'

import './layout.css'

const VideoContainer = styled.div`
  padding-top: 20px;
`

const SyncedNotes = withFirebase(Notes)
const SyncedSubtitle = withFirebase(SubtitlePanel)

window.reactPlayer = null

class DynamicPlayer extends Component {
  constructor(props) {
    super()
    this.seek = this.seek.bind(this)
    const { video, userId } = props
    const dbKey = `userData/${userId}_${video.name}`
    const db = firebase.database()
    this.notePath = `${dbKey}/notes`
    this.dummyPath = `${dbKey}`
    const dummyRef = db.ref(this.dummyPath)
    dummyRef.update({'valid': true})
    this.noteRef = db.ref(this.notePath)
    this.state = {
      subtitlePanel: true,
      bottomPanelIndex: 0,
      query: video.title,
      imageSearch: true,
    }
  }

  getChildContext() {
    return { baseline: this.props.baseline }
  }

  componentDidMount() {
    this.refs.player.subscribeToStateChange(this.handleStateChange.bind(this))
    window.reactPlayer = this.refs.player
  }

  handleStateChange = (state, prevState) => this.setState({ player: state, })

  handlePanelChange = (index) => this.setState({ subtitlePanel: (index === 0) })

  changeBottomPanel = (index) => this.setState({ bottomPanelIndex: index })

  changeQuery = (query) => this.setState({ query, subtitlePanel: false })

  changeSearchImage = (boolean) => this.setState({ imageSearch: boolean })

  seek(seconds) {
    return () => {
      logger.info({
        feature: "SEEK",
        message: "Seeking",
      })
      this.refs.player.seek(seconds)
    }
  }

  seekBySentenceId = (sentenceId) => {
    const start = this.getSentenceStart(sentenceId)
    start && this.refs.player.seek(start)
  }

  getSentenceStart = (sentenceId) => {
    const { video: { sub, pairs }, currentTime } = this.props

    let pair = undefined
    const pairsList = Object.values(pairs)
    for (let slidePairs of pairsList) {
      for (let p of slidePairs) {
        if (p.item_id === sentenceId) {
          pair = p
          break
        }
      }
    }
    if (pair) {
      const matchingSub = sub.sentences.find(s => s.id === pair.subtitle_id)
      // this.refs.player.seek(matchingSub.start)
      return matchingSub.start
    }
    return undefined
  }

  slideIndex(seconds, slides) {
    const index = slides.findIndex(s => s.start <= seconds && seconds < s.end)
    return index
  }

  inflatePair(slide, subtitle, pairs) {
    const pairsInSlide = pairs[slide.id] || []
    for (let pair of pairsInSlide) {
      const sentence = slide.sentences.find(sent => sent.id === pair.item_id)
      pair.wordIds = sentence ? sentence.words : [pair.item_id]
      const matchingSubtitle = subtitle.sentences.find(s => s.id === pair.subtitle_id)
      pair.start = matchingSubtitle.start
    }
    return pairsInSlide
  }

  calcWordTiming(timedPair) {
    const word2Start = {}
    for (let pair of timedPair) {
      for (let wordId of pair.wordIds)
        word2Start[wordId] = pair.start
    }
    return word2Start
  }

  currentTime = () => {
    const { player } = this.state
    const currentTime = (player && player.currentTime) || 0
    return currentTime
  }

  currentSlide = () => {
    const currentTime = this.currentTime()
    const { video: { slides } } = this.props
    const slideIndex = this.slideIndex(currentTime, slides)
    const slide = slideIndex === -1 ? undefined : slides[slideIndex]
    return slide
  }

  render() {
    const { subtitlePanel, animationOptions, bottomPanelIndex, query, imageSearch } = this.state
    const { baseline } = this.props
    const { src, title, sub, slides, pairs, weights } = this.props.video
    const { player } = this.state;
    const currentTime = (player && player.currentTime) || 0
    const slideIndex = this.slideIndex(currentTime, slides)
    const slide = slideIndex === -1 ? undefined : slides[slideIndex]
    let wordTiming = undefined
    let slideWeight = undefined
    if (slide) {
      const timedPair = this.inflatePair(slide, sub, pairs)
      wordTiming = this.calcWordTiming(timedPair)
    }

    return (
      <div>
        <Header title={title} />
        {/* First Row */}
        <VideoContainer className="container is-flex-widescreen">
          <div id="player-wrapper">
            <Player
              ref="player"
              autoPlay
            >
              <source src={src} />
              <ControlBar autoHide={false} />
            </Player>
            <AnimationWrapper
              slide={slide}
              width={VIDEO_WIDTH}
              currentTime={currentTime}
              wordTiming={wordTiming}
              noteRef={this.noteRef}
              changeQuery={this.changeQuery}
              seekBySentenceId={this.seekBySentenceId}
              getSentenceStart={this.getSentenceStart}
            />
          </div>

          <div id="subtitle-panel">
            <Selector
              options={!baseline ? ['Subtitle', 'Resources'] : ['Subtitle']}
              currentIndex={subtitlePanel ? 0 : 1}
              changeIndex={this.handlePanelChange}
            />
            {subtitlePanel ?
              <SyncedSubtitle
                dbPath={this.notePath}
                currentTime={currentTime}
                sub={sub}
              /> :
              <GooglePage
                query={query}
                changeQuery={this.changeQuery}
                changeSearchImage={this.changeSearchImage}
                image={imageSearch}
              />
            }
          </div>
        </VideoContainer>
        <div className="container">
          <div className="tabs">
            <ul>
              <li
                className={bottomPanelIndex === 0 ? 'is-active' : ''}
                onClick={() => this.changeBottomPanel(0)}
              >
                <a>Slides</a>
              </li>
              {!baseline &&
                <li
                  className={bottomPanelIndex === 2 ? 'is-active' : ''}
                  onClick={() => this.changeBottomPanel(2)}
                >
                  <a>Notes</a>
                </li>
              }
            </ul>
          </div>
          {/* Second Row */}
          {bottomPanelIndex === 0 &&
            <SlideNavigation
              slides={slides}
              seek={this.seek}
            />
          }
          {bottomPanelIndex === 2 &&
            <SyncedNotes
              dbPath={this.notePath}
              seekBySentenceId={this.seekBySentenceId}
              seekBySecond={this.seek}
            />
          }
        </div>
      </div>
    );
  }
}

DynamicPlayer.childContextTypes = {
  baseline: PropTypes.bool,
}

export default DynamicPlayer;
