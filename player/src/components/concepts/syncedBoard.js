import React, { Component } from 'react';
import Board from './board'


const createNoteTextItem = (dbRef, text, given={}, start) => {
  const newId = dbRef.push().key
  return Object.assign(given, {
    id: newId,
    text,
    start,
    type: 'TEXT',
    position: {
      x: 0,
      y: 0,
    }
  })
}

class SyncedBoard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: {}
    }
  }

  componentDidMount() {
    const app = this.props.dbRef
    app.on('value', snapshot => {
      this.getData(snapshot.val())
    })
  }

  getData = (values) => {
    this.setState({
      items: values
    })
  }

  createItem = (e) => {
    const text = prompt("Write text")
    const newId = this.props.dbRef.push().key;
    this.updateItem(newId, {
      id: newId,
      text,
      type: 'TEXT',
      position: {
        x: 0,
        y: 0
      }
    })
  }

  updateItem = (key, item) => {
    this.props.dbRef.update({
      [key]: item
    })
  }

  removeItem = (id) => {
    this.props.dbRef.update({ [id]: null })
  }

  render() {
    const { items } = this.state
    return (
      <div>
        <button
          onClick={this.createItem}
          className="button"
        >
          Add Text
        </button>
        <Board
          items={items}
          updateItem={this.updateItem}
          removeItem={this.removeItem}
        />
      </div>
    )
  }
}

export { SyncedBoard, createNoteTextItem }
