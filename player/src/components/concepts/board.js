import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Draggable from 'react-draggable'
import Item from './item'

const BoardWrapper = styled.div`
  position: relative;
  width: 700px;
  height: 400px;
  border: 1px dashed gray;
`

const dragBound = {
  left: 0,
  right: 600,
  top: 0,
  bottom: 350,
}

class Board extends Component {
  handleDragStop = (e, data) => {
    const id = data.node.id
    const { x, y } = data
    this.props.updateItem(`${id}/position`, { x, y })
  }

  updateText = (id, newText) => {
    this.props.updateItem(`${id}/text`, newText)
  }

  render() {
    const { items, removeItem } = this.props
    return (
      <BoardWrapper>
        {items && Object.keys(items).map(id => {
          const item = items[id]
          return (
            <Draggable
              key={id}
              position={item.position}
              onStop={this.handleDragStop}
              bounds={dragBound}
            >
              <div id={id} style={{position: 'absolute'}}>
                <Item
                  {...item}
                  removeItem={removeItem}
                  updateText={this.updateText}
                />
              </div>
            </Draggable>
          )
        })}
      </BoardWrapper>
    )
  }
}

Board.propTypes = {
  items: PropTypes.object,
  updateItem: PropTypes.func,
  removeItem: PropTypes.func,
}

export default Board
