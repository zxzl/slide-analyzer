import React from 'react'
import { storiesOf } from '@storybook/react'
import firebase from '../common/firebase'
import { SyncedBoard } from './syncedBoard'

// Provide DB
const dbKey = '0_video12'
const noteRef = firebase.database().ref(`${dbKey}/notes`)

storiesOf('SyncedBoard', module)
  .add('hi', () => (
    <SyncedBoard dbRef={noteRef} />
  ))
