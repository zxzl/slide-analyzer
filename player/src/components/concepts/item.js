import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const ItemDiv = styled.div`
  border: 1px solid #dcdde1;
  padding: 2px;
  border-radius: 3px;
  display: inline-block;
  width: max-content;
  cursor: move;

`
const ItemWrapper = styled.div`
&:not(:hover) .show-hovered {
  display: none!important;
}
`

class Item extends Component {
  handleDelete = () => this.props.removeItem(this.props.id)
  handleEdit = () => {
    const { id, text, updateText } = this.props
    const message = "Please change the text"
    const newText = prompt(message, text)
    if (newText) {
      updateText(id, newText)
    }
  }

  render() {
    const { text } = this.props
    return (
      <ItemWrapper>
        <ItemDiv>
          <span>{text}</span>
        </ItemDiv>
        <div className="buttons has-addons is-right show-hovered">
          <a
            className="button is-outlined is-info is-small"
            onClick={this.handleEdit}
          >
            <span className="icon is-small">
              <i className="fas fa-edit"></i>
            </span>
          </a>
          <a
            className="button is-outlined is-danger is-small"
            onClick={this.handleDelete}
          >
            <span className="icon is-small">
              <i className="fas fa-times"></i>
            </span>
          </a>
        </div>
      </ItemWrapper>
    )
  }
}

Item.propTypes = {
  id: PropTypes.string,
  text: PropTypes.string,
  type: PropTypes.string,
}

export default Item
