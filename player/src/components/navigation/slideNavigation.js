import React, { PureComponent } from 'react';
import styled from 'styled-components'
import urlJoin from 'url-join'
import { logger } from '../common/logger'

const IMAGE_BUCKET = process.env.REACT_APP_IMAGE_BUCKET

const NavigationWrapper = styled.div`
  display: flex!important;
  flex-direction: row;
  flex-wrap: wrap;
`

const SlideWrapper = styled.div`
  width: 150px;
  margin: 0 8px 14px 0;
  cursor: pointer;
  img {
    width: 100%;
    height: auto;
    display: block;
  }
`

class Slide extends PureComponent {

  handleClick = () => {
    const { seek, slide } = this.props
    logger.info({
      feature: "SLIDE_NAVIGATION",
      action: "JUMP",
      message: "slide clicked",
      slide: slide.id,
    })
    seek(slide.start)()
  }

  render() {
    const {slide} = this.props
    return(
      <SlideWrapper
        key={slide.id}
        onClick={this.handleClick}
      >
        <img alt="" src={urlJoin(IMAGE_BUCKET, `${slide.id}.jpg`)} />
        <div className="has-text-weight-light is-size-7">
          {`#${slide.id} ${slide.start} ~ ${slide.end}`}
        </div>
      </SlideWrapper>
    )
  }
}


class SlideNavigation extends PureComponent {
  render() {
    const { slides, seek } = this.props
    return (
      <div>
        <NavigationWrapper className="box">
          {slides.map(s =>
            <Slide key={s.id} slide={s} seek={seek} />
          )}
        </NavigationWrapper>
      </div>
    )
  }
}

export default SlideNavigation
