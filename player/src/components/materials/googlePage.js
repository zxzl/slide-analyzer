import React, { PureComponent } from 'react'
import styled from 'styled-components';

import { WithLoading, Switch } from '../common'
import { searchGoogle } from '../../api'

const Wrapper = styled.nav`
  max-height: 550px;
  overflow-y: auto;
  .column {
    padding-bottom: 0
  }
`

const ItemDiv = styled.div`
  margin: 0 0 5px 0;
  img {
    width: 100%;
    height: auto;
  }
`

const TextItem = ({ item: { link, title, snippet } }) => (
  <ItemDiv>
    <a href={link} target="blank">
      <span>{title}</span>
    </a>
  </ItemDiv>
)

const ImageItem = ({ item: { link, title, image: { contextLink } } }) => (
  <ItemDiv>
    <a href={contextLink} target="blank">
      <img src={link} />
    </a>
  </ItemDiv>
)

const SearchInput = ({ text, onClick, onChange}) => (
  <div className="field has-addons">
    <div className="control">
      <input
        className="input"
        type="text"
        placeholder="Find a repository"
        value={text}
        onChange={onChange}
      />
    </div>
    <div className="control">
      <a className="button is-info" onClick={onClick}>
        Search
      </a>
    </div>
  </div>
)

class GoogleSearchResult extends PureComponent {
  render() {
    const { query, image } = this.props
    const results = this.props.results || []
    return (
      <div>
        {image ?
          results.map(item => <ImageItem key={item.link} item={item} />) :
          results.map(item => <TextItem key={item.link} item={item} />)
        }
      </div>
    )
  }
}

class GooglePage extends PureComponent {
  state = {
    userQuery: ''
  }

  toggleImageSearch = (e) => {
    const { image, changeSearchImage } = this.props
    console.log('toggle')
    changeSearchImage(!image)
  }

  handleUserInput = e => this.setState({ userQuery: e.target.value })

  submitUserInput = () => this.props.changeQuery(this.state.userQuery)

  componentWillReceiveProps(nextProps) {
    const { query } = this.props
    const newQuery = nextProps.query
    if (query !== newQuery) {
      console.log('updating')
      this.setState({
        userQuery: newQuery,
      })
    }
  }

  render() {
    const { query, image, changeQuery } = this.props
    const { userQuery } = this.state
    return (
      <Wrapper>
        <SearchInput
          text={userQuery}
          onChange={this.handleUserInput}
          onClick={this.submitUserInput}
        />
        <Switch
          on={image}
          label="Search image"
          onClick={this.toggleImageSearch}
        />
        <WithLoading
          source={searchGoogle}
          payload={{ query, image }}
          render={data => (
            <GoogleSearchResult
              query={query}
              results={data.items}
              image={image}
            />
          )}
        />
      </Wrapper>
    )
  }
}

export default GooglePage
