import React, { Component } from 'react'
import Note from './note'

class Notes extends Component {
  render() {
    const { items, ...props } = this.props
    const sortedNotes = items.sort((a, b) => a.start - b.start)
    return (
      sortedNotes.map(note => (
        <Note
          key={note.id}
          note={note}
          {...props}
        />
      ))
    )
  }
}

export { Notes }
