import React, { PureComponent } from 'react'
import { formatSeconds } from '../common/helpers'
import { logger } from '../common/logger'


class Note extends PureComponent {

  handleDelete = () => {
    const { note: { id, text }, removeItem } = this.props
    logger.info({
      feature: "BOOKMARK_LIST",
      action: "DELETE",
      text,
      id,
    })
    removeItem(id)
  }

  handleJump = () => {
    const { note: { sentenceId, id, start, text }, seekBySecond, seekBySentenceId } = this.props
    logger.info({
      feature: "BOOKMARK_LIST",
      action: "JUMP",
      text,
      id,
    })
    if (start) {
      seekBySecond(start)()
    } else {
      seekBySentenceId(sentenceId)
    }
  }

  handleEdit = () => {
    const { note, note: { id, text, comment }, updateItem } = this.props
    const message = "Please edit comment"
    const newText = prompt(message, comment)
    const newNote = Object.assign(note, { comment: newText })
    logger.info({
      feature: "BOOKMARK_LIST",
      action: "EDIT_COMMENT",
      text,
      comment,
      newText,
      id,
    })
    updateItem(id, newNote)
  }

  render() {
    const { note: { text, comment, start } } = this.props
    return (
      <div className="card">
        <div className="card-content">
          <div className="content">
            <div className="columns">
              <div className="column has-text-right">
                <span>{formatSeconds(start)}</span>
              </div>
              <div className="column is-four-fifths">
                <span className="icon has-text-warning">
                  <i className="fas fa-sticky-note"></i>
                </span>
                {text ? text : ''}
                <br />
                <span className="icon has-text-info">
                  <i className="fas fa-comments"></i>
                </span>
                <span className="is-italic">{`${comment || ''}`}</span>
              </div>
              <div className="column">
                <div className="buttons has-addons is-right show-hovered">
                  <a
                    className="button is-success is-small"
                    onClick={this.handleJump}
                  >
                    <span className="icon is-small">
                      <i className="fas fa-play"></i>
                    </span>
                  </a>
                  <a
                    className="button is-info is-small"
                    onClick={this.handleEdit}
                  >
                    <span className="icon is-small">
                      <i className="fas fa-comment"></i>
                    </span>
                  </a>
                  <a
                    className="button is-danger is-small"
                    onClick={this.handleDelete}
                  >
                    <span className="icon is-small">
                      <i className="fas fa-times"></i>
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Note
