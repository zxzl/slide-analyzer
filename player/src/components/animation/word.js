import React from 'react'
import { calcCSS } from './wordEffects'

const EffectTypes = {
  NONE: 'NONE',
  HIDE: 'HIDE',
  BLUR: 'BLUR',
}


const Word = (props) => {
  const { word: {top, left, width, height, text}, scale, toggleHighlight, id,
         clickable } = props

  const onClick = () => toggleHighlight(id)

  const effectStyle = calcCSS(props)

  const style = Object.assign({
    position: 'absolute',
    boxSizing: 'content-box',
    width: `${width * scale}px`,
    height: `${height * scale}px`,
    top: `${top * scale}px`,
    left: `${left * scale}px`,
    color: 'transparent',
    cursor: `${clickable ? 'pointer' : 'text'}`,
  }, effectStyle)

  return (
    <div
      style={style}
      className="word-cover"
      onClick={clickable ? onClick : undefined}
    >
      {text + ' '}
    </div>
  )
}

export { EffectTypes, Word }
