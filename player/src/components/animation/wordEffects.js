import { EffectTypes } from './word'


const calcCSS = ({ effectType, ...options }) => {
  if (effectType === EffectTypes.HIDE)
    return calcCSSHide(options)
  else if (effectType === EffectTypes.BLUR)
    return calcCSSBlur(options)
  else if (effectType === EffectTypes.NONE)
    return calcCSSNone(options)
  alert("NOT IMPLEMENTED YET")
  return calcCSSNone(options)
}

const calcCSSHide = ({ highlighted, showWord, emphasizeWord, bgColor }) => {
  const style = {
    backgroundColor: showWord ? 'transparent' : bgColor,
    border: showWord ? 'none' : `3px solid ${bgColor}`,
  }
  if (emphasizeWord) {
    style.borderBottom = '1px solid green'
  }
  if (highlighted)
    style.backgroundColor = 'rgba(255, 214, 98, 0.4)'

  return style
}

const calcCSSBlur = ({ highlighted, showWord, emphasizeWord, bgColor }) => {
  const showWordStyle = {
    backgroundColor: 'transparent',
    border: 'none',
  }
  const BlurWordStyle = {
    backgroundColor: bgColor,
    opacity: 0.6,
    border: `2px solid ${bgColor}`
  }
  const style = showWord ? showWordStyle : BlurWordStyle
  if (emphasizeWord)
    style.borderBottom = '1px solid green'
  if (highlighted)
    style.backgroundColor = 'rgba(255, 214, 98, 0.4)'

  return style
}

const calcCSSNone = ({ emphasizeWord, highlighted, bgColor }) => {
  const style = {
    backgroundColor: 'transparent',
    border: 'none',
  }
  if (emphasizeWord)
    style.borderBottom = '1px solid green'
  if (highlighted)
    style.backgroundColor = 'rgba(255, 214, 98, 0.4)'
}

export { calcCSS }
