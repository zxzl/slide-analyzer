import React, { Component } from 'react';
import { Dropdown } from '../common'

import { EffectTypes } from './word'

const CheckBox = ({ checked, onChange, label }) => (
  <div className="control">
    <label className="checkbox">
      <input
        type="checkbox"
        checked={checked}
        onChange={onChange}
      />
      {label}
    </label>
  </div>
)


class Controller extends Component {
  state = {
    dropdownOpen: false,
  }

  toggleDropdown = () => this.setState({ dropdownOpen: !this.state.dropdownOpen })
  updateItem = index => {
    const selectedEffect = Object.values(EffectTypes)[index]
    this.props.changeEffect(selectedEffect)
    this.setState({
      dropdownOpen: false,
    })
  }

  render() {
    const { pointingOn, textFilterOn, clickable,
      togglePointingOn, toggleTextFilter, toggleClickable, effectType } = this.props;

    const index = Object.values(EffectTypes).indexOf(effectType)

    return (
      <div className="field is-grouped" >
        {/* <CheckBox
          checked={clickable}
          onChange={toggleClickable}
          label="Highlight by Click"
        />
        <CheckBox
          checked={pointingOn}
          onChange={togglePointingOn}
          label="Smart Pointer"
        /> */}
        {/* <CheckBox
          checked={textFilterOn}
          onChange={toggleTextFilter}
          label="TextFilter"
        /> */}
        <Dropdown
          active={this.state.dropdownOpen}
          toggle={this.toggleDropdown}
          currentIndex={index}
          items={Object.values(EffectTypes)}
          updateItem={this.updateItem}
        />
      </div>
    )
  }
}

export default Controller
