import React, { Component } from 'react'
import { createNoteTextItem } from '../concepts/syncedBoard'

class TextSelection extends Component {
  state = {
    'text': ''
  }

  getText = () => {
    var selectionObj = window.getSelection()
    this.setState({
      text: selectionObj.toString()
    })
  }

  addText = () => {
    const { noteRef } = this.props
    if (this.state.text) {
      const item = createNoteTextItem(noteRef, this.state.text)
      noteRef.update({
        [item.id]: item,
      })
    }
  }

  render() {
    return (
      <div>
        <button onClick={this.getText}>
          Get Text
        </button>
        <button onClick={this.addText}>
          Add Text
        </button>
        <div>
          <strong>Selected Words</strong>: {this.state.text}
        </div>
      </div>
    )
  }
}

export default TextSelection
