import React, { Component } from 'react'

import { Word, EffectTypes } from './word'
import { KEYWORD_RATIO } from '../constants'
import Sentence from './sentence';
import { logger } from '../common/logger'
import { createNoteTextItem } from '../concepts/syncedBoard.js'
import _ from 'lodash/object'

class AnimationLayer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      notes: {}
    }
  }

  componentDidMount() {
    this.props.noteRef.on('value', snapshot => {
      this.getData(snapshot.val())
    })
  }

  getData = (notes) => {
    console.log('animation layer received data', notes)
    this.setState({ notes })
  }

  decideEffect = (word, highlightedWords, phraseWords) => {
    const { currentTime, wordTiming, pointingOn, textFilterOn, slide } = this.props
    const wordHasTiming = Boolean(wordTiming[word.id])
    const mentioned = !wordHasTiming || wordTiming[word.id] <= currentTime

    const showWord = (mentioned && pointingOn) || !pointingOn

    const isWordImportant = phraseWords.includes(word.id)
    const emphasizeWord = textFilterOn && showWord && isWordImportant

    const highlighted = Boolean(highlightedWords.has(word.id))

    return {
      showWord,
      emphasizeWord,
      highlighted,
    }
  }

  addToNote = ({id, text, words}) => {
    const { noteRef, getSentenceStart } = this.props

    const notes = this.state.notes || {}
    const existing = Object.values(notes).find(n => n.sentenceId === id)
    if (existing) {
      logger.info({
        feature: "BOOKMARK_VIDEO",
        action: "DELETE",
        message: "Deleting note",
        text,
        id,
      })
      noteRef.update({
        [existing.id]: null,
      })
    } else {
      const sentenceStart = getSentenceStart(id)
      const newItem = createNoteTextItem(noteRef, text, {sentenceId: id}, sentenceStart)
      logger.info({
        feature: "BOOKMARK_VIDEO",
        action: "ADD",
        message: "Adding note",
        text,
        id,
      })
      noteRef.update({
        [newItem.id]: newItem,
      })
    }
  }

  getBookmarkedWords = () => {
    const { slide: { sentences }} = this.props
    const { notes } = this.state
    let wordIds = new Set()
    this.state.notes && Object.values(this.state.notes).forEach(({sentenceId}) => {
      const sentence = sentences.find(s => s.id === sentenceId)
      if (sentence) {
        sentence.words.forEach(i => wordIds.add(i))
      }
    })
    return wordIds
  }

  render() {
    const { slide, width, clickable, effectType, changeQuery, ...restProps } = this.props
    if (!slide) { return null }

    const scale = width / slide.width
    const bgColor = slide.bgColor || '#EBEBE9'
    const highlightedWords = this.getBookmarkedWords()

    return (
      <div id="overlay" style={{position: 'absolute', top: '0'}}>
        {slide.words.map(w => {
          const { showWord, emphasizeWord, highlighted } = this.decideEffect(w, highlightedWords, slide.phraseWords)
          return (
            <Word
              key={w.id}
              id={w.id}
              word={w}
              scale={scale}
              // Styles
              bgColor={bgColor}
              effectType={effectType}
              highlighted={highlighted}
              showWord={showWord}
              emphasizeWord={emphasizeWord}
            />
          )
        })}
        {slide.sentences.map(s => {
          return (
            <Sentence
              key={s.id}
              scale={scale}
              bgColor={bgColor}
              {...restProps}
              // toggleHighlight={() => this.toggleHilightWords(s.words)}
              addToConceptMap={() => this.addToNote(s)}
              changeQuery={changeQuery}
              {...s}
            />
          )
        })}
      </div>
    )
  }
}

export default AnimationLayer
