import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types';


import Controller from './controller'
import Layer from './animationLayer'
import TextSelection from './textSelection'

import { EffectTypes } from './word'

class Animation extends Component {
  constructor(props, context) {
    super(props)
    const effectType = context.baseline ? EffectTypes.NONE : EffectTypes.BLUR
    this.state = {
      textFilterOn: false,
      pointingOn: true,
      clickable: true,
      effectType,
    }
  }

  toggleTextFilter = () => {
    console.log('toggle text filter')
    this.setState({ textFilterOn: !this.state.textFilterOn })
  }
  togglePointingOn = () => this.setState({ pointingOn: !this.state.pointingOn })
  toggleClickable = () => this.setState({ clickable: !this.state.clickable })
  changeEffect = effectType => this.setState({ effectType })

  render() {
    const { noteRef } = this.props
    const { baseline } = this.context
    return (
      <Fragment>
        {!baseline &&
          <Controller
            {...this.state}
            togglePointingOn={this.togglePointingOn}
            toggleTextFilter={this.toggleTextFilter}
            toggleClickable={this.toggleClickable}
            changeEffect={this.changeEffect}
          />
        }
        {/* <TextSelection noteRef={noteRef} /> */}
        {!baseline &&
          <Layer
            {...this.props}
            {...this.state}
          />
        }
      </Fragment>
    )
  }
}

Animation.contextTypes = {
  baseline: PropTypes.bool,
}

export default Animation
