import React, { Component } from 'react'
import styled from 'styled-components'
import { logger } from '../common/logger'

const SentenceWrapper = styled.div`
  &:not(:hover) .show-hovered {
    display: none!important;
  }
  // &:not(:hover) .content {
  //   display: none!important;
  // }

  &:hover {
    border: 1px dashed gray;
  }
  line-height: 110%;

`

const Buttons = styled.div`
  .button.is-small {
    font-size: 0.5em;
  }
  position: absolute;
  right: -10px;
  bottom: -15px;
`

const Controls = ({toggleHighlight, addToConceptMap, changeQuery, seekBySentenceId}) => {
  return (
    <Buttons className="buttons has-addons is-right show-hovered">
      <a
        className="button is-success is-small"
        onClick={seekBySentenceId}
      >
        <span className="icon is-small">
          <i className="fas fa-play"></i>
        </span>
      </a>
      <a
        className="button is-warning is-small"
        onClick={addToConceptMap}
      >
        <span className="icon is-small">
          <i className="fas fa-star"></i>
        </span>
      </a>
      <a
        className="button is-info is-small"
        onClick={changeQuery}
      >
        <span className="icon is-small">
          <i className="fas fa-search"></i>
        </span>
      </a>

    </Buttons>
  )
}

class Sentence extends Component {
  changeQuery = () => this.props.changeQuery(this.props.text)

  seekBySentenceId = () => {
    const { text } = this.props
    logger.info({
      feature: "OBJECT_NAVIGATION",
      action: "JUMP",
      text,
    })
    this.props.seekBySentenceId(this.props.id)
  }

  render() {
    const { id, text, box: {top, left, width, height, fontHeight},
    scale, bgColor, ...props } = this.props
    const style = {
      position: 'absolute',
      boxSizing: 'content-box',
      width: `${width * scale}px`,
      height: `${height * scale}px`,
      top: `${top * scale}px`,
      left: `${left * scale}px`,
      // fontSize: `${fontHeight * scale}px`,
      // fontSize: '15px',
      color: 'transparent',
    }
    return (
      <SentenceWrapper style={style}>
        <span className="content">{text}</span>
        <Controls
          {...props}
          changeQuery={this.changeQuery}
          seekBySentenceId={this.seekBySentenceId}
        />
      </SentenceWrapper>
    )
  }
}


export default Sentence
