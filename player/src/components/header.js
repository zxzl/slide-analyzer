import React from 'react';

const Header = ({ title }) => (
  <nav className="navbar is-primary">
    <div className="container">
      <div className="navbar-brand">
        <span className="navbar-item has-text-weight-semibold">
          {title}
        </span>
      </div>
    </div>
  </nav>
)

export default Header
