const SEARCH_APIKEY = process.env.REACT_APP_SEARCH_APIKEY
const SEARCH_CONTEXT = process.env.REACT_APP_SEARCH_CONTEXT
const SEARCH_BASE_URL = `https://www.googleapis.com/customsearch/v1?key=${SEARCH_APIKEY}&cx=${SEARCH_CONTEXT}`

function searchGoogle({query, image}) {
  let endpoint= `${SEARCH_BASE_URL}&q=${query}`
  if (image)
    endpoint += `&searchType=image`
  return fetch(endpoint).then(resp => resp.json())
}

export { searchGoogle }
