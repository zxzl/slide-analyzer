import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom'
import Auth from './components/auth'
import DynamicPlayer from './components/dynamicPlayer'

import { videos } from './data/videos'
import './video-react.css'

const Page404 = () => (
  <section className="hero">
  <div className="hero-body">
    <div className="container">
      <h1 className="title">
        Wrong page
      </h1>
      <h2 className="subtitle">
        Please visit root
      </h2>
    </div>
  </div>
</section>
)

class App extends Component {

  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Auth} />
          <Route
            exact
            path="/watch/:vid"
            render={props => {
              const { vid } = props.match.params
              const params = new URLSearchParams(props.location.search)
              const uid = params.get('uid') || 0
              const baseline = params.get('condition') == 1
              return (
                <DynamicPlayer
                  video={videos[Number(vid)]}
                  userId={uid}
                  baseline={baseline}
                />
              )
            }}
          />
          <Route component={Page404} />
        </Switch>
      </Router>
    );
  }
}

export default App;
