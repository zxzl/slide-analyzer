from matcher import *
from matcher.type_hints import Reference
from collections import namedtuple
import logging
import json
import sys
import os
import time

from benchmark.cases import cases

BASE_PATH = os.path.abspath(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


class Runner:
    MatcherInfo = namedtuple('MatcherInfo', ['matcher', 'description'])
    MatchResult = namedtuple('MatchResult', ['case_name', 'num_data', 'num_guess', 'num_correct'])

    matcher_available = {
        'syntactic': MatcherInfo(SyntacticMatcher, 'Search only for exact inclusion'),
        'semantic_naive': MatcherInfo(SemanticNaiveMatcher, 'Search using word embedding distance'),
        'semantic_syntactic': MatcherInfo(SemanticSyntacticMatcher, 'Search using word distance, but with subtrees'),
        'semantic_ordered': MatcherInfo(SemanticOrderedMatcher,
                                        "Search using word distance, within subtree, with order constraint"),
        'semantic_permutation': MatcherInfo(PermutationMatcher, "Search best order from whole orderings"),
        'semantic_syntactic_optional': MatcherInfo(SemanticSyntacticOptional, "Search pairs of words~subtitle that has minimum similarity of sub phrases"),
        'nw': MatcherInfo(NeedlemanWunschCharacterMatcher, "Calculate using alignment score from 'needleman-wunsch"),
        # 'sequenceWMD': MatcherInfo(SequenceLocalAlignmentMatcher, "Utilize sentence-level sequence "),
        'tfidfVectorNaive': MatcherInfo(TfIdfVectorMatcher, "Calculate similarity using tf-idf weighted bag of word vector")
    }

    def __init__(self, matcher_name, verbose=False, matcher_options={}):
        assert matcher_name in self.matcher_available, '{} is not valid matcher name. Valid options: {}'.format(matcher_name, self.matcher_available)
        self.matcher_name = matcher_name
        self.matcher_options = matcher_options
        self.results = []
        self.verbose = verbose
        self.references = []
        self.logger = self.get_logger()
        self.answers = []

    def run(self, name, slides_path, subtitle_path, answer_path):
        self.logger.debug("New case {}".format(name))
        self.answers = Runner.read_answer(answer_path)

        matcher = self.matcher_available[self.matcher_name].matcher(
            slides_path, subtitle_path, self.answers)
        references = matcher.match()

        self.references += references
        result = self.evaluate(name, references, self.answers)
        if self.verbose:
            self.print_result(result)

        self.results.append(result)

    def run_case(self, c):
        self.run(c['name'], c['slides_path'], c['subtitle_path'], c['answer_path'])

    def run_cases(self, case_list):
        for c in case_list:
            self.run_case(c)

    def report(self):
        for r in self.results:
            self.print_result(r)

    def print_result(self, result: MatchResult):
        self.logger.info(json.dumps({
            'total_dataset': result.num_data,
            'total_guess': result.num_guess,
            'total_answer': result.num_correct
        }, indent=2))

    def get_logger(self):
        logger = logging.getLogger('matcher_runner')

        fh = logging.FileHandler(os.path.join(BASE_PATH, 'benchmark/logs/{}_{}.log'.format(time.asctime(), self.matcher_name)), encoding='utf-8')
        fh.setLevel(logging.DEBUG)
        logger.addHandler(fh)

        sh = logging.StreamHandler(sys.stdout)
        formatter = logging.Formatter('[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s > %(message)s')
        sh.setFormatter(formatter)
        sh.setLevel(logging.INFO)
        if self.verbose:
            sh.setLevel(logging.DEBUG)
        logger.addHandler(sh)

        logger.setLevel(logging.DEBUG)

        return logger

    @staticmethod
    def read_answer(answer_path):
        with open(answer_path, 'r') as f:
            answers = json.load(f)

        answer_references = []
        for slide in answers:
            for pair in slide['pairs']:
                answer_references.append(Reference(slide['slide_id'], pair['word_id'], pair['sentence_id']))

        return answer_references

    @staticmethod
    def evaluate(case_name, result_references, truth_references):
        count_correct = 0
        for ref in truth_references:
            if ref in result_references:
                count_correct += 1

        return Runner.MatchResult(
            case_name, len(truth_references), len(result_references), count_correct)


if __name__ == '__main__':
    # Syntactic Matcher
    # runner_syntactic = Runner('syntactic', verbose=True)
    # runner_syntactic.run_case(cases[-1])
    # runner_syntactic.report()

    # Syntactic-Semantic-Optional
    # runner_sso = Runner('semantic_syntactic_optional', verbose=True)
    # runner_sso.run_case(cases[-1])
    # runner_sso.report()

    # Semantic Matcher (dup)
    # runner_semantic = Runner('semantic')
    # runner_semantic.run_cases(cases)
    # runner_semantic.report_total()

    # Semantic Syntactic Matcher
    # runner_semantic_syntactic = Runner('semantic_syntactic', verbose=True)
    # runner_semantic_syntactic.run_case(cases[-1])
    # runner_semantic_syntactic.report_accuracy()

    # Semantic Ordered Matcher
    # runner_semantic_ordered = Runner('semantic_ordered', verbose=True)
    # runner_semantic_ordered.run_case(cases[-1])
    # runner_semantic_ordered.run_cases(cases[0])
    # runner_semantic_ordered.report_total()
    # runner_semantic_ordered.print_detail()

    # Permutation based Matcher
    # runner_permutation_naive = Runner('semantic_permutation', verbose=True)
    # runner_permutation_naive = Runner('semantic_permutation')
    # runner_permutation_naive.run_case(cases[6])
    # # runner_permutation_naive.run_cases(cases)
    # runner_permutation_naive.report_total()
    # runner_permutation_naive.print_detail()

    # NW Matcher
    # runner_nw = Runner('sequenceWMD', verbose=True)
    # runner_nw.run_case(cases[-1])
    # runner_nw.report()

    # TF-IDF Matcher
    runner_tfidf = Runner('tfidfVectorNaive', verbose=True)
    runner_tfidf.run_case(cases[-1])
    runner_tfidf.report()
