cases = [
    {
        'name': 'video1',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video1/video1_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video1/video1_sub.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video1/video1_pairs.json',
    },
    {
        'name': 'video3',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video3/video3_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video3/video3_subs.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video3/video3_pairs_gt.json',
    },
    {
        'name': 'video4',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video4/video4_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video4/video4_subs.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video4/video4_pairs.json',
    },
    {
        'name': 'video5',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video5/video5_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video5/video5_subs.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video5/video5_pairs.json',
    },
    {
        'name': 'video6',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video6/video6_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video6/video6_subs.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video6/video6_pairs.json',
    },
    {
        'name': 'video7',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video7/video7_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video7/video7_subs.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video7/video7_pairs.json',
    },
    {
        'name': 'video8',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video8/video8_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video8/video8_subs.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video8/video8_pairs.json',
    },
    {
        'name': 'video2_v2',
        'format': '2',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/video2/video2_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/video2/video2_sub_sent.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/video2/video2_pair_sent.json',
    },
    {
        'name': 'video4_v2',
        'format': '2',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/video4/video4_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/video4/video4_sub_sent.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/data/v2/video4/video4_pair.json',
    },
]
