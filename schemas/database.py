import os
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine(os.environ.get('PG_URI'))
test_engine = create_engine(os.environ.get('PG_TEST_URI'))

db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    # This function should be called after this module is loaded!!
    import schemas.models
    Base.metadata.create_all(bind=engine)


# Test Utilities
def init_test_db():
    import schemas.models
    Base.metadata.create_all(bind=test_engine)


def get_test_session():
    return scoped_session(sessionmaker(bind=test_engine,
                                       autocommit=False,
                                       autoflush=False))


def drop_test_db():
    Base.metadata.drop_all(bind=test_engine)
