from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, \
    func, Text, Boolean
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.orm import relationship, backref, object_session
from schemas.database import Base


class Batch(Base):
    __tablename__ = 'batch'

    id = Column(Integer, primary_key=True)
    memo = Column(String)
    min_ocrid = Column(Integer)
    max_ocrid = Column(Integer)
    annotation_per_worker = Column(Integer)
    annotation_per_worker_sentence = Column(Integer)
    vote_per_worker = Column(Integer)

    annotations = relationship('Annotation', backref="Batch",
                               cascade="delete")


class OCRResult(Base):
    __tablename__ = 'ocr_results'

    id = Column(Integer, primary_key=True)
    job_name = Column(String, default='test')
    file_name = Column(String)
    width = Column(Integer)
    height = Column(Integer)
    results = Column(JSON)
    slide_id = Column(Integer, ForeignKey("slide.id", ondelete="CASCADE"))

    annotations = relationship('Annotation', backref="ocr_results",
                               cascade="delete")
    workers = relationship('Worker', backref="ocr_results", cascade="delete")
    feedbacks = relationship('Feedback', backref="ocr_results",
                             cascade="delete")
    slide = relationship("Slide", backref=backref("ocr_results", uselist=False))

    @property
    def results_flat(self):
        flat_boxes = [{
            'id': box['id'],
            'text': box['text'],
            'top': box['vertices']['top'],
            'left': box['vertices']['left'],
            'width': box['vertices']['right'] - box['vertices']['left'],
            'height': box['vertices']['bottom'] - box['vertices']['top'],
        } for box in self.results]
        return flat_boxes

    @property
    def filtered_results(self):
        return [r for r in self.results if len(r['text']) > 1]

    @property
    def valid_word_ids(self):
        return [s['id'] for s in self.filtered_results]

    @property
    def sentences(self):
        """
        Return human-generated grouping of words
        :return: Annotation with 'sentence' stage
        """
        valid_words = self.valid_word_ids
        sentence_annotation = [a for a in self.annotations
                               if a.body['stage'] == 'sentence'][0]
        for group in sentence_annotation.body['payload']:
            group['words'] = [idx for idx in group['words'] if idx in valid_words]
        return sentence_annotation


    @property
    def title(self):
        """
        Return human-generated grouping of titles
        :return: Annotation whose stage is 'title'
        """
        return [a for a in self.annotations
                if a.body['stage'] == 'title'][0]


class Annotation(Base):
    __tablename__ = 'annotation'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    body = Column(JSON)
    worker_id = Column(Integer, ForeignKey('worker.id'))
    ocr_id = Column(Integer, ForeignKey("ocr_results.id", ondelete="CASCADE"),
                    index=True)
    vote_finished = Column(Boolean, default=False, nullable=False,
                           server_default='false')
    batch_from = Column(Integer, ForeignKey('batch.id'))
    admin_approved = Column(Boolean, default=False, nullable=False,
                            server_default='false')

    votes = relationship('Vote', backref='annotation', cascade="delete")

    @property
    def is_good(self):
        total, up = 0, 0
        for vote in self.votes:
            total += 1
            if vote.up:
                up += 1
        return total >= 3 and (up > total - up)


class Vote(Base):
    __tablename__ = 'vote'
    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    up = Column(Boolean, nullable=False)

    annotation_id = Column(Integer,
                           ForeignKey("annotation.id", ondelete="CASCADE"))
    worker_id = Column(Integer, ForeignKey("worker.id", ondelete="CASCADE"))


class Worker(Base):
    __tablename__ = 'worker'

    id = Column(Integer, primary_key=True)
    alias = Column(String, default='')
    ocr_id = Column(Integer, ForeignKey("ocr_results.id", ondelete="CASCADE"))
    started_at = Column(DateTime(timezone=True), server_default=func.now(),
                        index=True)
    finished_at = Column(DateTime(timezone=True), index=True)

    annotations = relationship('Annotation', backref="worker")


class Feedback(Base):
    __tablename__ = 'feedback'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    created_by = Column(Integer, ForeignKey('worker.id'))
    slide_id = Column(Integer, ForeignKey('ocr_results.id', ondelete="CASCADE"))
    stage = Column(String)
    body = Column(Text)


class Presentation(Base):
    __tablename__ = 'presentation'

    id = Column(Integer, primary_key=True)
    topic = Column(String)
    filename = Column(String)

    slides = relationship('Slide', backref="presentation", cascade="delete")


class Slide(Base):
    __tablename__ = 'slide'

    id = Column(Integer, primary_key=True)
    presentation_id = Column(Integer,
                             ForeignKey("presentation.id", ondelete="CASCADE"))
    image_filename = Column(String)
    width = Column(Integer)
    height = Column(Integer)
    text_objects = Column(JSON)
    figure_objects = Column(JSON)
