import math

from matcher import BaseMatcher
from matcher.type_hints import Reference
from matcher.alignments import global_align


class NeedlemanWunschCharacterMatcher(BaseMatcher):
    WINDOW_SIZE = 4

    def __init__(self, slides_path, subtitle_path, answer=None, logger=None):
        super().__init__(slides_path, subtitle_path, answer, logger)

    def match_slide(self, slide, subtitles):
        """
        For each word in slide, find closest subtitle
        :param slide: slide dict
        :param subtitles: list of subtitle dict
        :return: list of tuple
        """

        references = []
        last_subtitle_index = 0
        # Tokenize words in slide
        for slide_element in slide['words']:
            max_score, max_sub_index = -1 * math.inf, -1
            for sub_index in range(
                last_subtitle_index,
                min(last_subtitle_index + self.WINDOW_SIZE, len(subtitles))):
                score = self.compute_similarity_score(slide_element, subtitles[sub_index])
                if score > max_score:
                    max_score, max_sub_index = score, sub_index
            subtitle_id = subtitles[max_sub_index]['id']
            references.append(
                Reference(slide['index'], slide_element['id'], subtitle_id)
            )
            last_subtitle_index = max_sub_index
        self.log_slide(self.logger, slide, subtitles)
        self.log_references(slide, subtitles, references)
        return references

    def compute_similarity_score(self, slide_element, subtitle) -> float:
        subtitle_text = " ".join(subtitle['lines']) \
            if 'lines' in subtitle else subtitle['text']
        return self.compute_alignment_score(subtitle_text, slide_element['text'])

    @staticmethod
    def compute_alignment_score(s1: str, s2: str):
        a1, a2, score = global_align(s1, s2)
        return score

