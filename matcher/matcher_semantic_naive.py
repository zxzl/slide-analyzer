from embeddings import GloveEmbedding
from matcher import BaseMatcher
import numpy as np


class SemanticNaiveMatcher(BaseMatcher):

    def __init__(self, slides_path, subtitle_path, answer=None, logger=None):
        super().__init__(slides_path, subtitle_path, answer, logger)
        self.glove = GloveEmbedding('common_crawl_840', d_emb=300, show_progress=False)

    def match_slide(self, slide, subtitles):
        """
        For each word in slide, find closest subtitle
        :param slide: slide dict
        :param subtitles: list of subtitle dict
        :return: list of tuple
        """
        references = []
        # Tokenize words in slide
        for word in slide['words']:
            references += self.find_most_similar_subtitle(
                slide['index'], word, subtitles)

        return references

    def find_most_similar_subtitle(self, slide_id, word, subtitles):
        """
        Match pair with the most closes subtitle to given word
        :param slide_id: int
        :param word: word dict
        :param subtitles: list of subtitle dict
        :return: list of tuple
        """
        max_similarity, max_subtitle_id = -999, -1
        for subtitle in subtitles:
            subtitle_text = " ".join(subtitle['lines'])
            try:
                similarity = self.compute_phrase_similarity_sum(word['text'], subtitle_text)
            except self.EmptyToken as e:
                continue
            if similarity > max_similarity:
                max_similarity, max_subtitle_id = similarity, subtitle['id']

        if max_subtitle_id == -1:
            return []
        return [(slide_id, word['id'], max_subtitle_id)]

    """
    Functions for compute similarities
    """
    def compute_phrase_similarity_sum(self, s1, s2):
        v1 = self.compute_sum_embedding(s1)
        v2 = self.compute_sum_embedding(s2)
        return self.compute_cos_sim(v1, v2)

    def compute_phrase_similarity_avg(self, s1, s2):
        v1 = self.compute_sum_embedding(s1, avg=True)
        v2 = self.compute_sum_embedding(s2, avg=True)
        return self.compute_cos_sim(v1, v2)

    def compute_sum_embedding(self, sentence, avg=False):
        tokens = self.tokenize(sentence)
        if len(tokens) is 0:
            raise self.EmptyToken(sentence)

        vectors = [np.array(self.glove.emb(token)) for token in tokens]

        sum_vector = np.zeros(len(vectors[0]))
        for vector in vectors:
            if vector[0] is None:
                continue
            sum_vector += vector
        if avg:
            sum_vector / len(vectors)
        return sum_vector


    @staticmethod
    def compute_cos_sim(v1, v2):
        assert v1.shape == v2.shape, "Two vectors are in wrong dimension"
        return np.inner(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2))
