import numbers
import math
import os
from unittest import TestCase
from matcher import PermutationMatcher

BASE_PATH = os.path.abspath(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


class TestPermutationMatcher(TestCase):
    def setUp(self):
        slides_path = os.path.join(BASE_PATH, 'parser/data/video1/video1_slides.json')
        subtitle_path = os.path.join(BASE_PATH, 'parser/data/video1/video1_sub.json')
        self.matcher = PermutationMatcher(slides_path, subtitle_path)
        self.sample_slide = self.matcher.slides[1]
        self.sample_words = self.sample_slide['words']
        self.sample_subtitles = self.matcher.subtitle_for_slide(self.sample_slide, self.matcher.subtitles)

    def test_match_slide(self):
        refs = self.matcher.match_slide(self.sample_slide, self.sample_subtitles)
        pass

    def test_evaluate_order_naive(self):
        orders = self.make_orders()
        sample_order = next(orders)

        score = self.matcher.evaluate_order_naive(sample_order)
        print(score)
        pass

    def test_evaluate_order_word_subtitles(self):
        orders = self.make_orders()
        sample_order = next(orders)

        score = self.matcher.evaluate_order_word_subtitles(sample_order)
        print(score)
        pass

    def test_orders_to_pairs(self):
        orders = self.make_orders()
        sample_order = next(orders)

        pairs = self.matcher.orders_to_pairs(-1, sample_order)
        self.assertEqual(pairs, [(-1, 6, 6)])
        print(pairs)

    def test_make_orders(self):
        orders = list(self.make_orders())
        num_possible_orders = self.nCr(len(self.sample_words) + len(self.sample_subtitles) - 1,
                                       len(self.sample_words))

        self.assertEqual(len(orders), num_possible_orders)

    def test_compare_sub_and_word(self):
        sample_word = self.matcher.slides[1]['words'][0]
        sample_subtitle = self.matcher.subtitles[0]

        subtitle_txt = " ".join(sample_subtitle["lines"])
        word_text = sample_word['text']

        score = self.matcher.compare_sub_and_word(subtitle_txt, word_text)
        self.assertIsInstance(score, numbers.Number)

    # Helper functions for tests
    def make_orders(self):
        orders = self.matcher.make_orders(self.sample_words, self.sample_subtitles)
        return orders

    @staticmethod
    def nCr(n, r):
        f = math.factorial
        return f(n) / f(r) / f(n-r)
