import numpy as np

from .align_doc_base import BaseAlign, Document, Sentence, Arrow


class GlobalAlign(BaseAlign):

    def __init__(self, gap_score=-1):
        self.gap_score = gap_score
        self.max_i = 0
        self.max_j = 9
        self.doc_i = []
        self.doc_j = []

    def align(self, doc_i: Document, doc_j: Document):
        self.max_i = len(doc_i)
        self.max_j = len(doc_j)
        self.doc_i = doc_i
        self.doc_j = doc_j

        score_init, pointer_init = self._initialize_matrix()
        score, pointer = self._compute_matrix(score_init, pointer_init)
        result1, result2 = self._backtrace(score, pointer)
        return result1, result2

    def _backtrace(self, score, pointer):
        aligned_i = []
        aligned_j = []

        i, j = self.max_i, self.max_j
        while True:
            p = pointer[i, j]
            self.log.debug("{} {} {}".format(p, i, j))
            if p == Arrow.NONE.value:
                break
            if p == Arrow.DIAG.value:
                aligned_j.append(self.doc_j[j - 1])
                aligned_i.append(self.doc_i[i - 1])
                i -= 1
                j -= 1
            elif p == Arrow.LEFT.value:
                aligned_j.append(self.doc_j[j - 1])
                aligned_i.append("-")
                j -= 1
            elif p == Arrow.UP.value:
                aligned_j.append("-")
                aligned_i.append(self.doc_i[i - 1])
                i -= 1
            else:
                raise Exception('wtf!')

        return aligned_i[::-1], aligned_j[::-1]

    def _compute_matrix(self, score, pointer):
        for i in range(1, self.max_i + 1):
            si = self.doc_i[i - 1]
            for j in range(1, self.max_j + 1):
                sj = self.doc_j[j - 1]

                diag_score = score[i - 1, j - 1] + self.compare_sentences(si, sj)
                up_score = score[i - 1, j] + self.gap_score
                left_score = score[i, j - 1] + self.gap_score

                if diag_score >= up_score:
                    if diag_score >= left_score:
                        score[i, j] = diag_score
                        pointer[i, j] = Arrow.DIAG.value
                    else:
                        score[i, j] = left_score
                        pointer[i, j] = Arrow.LEFT.value

                else:
                    if up_score > left_score:
                        score[i, j] = up_score
                        pointer[i, j] = Arrow.UP.value
                    else:
                        score[i, j] = left_score
                        pointer[i, j] = Arrow.LEFT.value
        return score, pointer

    def compare_sentences(self, s1: Sentence, s2: Sentence):
        raise NotImplementedError

    def _initialize_matrix(self):
        # I used numpy matrix because it's beautifully shown as 2d matrix
        # in PyCharm's debugger
        score = np.zeros((self.max_i + 1, self.max_j + 1), dtype='f')
        pointer = np.zeros((self.max_i + 1, self.max_j + 1), dtype='i')

        pointer[0, 0] = Arrow.NONE.value
        score[0, 0] = 0.0

        pointer[0, 1:] = Arrow.LEFT.value
        pointer[1:, 0] = Arrow.UP.value

        score[0, 1:] = 0
        score[1: 0] = 0

        return score, pointer
