from typing import List, NamedTuple
from enum import Enum

from utils import LoggingMixin


class Sentence(NamedTuple):
    id: int
    body: str


Document = List[Sentence]


class Arrow(Enum):
    UP = 0
    LEFT = 1
    DIAG = 2
    NONE = 3


class BaseAlign(LoggingMixin):

    def align(self, doc_i: Document, doc_j: Document):
        raise NotImplementedError

    @staticmethod
    def pp_sentences_comparision(s1: Document, s2: Document):
        assert len(s1) == len(s2)

        for s1, s2 in zip(s1, s2):
            print(s1)
            print(s2)
            print()
