import os
# Preparing pretrained w2v embeddings
from gensim.models import KeyedVectors

# Preparing stopwords
from nltk.corpus import stopwords
from nltk import download
download('stopwords')
stop_words = stopwords.words('english')

if not os.path.exists(
    '/Users/shik/gensim_data/GoogleNews-vectors-negative300.bin.gz'):
    raise ValueError("SKIP: You need to download the google news model")

model = KeyedVectors.load_word2vec_format(
    '/Users/shik/gensim_data/GoogleNews-vectors-negative300.bin.gz', binary=True, limit=50000, )
model.init_sims(replace=True)

def wmdistance(s1: str, s2: str):
    w1 = preprocess(s1)
    w2 = preprocess(s2)
    distance = model.wmdistance(w1, w2)
    return distance

def preprocess(sentence: str):
    words = sentence.lower().split()
    without_stopwords = [w for w in words if w not in stop_words]
    return without_stopwords

