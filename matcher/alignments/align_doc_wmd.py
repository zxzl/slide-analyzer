from .align_doc_base import Sentence
from .align_doc_global import GlobalAlign
from .align_doc_local import LocalAlign
from .distances import wmdistance


class WMDAGlobalAlign(GlobalAlign):
    def __init__(self, delta=3, gap=-1):
        super().__init__(gap)
        self.delta = delta

    def compare_sentences(self, s1: Sentence, s2: Sentence):
        distance = wmdistance(s1.body, s2.body)
        return self.delta - distance # Higher the better


class WMDLocalAlign(LocalAlign):
    def __init__(self, delta=3, gap=-1):
        super().__init__(gap)
        self.delta = delta

    def compare_sentences(self, s1: Sentence, s2: Sentence):
        distance = wmdistance(s1.body, s2.body)
        return self.delta - distance # Higher the better
