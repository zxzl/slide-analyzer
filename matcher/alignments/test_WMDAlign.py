from unittest import TestCase
import numpy as np

from .align_doc_base import Sentence
from .align_doc_wmd import WMDAGlobalAlign, WMDLocalAlign
from .distances import wmdistance


d1 = [
    'Approach',
    'Instructor Introduction',
    'The History of this Course - Spring 2012 UC Berkeley',
    'Spring 2013 UC Berkeley USC',
    'Fall 2013 SF State',
    'Spring 2014 UC Berkeley USC',
    'Why we are focusing on Vulnerable Children'
]

d1_essential = [
    'The History of this Course - Spring 2012 UC Berkeley',
    'Spring 2013 UC Berkeley USC',
    'Fall 2013 SF State',
    'Spring 2014 UC Berkeley USC',
    'Why we are focusing on Vulnerable Children'
]

d2 = [
    'I am a lecturer at the Goldman School of Public Policy at UC Berkeley, where I work on this class called Journalism for Social Change, which was launched in 2012.',
    "I'm a journalist by trade.",
    "And I decided that we needed to recruit an army of journalists to cover complex social issues from a solution-based perspective and that that would be my contribution to driving policy change and improving the field of journalism itself.",
    "This course was started in the Spring of 2012 at UC Berkeley.",
    "By 2013, it had spread from UC Berkeley with the schools of public policy, journalism, and social welfare, with students from the three different schools, to UCS's graduate school of journalism, graduate school of public policy-- USC's graduate school of public policy, where we have the same composition of students.",
    "In fall of 2013, a variant of the course was taught at San Francisco State University.",
    "And in spring of 2014, it was both at UC Berkeley and USC with this development of the massive open online course, which is what we're doing now, which you are a part of, to teach a class to a broader audience.",
    "The focus for the class will be on vulnerable children.",
    "You may be asking yourself why we're going to cover vulnerable children."
]


class TestWMDGlobalAlign(TestCase):

    def test_compare_sentence_run(self):
        s1 = 'Obama speaks to the media in Illinois'
        s2 = 'The president greets the press in Chicago'
        distance = wmdistance(s1, s2)

    def test_closer_sentence_has_smaller_distance(self):

        distance1 = wmdistance(d1[1], d2[0])
        distance2 = wmdistance(d1[1], d2[1])

        self.assertTrue(distance1 < distance2)

    def test_global_align_aligns_all_sentences(self):
        aligner = WMDAGlobalAlign(0)
        align1, align2 = aligner.align(d1_essential, d2)

        max_length = max(len(d1), len(d2))
        self.assertEqual(max_length, len(align1))
        self.assertEqual(max_length, len(align2))

        aligner.pp_sentences_comparision(align1, align2)

    def test_local_aligner_aligns_all_sentences(self):
        aligner = WMDLocalAlign(2)

        doc1, doc2 = self._load_sentence(d1), self._load_sentence(d2)

        distances = self._calculate_distance_matrix(doc1, doc2)
        align1, align2 = aligner.align(doc1, doc2)
        aligner.pp_sentences_comparision(align1, align2)
        pass

    def _calculate_distance_matrix(self, d1, d2):
        dists = np.zeros((len(d1), len(d2)))
        for i, s_i in enumerate(d1):
            for j, s_j in enumerate(d2):
                dists[i, j] = wmdistance(s_i.body, s_j.body)
        return dists

    def _load_sentence(self, strings):
        sentences = []
        for i, v in enumerate(strings):
            sentences.append(Sentence(i, v))
        return sentences
