from typing import NamedTuple


class Reference(NamedTuple):
    slide_id: int
    word_id: int
    subtitle_id: int
