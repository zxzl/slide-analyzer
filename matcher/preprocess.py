import json
import nltk
from nltk.corpus import stopwords
from nltk.corpus import wordnet as wn


def process_sentence(sentence):
    """
    Remove punctuation and stop words from list of words
    :param words: sentence
    :return: [(string, [string])]
    """
    words = nltk.word_tokenize(sentence)
    no_punctuation = list(filter(lambda w: w.isalpha(), words))
    stop = set(stopwords.words('english'))
    no_stopwords = list(filter(lambda w: w not in stop, no_punctuation))

    output = []
    for word in no_stopwords:
        synset = wn.synsets(word)
        synonyms = [w for w in synset[0].lemma_names() if w != word] if len(synset) >= 1 else []
        output.append([word, synonyms])
    return output


def process_slide(slide_path):
    """
    Get keywords from the slide.
    For future matching, assign ids to words as their order in json file
    :param slide_path:
    :return: [(id, [(word, [synonyms])])]
    """
    with open(slide_path, 'r') as f:
        blocks = json.load(f)
    keywords = []
    for i, block in enumerate(blocks):
        sentence = block['description'].lower().replace("-", " ")
        words = process_sentence(sentence)
        keywords.append({
            'id': i,
            'words': words,
            'all': block['description']
        })

    return keywords


def process_subtitle(subtitle_path, start, end):
    """
    Get subtitle for the slide and find synonyms of them
    :param subtitle_path: path of json subtitle
    :param start: when this slide starts (in seconds)
    :param end: when this slide ends (in seconds)
    :return: A sub-list of captions in [ (index, word, synonym) ...] format
    """
    with open(subtitle_path, 'r') as f:
        captions = json.load(f)
    captions_relevant = [" ".join(c['lines']).lower() for c in captions if c['start'] >= start and c['end'] <= end]
    # Convert into list of words
    captions_tokenized = [nltk.word_tokenize(c) for c in captions_relevant]
    # Remove punctuation
    captions_wo_punctuation = [list(filter(lambda w: w.isalpha(), c)) for c in captions_tokenized]
    # Remove stop words
    stop = set(stopwords.words('english'))
    captions_wo_stopwords = [list(filter(lambda w: w not in stop, c)) for c in captions_wo_punctuation]
    # Assign synonyms
    word_syn_pairs = []
    for i, words in enumerate(captions_wo_stopwords):
        word_syn = []
        for word in words:
            synset = wn.synsets(word)
            synonyms = [w for w in synset[0].lemma_names() if w != word] if len(synset) >= 1 else []
            word_syn.append((word, synonyms))
        word_syn_pairs.append({
            'id': i,
            'words': word_syn,
            'all': captions_relevant[i],
        })
    return word_syn_pairs