from unittest import TestCase
from matcher.alignments import global_align, local_align


class TestAlignments(TestCase):
    ###
    # Test cases for global alignments
    ###
    def test_global_align_same_length(self):
        s1 = 'GCATGCU'
        s2 = 'GATTACA'

        align1, align2, score = global_align(s1, s2)
        self.assertEqual(align1, 'GCA-TGCU')
        self.assertEqual(align2, 'G-ATTACA')

    def test_global_align_different_length(self):
        align3, align4, score = global_align('COELANCANTH', 'PELICAN')
        self.assertEqual(align3, 'COELANCANTH')
        self.assertEqual(align4, '-PEL-ICAN--')

    def test_global_align_empty_string(self):
        align5, align6, score = global_align('COELANCANTH', '')
        self.assertEqual(align5, 'COELANCANTH')
        self.assertEqual(align6, '-----------')

    def test_global_align_two_empty_string(self):
        align5, align6, score = global_align('', '')
        self.assertEqual(align5, '')
        self.assertEqual(align6, '')

    ##
    # Test cases for local alignment
    ##
    def test_local_align(self):
        s1 = 'GGTTGACTA'
        s2 = 'TGTTACGG'
        a1, a2 = local_align(s1, s2)

        self.assertEqual(a1, 'GGTTGAC')
        self.assertEqual(a2, 'G-TT-AC')

    def test_local_align_from_slide(self):
        s1 = "Approach Instructor Introduction The History of this Course -Spring 2012 UC Berkeley Spring 2013 UC Berkeley USC Fall 2013 SF State Spring 2014 UC Berkeley USC Why we are focusing on Vulnerable Children"
        s2 = "I am a lecturer at the Goldman School of Public Policy at UC Berkeley, where I work on this class called Journalism for Social Change, which was launched in 2012. I'm a journalist by trade. And I decided that we needed to recruit an army of journalists to cover complex social issues from a solution-based perspective and that that would be my contribution to driving policy change and improving the field of journalism itself. This course was started in the Spring of 2012 at UC Berkeley. By 2013, it had spread from UC Berkeley with the schools of public policy, journalism, and social welfare, with students from the three different schools, to UCS's graduate school of journalism, graduate school of public policy-- USC's graduate school of public policy, where we have the same composition of students. In fall of 2013, a variant of the course was taught at San Francisco State University. And in spring of 2014, it was both at UC Berkeley and USC with this development of the massive open online course, which is what we're doing now, which you are a part of, to teach a class to a broader audience. The focus for the class will be on vulnerable children. You may be asking yourself why we're going to cover vulnerable children."
        a1, a2 = local_align(s1, s2)
        pass
