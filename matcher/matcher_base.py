from abc import *
from typing import List
import json
import nltk
from nltk.corpus import stopwords
from nltk.corpus import wordnet as wn
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer

from .type_hints import Reference
from utils import LoggingMixin


class BaseMatcher(LoggingMixin, metaclass=ABCMeta):

    class EmptyToken(Exception):
        pass

    def __init__(self, slides_path, subtitle_path, answers=None, logger=None):
        self.slides = self.open_slides(slides_path)
        self.subtitles = self.open_subtitle(subtitle_path)
        if logger:
            self.logger = logger
        else:
            self.logger = self.log
        if answers:
            # Answers are helpful for logging
            self.answers: List[Reference] = answers

    def match(self) -> List[Reference]:
        references: List[Reference] = []
        for slide in self.slides:
            slide_subtitles = self.subtitle_for_slide(slide, self.subtitles)
            references += self.match_slide(slide, slide_subtitles)
        return references

    @abstractclassmethod
    def match_slide(self, slide, subtitles) -> List[Reference]:
        """
        Given a slide and corresponding subtitles,
        find references
        """
        pass

    @staticmethod
    def log_slide(logger, slide, subtitles):
        logger.info("======================")
        logger.info("Slide{}".format(slide['index']))
        logger.info("Words are ...")
        for word in slide['words']:
            logger.info("\t {} '{}'".format(word['id'], word['text']))
        logger.info("Subtitles are...")
        for subtitle in subtitles:
            logger.info("\t {} '{}'".format(subtitle['id'], subtitle['text']))

    def log_references(self, slide, subtitles, references: List[Reference]):
        self.logger.info("=====================")
        self.logger.info(f"Slide{slide['index']}")
        self.logger.info("References guessed are (word_id, subtitle_id)")
        for ref in references:
            is_true = 'O'
            true_answer = ""
            if Reference(slide['index'], ref.word_id, ref.subtitle_id) \
                not in self.answers:
                is_true = 'X'
                true_answer = [a.subtitle_id for a in self.answers
                               if a.slide_id == slide['index'] and a.word_id == ref.word_id]
            self.logger.info("\t {} {} {} {}".format(
                is_true, ref.word_id, ref.subtitle_id, true_answer))

    @staticmethod
    def open_slides(slides_path):
        with open(slides_path, 'r') as f:
            slides = json.load(f)
        return slides

    @staticmethod
    def open_subtitle(subtitle_path):
        with open(subtitle_path, 'r') as f:
            subtitle = json.load(f)
            if "sentences" in subtitle:
                subtitle = subtitle['sentences']
        return subtitle

    @staticmethod
    def tokenize(sentence):
        lowered = sentence.lower()
        tokens = nltk.word_tokenize(lowered)
        tokens_split = []
        for token in tokens:
            tokens_split += token.split('-')
        # Remove punctuation
        tokens_wo_punctuation = list(filter(lambda w: w.isalpha(), tokens_split))
        # Remove stop words
        stopwords_set = set(stopwords.words('english'))
        tokens_wo_stopwords = list(filter(lambda w: w not in stopwords_set, tokens_wo_punctuation))

        # Lemmatize
        wnl = WordNetLemmatizer()
        lemmatized = [wnl.lemmatize(word) for word in tokens_wo_stopwords]

        return lemmatized

    @staticmethod
    def stemmize(sentence):
        """
        Convert string into set of token words by remove stop words, stem, etc...
        :param sentence: string
        :return: list of string
        """
        tokens = BaseMatcher.tokenize(sentence)
        # Stemmize
        stemmer = PorterStemmer()
        stemmed = [stemmer.stem(token) for token in tokens]

        return stemmed

    @staticmethod
    def populate_synonyms(word):
        """
        Find similar words in synset
        :param word: string
        :return: list of string
        """
        synset = wn.synset(word)
        synonyms = [w for w in synset[0].lemma_names() if w != word] if len(synset) >= 1 else []
        return synonyms

    @staticmethod
    def subtitle_for_slide(slide, subtitles):
        """
        Return subset of subtitles within given time range
        :param slide: slide dict
        :param subtitles: list of subtitle dict
        :return: list of subtitle
        """
        assert 'start' in slide, "Slide is in wrong format. {}".format(slide)
        assert 'end' in slide, "Slide is in wrong format. {}".format(slide)

        start, end = slide['start'], slide['end']
        return [subtitle for subtitle in subtitles
                if (subtitle['start'] >= start and subtitle['end'] < end) or
                (subtitle['start'] < start <= subtitle['end']) or
                (subtitle['start'] <= end < subtitle['end'])]

    @staticmethod
    def contains(words_a, words_b):
        """
        Check if 'words_b ⊂ words_a' (except for b is empty)
        :param words_a: list of words
        :param words_b: list of words
        :return: boolean
        """
        if len(words_b) == 0:
            return False

        for word in words_b:
            if word not in words_a:
                return False
        return True

    @staticmethod
    def list_partition(l, num_partition):
        """
        Return
        :param l: any list
        :param num_partition:
        :return:
        """

    @staticmethod
    def reference_nodup(references):
        reference_set = set(references)
        return reference_set

