from typing import List

from matcher.type_hints import Reference
from .alignments import WMDLocalAlign
from . import BaseMatcher
from .alignments.align_doc_base import Sentence, Document


class SequenceLocalAlignmentMatcher(BaseMatcher):
    def __init__(self, *args):
        super().__init__(*args)
        self.aligner = WMDLocalAlign(3)

    def match_slide(self, slide, subtitles):

        formatted_slide_elements = slide_to_doc(slide)
        formatted_subtitles = subtitles_to_doc(subtitles)

        align1, align2 =  self.aligner.align(formatted_slide_elements, formatted_subtitles)
        references = self._find_references(align1, align2, slide['index'])

        self.log_slide(self.logger, slide, subtitles)
        self.log_references(slide, subtitles, references)
        return references

    def _find_references(self,
                         aligned_slide_texts: Document,
                         aligned_subtitle_texts: Document,
                         slide_index: int) -> List[Reference]:
        assert len(aligned_slide_texts) == len(aligned_subtitle_texts)
        references = []
        for s1, s2 in zip(aligned_slide_texts, aligned_subtitle_texts):
            if s1 == '-' or s2 == '-':
                continue
            references.append(Reference(slide_index, s1.id, s2.id))
        return references


def slide_to_doc(slide):
    return [Sentence(w['id'], w['text'])
            for w in slide['words']]


def subtitles_to_doc(subtitles):
    return [Sentence(w['id'], w['text'])
            for w in subtitles]
