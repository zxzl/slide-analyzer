from matcher import SemanticNaiveMatcher
from matcher import utils


class SemanticSyntacticMatcher(SemanticNaiveMatcher):

    def match_slide(self, slide, subtitles):
        references = []
        for word in slide['words']:
            references += self.find_most_similar_subtitle(slide['index'], word, subtitles)
        return references

    def find_most_similar_subtitle(self, slide_id, word, subtitles):
        max_similarity, max_subtitle_id = -999, -1
        for subtitle in subtitles:
            subtitle_text = " ".join(subtitle['lines'])

            tree = utils.make_tree(subtitle_text)
            bfs = utils.traverse_bfs(tree)
            for node in bfs:
                leaves = node.leaves()
                phrase_leaves = " ".join(leaves)
                try:
                    similarity = self.compute_phrase_similarity_sum(word['text'], phrase_leaves)
                except self.EmptyToken as e:
                    continue
                if similarity > max_similarity:
                    max_similarity, max_subtitle_id = similarity, subtitle['id']

        if max_subtitle_id == -1:
            return []
        return [(slide_id, word['id'], max_subtitle_id)]
