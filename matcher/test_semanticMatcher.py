import numpy as np
import os
from unittest import TestCase
from matcher import SemanticMatcher

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

BASE_PATH = os.path.abspath(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


class TestSemanticMatcher(TestCase):
    def setUp(self):
        slides_path = os.path.join(BASE_PATH, 'parser/data/video1/video1_slides.json')
        subtitle_path = os.path.join(BASE_PATH, 'parser/data/video1/video1_sub.json')
        self.semantic_matcher = SemanticMatcher(slides_path, subtitle_path)

    def test_compute_embedding(self):
        sample_sentence = "All, everything that I understand, \
            I only understand because I love"
        sample_tokens = self.semantic_matcher.stemmize(sample_sentence)
        logger.info('tokens are ', sample_tokens)
        vectors = self.semantic_matcher.compute_embedding(sample_tokens)
        logger.info('vectors are ', vectors)
        self.assertEqual(len(vectors), len(sample_tokens), "Each token should have own vector")
        for v in vectors:
            self.assertEqual(len(vectors[0]), len(v), "Vectors' dimensions should be equal")

    def test_compute_embedding_unknown_words(self):
        unknown_words = "Hyeungshik"
        unknown_tokens = self.semantic_matcher.stemmize(unknown_words)
        vectors = self.semantic_matcher.compute_embedding(unknown_tokens)
        for v in vectors[0]:
            self.assertEqual(None, v)

    def test_compute_cos_sim(self):
        v1 = np.array([1, 2, 3, 4])
        v2 = np.array([1, 2, 3, 4])
        cos_similarity = self.semantic_matcher.compute_cos_sim(v1, v2)
        self.assertAlmostEqual(cos_similarity, 1, "Same vector should have cos distance of 1")

        v3 = np.array([1,2,3,4])
        v4 = np.array([1,2,3])
        with self.assertRaises(Exception) as context:
            self.semantic_matcher.compute_cos_sim(v3, v4)
        self.assertTrue('dimension' in str(context.exception))

    def test_compute_sum_embedding(self):
        sample_sentence = "Hyeungshik Jung likes pizza"
        sum_vector = self.semantic_matcher.compute_sum_embedding(sample_sentence)
        word_vector = np.array(self.semantic_matcher.glove.emb("pizza"))
        self.assertEqual(sum_vector.shape, word_vector.shape)

    def test_compute_phrase_similarity(self):
        s1 = "good great awesome"
        s2 = "bad suck"
        s3 = "happy"
        v1 = self.semantic_matcher.compute_alignment_score(s1, s3)
        v2 = self.semantic_matcher.compute_alignment_score(s2, s3)
        self.assertLess(v2, v1)

    def test_find_most_similar_subtitle(self):
        slide_id = -1
        test_slide = self.semantic_matcher.slides[1]
        word = test_slide['words'][0]
        subtitles = self.semantic_matcher.subtitle_for_slide(test_slide, self.semantic_matcher.subtitles)

        pair = self.semantic_matcher.find_most_similar_subtitle(slide_id, word, subtitles)

