from matcher import BaseMatcher
from matcher import utils
from matcher.type_hints import Reference


class SyntacticMatcher(BaseMatcher):
    """
    Find reference in given slides
    According to paper,
        We then traverse the phrase tree in breadth-first order starting at the root \
        and check if the entire phrase in the current subtree (after removing stop words and lemmatization) \
        is uniquely contained within a single cell of the table
    :return: list of tuple
    """
    def match_slide(self, slide, subtitles):
        references = []
        # Tokenize words in slide
        for word in slide['words']:
            tokens = self.stemmize(word['text'])
            word['tokens'] = tokens

    def match_slide(self, slide, subtitles):
        """
        Find reference in one slide
        :param slide: slide dict
        :param subtitles: list of subtitle dict
        :return: list of tuple
        """
        references = []
        # Tokenize words in slide
        for word in slide['words']:
            tokens = self.stemmize(word['text'])
            word['tokens'] = tokens

        for subtitle in subtitles:
            references += self.match_subtitle(slide['index'], subtitle, slide['words'])

        return references

    def match_subtitle(self, slide_id, subtitle, words_with_token):
        """
        Find words that contains subtree of subtitle
        :param slide_id: unique integer that subtitle belongs to
        :param subtitle: subtitle dict
        :param words_with_token: list of word dict including tokens
        :return: list of tuple
        """
        references = []
        subtitle_text = " ".join(subtitle['lines'])

        tree = utils.make_tree(subtitle_text)
        bfs = utils.traverse_bfs(tree)
        for node in bfs:
            leaves = node.leaves()
            subphrase = " ".join(leaves)
            subtitle_tokens = self.stemmize(subphrase)

            for word in words_with_token:
                if self.contains(subtitle_tokens, word['tokens']):
                    self.logger.debug(f"WORD::{word['text']} || SUB::{subtitle_tokens}")
                    references.append(Reference(slide_id, word['id'], subtitle['id']))

        return references
