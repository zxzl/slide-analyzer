import os
from unittest import TestCase
from matcher import SemanticOrderedMatcher

BASE_PATH = os.path.abspath(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


class TestSemanticOrderedMatcher(TestCase):
    def setUp(self):
        slides_path = os.path.join(BASE_PATH, 'parser/data/video1/video1_slides.json')
        subtitle_path = os.path.join(BASE_PATH, 'parser/data/video1/video1_sub.json')
        self.matcher = SemanticOrderedMatcher(slides_path, subtitle_path)

    def test_match(self):
        references = self.matcher.match()

        last_subtitle_id = -1
        for reference in references:
            self.assertTrue(last_subtitle_id <= reference[2])
            last_subtitle_id = reference[2]

    def test_match_slide_ordered(self):
        test_slide_index = 1
        test_slide = self.matcher.slides[test_slide_index]
        test_subtitle = self.matcher.subtitle_for_slide(test_slide, self.matcher.subtitles)

        references = self.matcher.match_slide_ordered(test_slide, test_subtitle)



