from unittest import TestCase
from matcher import BaseMatcher


class TestBaseMatcher(TestCase):
    def setUp(self):
        self.matcher = BaseMatcher()

    def test_stemmize(self):
        sentence = "A dog eats two apples!!"
        tokens = self.matcher.stemmize(sentence)
        self.assertNotIn('A', tokens)
        self.assertNotIn('eats', tokens)
        self.assertNotIn('!', tokens)

        sentence2 = "collision-free"
        tokens = self.matcher.stemmize(sentence2)

    def test_references_nodup(self):
        references = [
            self.matcher.Reference(16, 1, 1),
            self.matcher.Reference(16, 1, 1),
            self.matcher.Reference(16, 2, 3),
            self.matcher.Reference(16, 2, 3),
        ]
        nodup = self.matcher.reference_nodup(references)
        self.assertEqual(len(nodup), 2)


