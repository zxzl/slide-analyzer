import itertools
from tqdm import tqdm
import json
import logging
import functools

from matcher import SemanticNaiveMatcher
from matcher import utils


class PermutationMatcher(SemanticNaiveMatcher):

    def __init__(self, slides_path, subtitle_path, logger=None):
        super().__init__(slides_path, subtitle_path)
        self.logger = logger if logger else logging

    def match(self):
        """
        Run matcher for each slide
        :return: list of tuple
        """
        references = []
        for slide in self.slides:
            slide_subtitles = self.subtitle_for_slide(slide, self.subtitles)
            new_references = self.match_slide(slide, slide_subtitles)
            references += new_references

            if slide['index'] == 13:
                break

        return references

    def match_slide(self, slide, subtitles):
        cases = self.make_orders(slide['words'], subtitles)
        self.logger.debug("Slide {} started".format(slide['index']))
        best_case, best_score = -1, -1
        # for case in tqdm(cases):
        for case in cases:
            # score = self.evaluate_order_naive(case)
            # score = self.evaluate_order_naive2(case)
            # score = self.evaluate_order_word_subtitles(case)
            score = self.evaluate_order_words_subtitles(case)

            if score > best_score:
                best_case, best_score = case, score
        self.logger.debug("Slide {} Score {} Order {}".format(
            slide['index'], best_score, '\n' + self.pp_order(best_case)))
        references = self.orders_to_pairs(slide['index'], best_case)
        return references

    def evaluate_order_naive(self, order):
        """
        Evaluate the quality of partition
        by summing similarity btw neighboring (subtitle, word) pairs
        :param order: list of subtitles and words
        :return: score
        """
        score = 0
        for i in range(len(order) - 1):
            if 'start' in order[i] and 'position' in order[i+1]:
                subtitle_text = " ".join(order[i]["lines"])
                word_text = order[i+1]["text"]
                score += self.compare_sub_and_word(subtitle_text, word_text)
        return score

    def evaluate_order_naive2(self, order):
        """
        Evaluate the quality of partition by summing similarity btw
        neighboring (word, subtitle) pairs
        :param order: list of subtitles and words
        :return: score
        """
        score = 0
        for i in range(len(order) - 1):
            if 'position' in order[i] and 'start' in order[i+1]:
                subtitle_text = " ".join(order[i+1]['lines'])
                word_text = order[i]["text"]
                score += self.compare_sub_and_word(subtitle_text, word_text)
        return score

    def evaluate_order_word_subtitles(self, order):
        """
        Evaluate the quality of partition by summing similarity btw
        neighboring (word, [subtitle]) sublists
        :param order: list of subtitle and words
        :return: score
        """
        score = 0
        for i in range(len(order) - 1):
            if 'position' in order[i]:
                word_text = order[i]['text']
                i = i + 1
                while i < len(order) and 'start' in order[i]: # order['j'] is subtitle
                    subtitle_text = " ".join(order[i]['lines'])
                    score += self.compare_sub_and_word(subtitle_text, word_text)
                    i = i + 1
        return score

    def evaluate_order_words_subtitles(self, order):
        """
        Evalute by ([word], [subtitle]) sublists
        :param order: list of subtitle and words
        :return: score
        """
        score = 0
        for i in range(len(order) - 1):
            if 'position' in order[i]: # Find word
                group_score = 0
                # Collect follwing words
                word_texts = [order[i]['text']]
                i = i + 1
                while i < len(order) and 'position' in order[i]:
                    word_texts.append(order[i]['text'])
                    i = i + 1

                # Collect subtitles
                subtitle_texts = []
                while i < len(order) and 'start' in order[i]:
                    subtitle_text = " ".join(order[i]['lines'])
                    subtitle_texts.append(subtitle_text)
                    i = i + 1

                # Calculate score by most similar pair
                for word in word_texts:
                    for sub in subtitle_texts:
                        current_score = self.compare_sub_and_word(sub, word)
                        group_score = group_score + current_score
                if len(word_texts) * len(subtitle_texts) != 0:
                    group_score = group_score / len(word_texts) / len(subtitle_texts)
                score = score + group_score
        return score

    @functools.lru_cache(10000)
    def compare_sub_and_word(self, subtitle_text, word_text):
        max_similarity = -1

        tree = utils.make_tree(subtitle_text)
        bfs = utils.traverse_bfs(tree)
        for node in bfs:
            leaves = node.leaves()
            phrase_leaves = " ".join(leaves)
            try:
                similarity = self.compute_phrase_similarity_sum(word_text, phrase_leaves)
            except self.EmptyToken:
                similarity = 0
            if similarity > max_similarity:
                max_similarity = similarity

        return max_similarity

    @staticmethod
    def pp_order(order):
        output_string = ""
        for o in order:
            output_string += "\t" + json.dumps(o) + "\n"
        return output_string

    @staticmethod
    def orders_to_pairs(slide_id, order):
        references = []
        for i in range(len(order) - 1):
            if 'start' in order[i] and 'position' in order[i+1]:
                subtitle = order[i]
                word = order[i+1]
                references.append((slide_id, word['id'], subtitle['id'], ''))
        return references

    @staticmethod
    def make_orders(words, subtitles):
        num_slots = len(words) + len(subtitles) - 1;
        words_positions = itertools.combinations(range(num_slots), len(words))
        for words_position in words_positions:
            order = [0 for _ in range(num_slots)]
            # place words and subtitles
            word_index, sub_index = 0, 1
            for i in range(0, len(order)):
                if i in words_position:
                    order[i] = words[word_index]
                    word_index += 1
                else:
                    try:
                        order[i] = subtitles[sub_index]
                        sub_index += 1
                    except IndexError:
                        print(sub_index)
            order = [subtitles[0]] + order
            yield order
