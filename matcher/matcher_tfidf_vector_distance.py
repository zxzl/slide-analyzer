import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from typing import List

from matcher.type_hints import Reference
from matcher import BaseMatcher
from matcher.alignments.distances import stop_words
from matcher import utils


class TfIdfVectorMatcher(BaseMatcher):
    def __init__(self, *args):
        super().__init__(*args)
        self.vectorizer = None

    def match(self) -> List[Reference]:
        slide_text, subtitle_text = self.load_documents(self.slides, self.subtitles)
        corpus = slide_text + subtitle_text
        self.vectorizer = self.train_tfidf_vectorizer(corpus)
        self.print_vocab_and_idf_val(self.vectorizer)
        return super().match()

    def match_slide(self, slide, subtitles):
        slide_words = [word['text'] for word in slide['words']]
        subtitle_words = [subtitle['text'] for subtitle in subtitles]

        slide_words_vector = self.vectorizer.transform(slide_words)
        subtitle_words_vector = self.vectorizer.transform(subtitle_words)
        similarity_matrix = cosine_similarity(slide_words_vector, subtitle_words_vector)

        self.print_similarity(similarity_matrix, slide_words_vector, subtitle_words_vector)

        # Pick the most similar subtitle for each word
        references = []
        most_similar_subtitle_index = np.argmax(similarity_matrix, axis=1)
        for i, word in enumerate(slide['words']):
            sub_index = most_similar_subtitle_index[i]

            # self.split_into_subtree(subtitles[sub_index]['text'])

            references.append(Reference(slide['index'],
                                        word['id'],
                                        subtitles[sub_index]['id']))

        self.log_slide(self.logger, slide, subtitles)
        self.log_references(slide, subtitles, references)
        return references

    @staticmethod
    def load_documents(slides, subtitles):
        slide_docs = []
        subtitles_docs = []
        for slide in slides:
            slide_docs += [w['text'] for w in slide['words']]
        for subtitle in subtitles:
            subtitles_docs.append(subtitle['text'])

        return slide_docs, subtitles_docs

    @staticmethod
    def train_tfidf_vectorizer(corpus):
        tfidf_vectorizer = TfidfVectorizer(stop_words=stop_words)
        tfidf_vectorizer.fit(corpus)
        return tfidf_vectorizer

    def print_vocab_and_idf_val(self, vectorizer):
        idf = vectorizer.idf_
        for word, index in vectorizer.vocabulary_.items():
            self.log.info(f"{word}  {idf[index]}")

    def print_similarity(self, sim_matrix, texts_row, texts_col):
        self.log.info(f"{sim_matrix}")
        self.log.info(">> ROW")
        for i, text in enumerate(texts_row):
            self.log.info(f"{i}  {text}")
        self.log.info(">> COL")
        for i, text in enumerate(texts_col):
            self.log.info(f"{i}  {text}")

    def split_into_subtree(self, sentence: str):
        tree = utils.make_tree(sentence)
        phrase = []
        for node in utils.traverse_bfs(tree):
            leaves = node.leaves()
            phrase_leaves = " ".join(leaves)
            phrase.append(phrase_leaves)

        return phrase

