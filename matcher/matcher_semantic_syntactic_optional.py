from matcher.matcher_semantic_syntactic import SemanticSyntacticMatcher
from matcher.type_hints import Reference
from matcher import utils


class SemanticSyntacticOptional(SemanticSyntacticMatcher):
    THRESHOLD = 0.7
    WINDOW_SIZE = 4

    def match_slide(self, slide, subtitles):
        self.log_slide(self.logger, slide, subtitles)

        references = []
        last_subtitle_index = 0
        for slide_element in slide['words']:
            max_score = 0
            max_sub_index = -1
            for sub_index in range(last_subtitle_index,
                                   min(last_subtitle_index + self.WINDOW_SIZE, len(subtitles))):
                score = self.calc_similar_word_subtitle_tree(slide_element, subtitles[sub_index])
                if score > max_score:
                    max_score, max_sub_index = score, sub_index
            if max_score > self.THRESHOLD:
                subtitle_id = subtitles[max_sub_index]['id']
                references.append(
                    Reference(slide['index'], slide_element['id'], subtitle_id))
                self.logger.debug(
                    f"\t\t{slide_element['id']} {subtitle_id} {max_score}")
                last_subtitle_index = max_sub_index

        self.log_references(slide, subtitles, references)
        return references

    def calc_similar_word_subtitle_tree(self, slide_element, subtitle) -> float:
        subtitle_text = " ".join(subtitle['lines']) \
                        if 'lines' in subtitle else subtitle['text']
        max_similarity = -1

        tree = utils.make_tree(subtitle_text)
        tree_bfs = utils.traverse_bfs(tree)
        for node in tree_bfs:
            leaves = node.leaves()
            phrase_leaves = " ".join(leaves)
            try:
                similarity = self.compute_phrase_similarity_avg(slide_element['text'],
                                                                phrase_leaves)
                max_similarity = max(max_similarity, similarity)
            except self.EmptyToken as e:
                continue

        return max_similarity
